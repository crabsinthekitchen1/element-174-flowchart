screen crabs_day6():
    modal True
    tag flowchart
    add "#aaaa" xsize 1920 ysize 1080

    key ["K_ESCAPE"] action [Hide("crabs_day6"), If(show_clock, true=Show("clock"))]
    key ["shift_K_LEFT"] action [Hide("crabs_day6"), Show("crabs_day5")]
    key ["shift_K_RIGHT"] action [Hide("crabs_day6"), Show("crabs_day7")]

    viewport:
        scrollbars "vertical"
        mousewheel True
        arrowkeys True
        pagekeys True

        $ graph = [Crabs_Node(id="start", text=" ", connections=[Crabs_Connection("morning")], img="Day6", condition=gamedate >= 6),
        Crabs_Node(id="morning", text="Morning", connections=[Crabs_Connection("penny", "Help Penny"), Crabs_Connection("malia", "Answer Malia's call")], img="KonbiniPennyCaleb", condition=flag_day6_penny or flag_day6_malia),
        Crabs_Node(id="penny", text="Penny + 1", color="#602f6b", connections=[Crabs_Connection("afternoon")], img="KonbiniPennySmile", condition=flag_day6_penny),
        Crabs_Node(id="malia", text="Malia + 1", color="#860111", connections=[Crabs_Connection("afternoon")], img="MaliaPhone5", condition=flag_day6_malia),
        Crabs_Node(id="afternoon", text="Afternoon", connections=[Crabs_Connection("ash", "Go to the movies"), Crabs_Connection("sarah", "Stay with Sarah"), Crabs_Connection("madison", "Go to the restaurant")], img="ParkSarahOK", condition=flag_day6_ash or flag_day6_sarah or flag_day6_madison),
        Crabs_Node(id="ash", text="Ash + 1", color="#4a646c", connections=[Crabs_Connection("evening")], img="TheaterAshGreet", condition=flag_day6_ash),
        Crabs_Node(id="sarah", text="Sarah + 1", color="#a8e4a0", connections=[Crabs_Connection("evening")], img="RooftopSarahSundown", condition=flag_day6_sarah),
        Crabs_Node(id="madison", text="Madison + 1", color="#fc89ac", connections=[Crabs_Connection("evening")], img="RestaurantMadisonKiss", condition=flag_day6_madison),
        Crabs_Node(id="evening", text="Evening", connections=[Crabs_Connection("bobscorner", "Go to Bob's corner"), Crabs_Connection("alley", "Decline")], img="DowntownNight", condition=flag_day6_katy or flag_day6_isabella or flag_day6_holmes or flag_day6_trish or flag_day6_jenna),
        Crabs_Node(id="bobscorner", text="Bob's Corner", connections=[Crabs_Connection("katy", "Flirt with Katy"), Crabs_Connection("isabella", "Focus on Isabella")], img="CafeIsabellaGreet", condition=flag_day6_katy or flag_day6_isabella),
        Crabs_Node(id="alley", text="Jenna in the alley", connections=[Crabs_Connection("hello", "Say hello"), Crabs_Connection("jenna", "Just watch")], img="AlleywayJennaKiss", condition=flag_day6_jenna or flag_day6_holmes or flag_day6_trish),
        Crabs_Node(id="katy", text="Katy + 1", color="#592720", img="CafeKatyBare", condition=flag_day6_katy),
        Crabs_Node(id="isabella", text="Isabella + 1", color="#FF3EA5", img="CafeIsabellaKiss", condition=flag_day6_isabella),
        Crabs_Node(id="jenna", text="Jenna + 1", color="#253529", img="AlleywayJennaFinger2", condition=flag_day6_jenna),
        Crabs_Node(id="hello", text="Empty alley", connections=[Crabs_Connection("trish", "Go left"), Crabs_Connection("holmes", "Go right")], img="AlleywayChoice", condition=flag_day6_holmes or flag_day6_trish),
        Crabs_Node(id="trish", text="Trish + 1", color="#2e1a47", img="SubwayTrishThumbsup", condition=flag_day6_trish),
        Crabs_Node(id="holmes", text="Holmes + 1", color="#135DD8", img="SubwayManShot", condition=flag_day6_holmes),
        ]
        $ screen_size = calc_graph_size(graph)

        vbox:
            xsize 1920
            ysize screen_size
            use crabs_days(6)
            use graph(graph)


    ### Tooltips
    nearrect:
        focus "malia"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 50
            add "#860111"
            vbox:
                xcenter 0.5
                spacing 10
                text "Required to see Malia on day 7 if lied to Holmes on day 5" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "penny"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 50
            add "#602f6b"
            vbox:
                xcenter 0.5
                spacing 10
                text "Required to see Penny on day 7 and day 10" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "sarah"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 175
            add "#a8e4a0"
            vbox:
                xcenter 0.5
                spacing 10
                null
                add "SarahHeadLap" zoom 0.09 xcenter 0.5
                text "Different scene if haven't met Sarah before" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "ash"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 50
            add "#4a646c"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if seen Ash on day 5 or have 3+ points" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "katy"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 75
            add "#592720"
            vbox:
                xcenter 0.5
                spacing 10
                text "Required scene for Katy" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Available if Katy has 3+ points" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "isabella"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 75
            add "#FF3EA5"
            vbox:
                xcenter 0.5
                spacing 10
                text "Required scene for Isabella" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Available if Isabella has 3+ points" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "trish"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 75
            add "#2e1a47"
            vbox:
                xcenter 0.5
                spacing 10
                text "Required scene for Trish" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Available if Trish has 3+ points" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "holmes"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 75
            add "#135DD8"
            vbox:
                xcenter 0.5
                spacing 10
                text "Required scene for Holmes" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Available if Holmes has 3+ points" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "hello"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 75
            add "#232323"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if Holmes or Trish have 3+ points and Katy and Isabella have less than 3 points" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]