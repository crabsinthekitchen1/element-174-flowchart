screen crabs_day9():
    modal True
    tag flowchart
    add "#aaaa" xsize 1920 ysize 1080
    key ["K_ESCAPE"] action [Hide("crabs_day9"), If(show_clock, true=Show("clock"))]
    key ["shift_K_LEFT"] action [Hide("crabs_day9"), Show("crabs_day8")]
    key ["shift_K_RIGHT"] action [Hide("crabs_day9"), Show("crabs_day10")]
    ###Flowchart

    #Trish/Jules split, then Jules/Chloe decision in an hbox flowing into 3 decisions, before flowing into morning
    # morning and afternoon as usual, then evening decision in an hbox containing same images as Jules/Chloe decision results but actual consequence

    $ day8_sarah_photo = flag_sarah_photo and not day10_morning == "scene_sarah_photograph"
    $ day9_madison = flag_escort_club and not day10_evening == "scene_escort_club"
    $ camgirl = flag_day2_camgirl or flag_day5_chloe or flag_day8_sluttydemon
    $ crabs_current_time = crabs_current_time_hashcode()

    $ graph = [Crabs_Node(id="start", text=" ", connections=[Crabs_Connection("trishart"), Crabs_Connection("julesart")], img="Day9", condition=gamedate >= 9),
        Crabs_Node(id="trishart", text="Trish route", color="#2e1a47", connections=[Crabs_Connection("juleschloe")], img="ArtTrishHug", condition=flag_day6_trish and not (flag_day5_jules and points_jules >= 3) and gamedate >= 9),
        Crabs_Node(id="julesart", text="Jules route", color="#ffd300", connections=[Crabs_Connection("juleschloe")], img="ArtJulesSmile2", condition=flag_day5_jules and points_jules >= 3 and gamedate >= 9),
        Crabs_Node(id="juleschloe", text="After art class", color=["#fc89ac", "#ffd300"], connections=[Crabs_Connection("chloedecision"), Crabs_Connection("downtowndecision"), Crabs_Connection("julesdecision")], img=["HallwayChloeShy", "juleschloefaceoff", "HallwayJulesScowl"], condition=((points_jules >= 3 and flag_day5_jules) or (points_chloe >= 3 and flag_day7_chloe2 and camgirl)) and gamedate >= 9),
        Crabs_Node(id="chloedecision", text="Visit Chloe", color="#fc89ac", connections=[Crabs_Connection("morning")], img="HallwayChloeSmile", condition=flag_chloe_secret and not flag_jules_fingerbang and not day9_madison),
        Crabs_Node(id="downtowndecision", text="Explore downtown", color="#d470a2", connections=[Crabs_Connection("morning")], img="HallwayChloeAlone", condition=day9_madison),
        Crabs_Node(id="julesdecision", text="Visit Jules", color="#ffd300", connections=[Crabs_Connection("morning")], img="HallwayJulesSmile", condition=flag_jules_fingerbang),
        Crabs_Node(id="morning", text="Morning", connections=[Crabs_Connection("holmes", "Ask Holmes about Florist"), Crabs_Connection("rachel", "Talk with Rachel"), Crabs_Connection("katy", "Find Katy"), Crabs_Connection("jenna", "Try to help Jenna")], img="CollegeZoeyRachel2", condition=crabs_current_time >= 33),
        Crabs_Node(id="holmes", text="Holmes + 1", color="#135DD8", connections=[Crabs_Connection("afternoon")], img="ClosetHolmesKiss", condition=flag_holmes_help and not day10_morning == "scene_holmes_help"),
        Crabs_Node(id="rachel", text="Rachel + 1", color="#0038a8", connections=[Crabs_Connection("afternoon")], img="BeanRachelSmug", condition=flag_rachel_coffee and not day10_morning == "scene_rachel_coffee"),
        Crabs_Node(id="katy", text="Katy + 1", color="#592720", connections=[Crabs_Connection("afternoon")], img="CollegeKatyCloseup", condition=flag_katy_cultist),
        Crabs_Node(id="jenna", text="Jenna + 1", color="#253529", connections=[Crabs_Connection("afternoon")], img="AutoJennaZoeyCute", condition=flag_jenna_zoey or (flag_jenna_coffee and (flag_penny_closet or flag_learned_CPR or flag_caroline_hungover) )),
        Crabs_Node(id="afternoon", text="Afternoon", connections=[Crabs_Connection("emma", "Go with Emma"), Crabs_Connection("trish", "Look for Trish"), Crabs_Connection("ash", "Tuesday club meeting"), Crabs_Connection("caroline", "Confront Caroline")], img="CollegeOutside-Afternoon", condition=crabs_current_time >= 34),
        Crabs_Node(id="emma", text="Emma + 1", color="#ff8243", connections=[Crabs_Connection("evening")], img="ClassroomEmmaTopless", condition=(flag_emma_classroom and not day12_afternoon == "scene_emma_classroom") or (flag_emma_trailed and (flag_madison_sit or flag_trish_coffee or day8_sarah_photo))),
        Crabs_Node(id="trish", text="Trish + 1", color="#2e1a47", connections=[Crabs_Connection("evening")], img="RestroomTrishKiss", condition=flag_trish_secret and not day10_afternoon == "scene_trish_secret"),
        Crabs_Node(id="ash", text="Ash + 1", color="#4a646c", connections=[Crabs_Connection("evening")], img="ClubAshJoey4", condition=flag_ash_clothing),
        Crabs_Node(id="caroline", text="Caroline + 1", color="#b11226", connections=[Crabs_Connection("evening")], img="DeansCarolineAngry", condition=flag_caroline_confront),
        Crabs_Node(id="evening", text="Evening", connections=[Crabs_Connection("chloe", "Visit Chloe"), Crabs_Connection("madison", "Explore downtown"), Crabs_Connection("jules", "Visit Jules")], img=["HallwayChloeSmile", "HallwayChloeAlone", "HallwayJulesSmile"], condition=crabs_current_time >= 35),
        Crabs_Node(id="chloe", text="Chloe + 1", color="#fc89ac", connections=[Crabs_Connection("zoey")], img="TrailerRoom3", condition=flag_chloe_secret and not flag_jules_fingerbang and not day9_madison and "she is SluttyDemon" in desc_chloe),
        Crabs_Node(id="madison", text="Madison + 1", color="#d470a2", connections=[Crabs_Connection("zoey")], img="StripMadisonDance2", condition=day9_madison and "Branchfield Social Club" in desc_madison),
        Crabs_Node(id="jules", text="Jules + 1", color="#ffd300", connections=[Crabs_Connection("zoey")], img="BedroomJulesFreeze", condition=flag_jules_fingerbang and "she went motionless" in desc_jules),
        Crabs_Node(id="zoey", text="Zoey + 1", color="#d8b2d1", connections=[], img="HomeStrangerThrust", condition="a stranger appeared and fought Zoey" in desc_zoey),
        ]
    $ screen_size = calc_graph_size(graph)
    viewport:
        scrollbars "vertical"
        mousewheel True
        arrowkeys True
        pagekeys True
        vbox:
            xsize 1920
            ysize screen_size
            use crabs_days(9)
            use graph(graph)

    ### Tooltips
    nearrect:
        focus "trishart"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 50
            add "#2e1a47"
            vbox:
                xcenter 0.5
                spacing 10
                text "Small scene with Trish if seen Trish on day 6 and not on Jules route" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "julesart"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 75
            add "#ffd300"
            vbox:
                xcenter 0.5
                spacing 10
                text "Small scene with Jules if lied to Holmes on day 5 and have 3+ points with Jules" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "juleschloe"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 750
            ymaximum 75
            hbox:
                add "#fc89ac" xsize 375
                add "#ffd300" xsize 375
            hbox:
                spacing 50
                text "Chloe will approach you if you talked to her on day 7 and stayed to listen" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ] xsize 350
                text "Jules will approach you if you lied to Holmes on day 5 and have 3+ points with her" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ] xsize 350

    nearrect:
        focus "holmes"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 100
            add "#135DD8"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if seen Holmes on day 6" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Required scene for Holmes which can be seen on day 10" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "rachel"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 100
            add "#0038a8"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if Rachel has 3+ points" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Required scene for Rachel which can be seen on day 10" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "katy"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 50
            add "#592720"
            vbox:
                text "Available if seen Katy on day 6" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "jenna"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 200
            add "#253529"
            vbox:
                xcenter 0.5
                spacing 10
                null
                add "BeanJennaSmile2" zoom 0.09 xcenter 0.5
                text "Day 8 scene if haven't seen it yet" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Potential start of the subroute with Jenna and Zoey" color "#d8b2d1" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "emma"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 150
            add "#ff8243"
            vbox:
                xcenter 0.5
                spacing 10
                null
                add "CollegeEmmaFollow3" zoom 0.09 xcenter 0.5
                text "Day 8 scene if haven't seen it yet" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "trish"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 125
            add "#2e1a47"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if you've seen Trish on day 6" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Required scene for Trish which can be seen on day 10" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "ash"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 50
            add "#4a646c"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if Ash has 3+ points" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "caroline"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 75
            add "#b11226"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if Caroline has 3+ points" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Required scene for Caroline" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "evening"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 75
            add "#232323"
            vbox:
                xcenter 0.5
                spacing 10
                text "Based on the morning decision between Chloe, Jules or exploring the downtown" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "chloe"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 125
            add "#fc89ac"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if kept listening to Chloe on day 7" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Required scene for Chloe which can be seen on day 11" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "madison"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 200
            add "#d470a2"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if Madison has 3+ points and listened to Madison and Schafer on day 5" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Required scene for Madison which can be seen on day 10" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Not available if on both Chloe and Jules routes" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "jules"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 150
            add "#ffd300"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if lied to Holmes on day 5 and have 3+ points with Jules" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Required scene for Jules" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Can activate a subroute where you plan to humiliate Chloe" color "#fc89ac" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]