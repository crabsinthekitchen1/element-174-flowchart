screen crabs_day13():
    modal True
    tag flowchart
    add "#aaaa" xsize 1920 ysize 1080
    key ["K_ESCAPE"] action [Hide("crabs_day13"), If(show_clock, true=Show("clock"))]
    key ["shift_K_LEFT"] action [Hide("crabs_day13"), Show("crabs_day12")]
    key ["shift_K_RIGHT"] action [Hide("crabs_day13"), Show("crabs_day14")]
    $ crabs_current_time = crabs_current_time_hashcode()

    $ graph = [Crabs_Node(id="start", text=" ", connections=[Crabs_Connection("morning")], img="Day13", condition=gamedate >= 13),
        Crabs_Node(id="morning", text="Morning", connections=[Crabs_Connection("isabella", "Go to Isabella's game"), Crabs_Connection("jules", "Go out with Jules"), Crabs_Connection("emmaconfront", "Confront Emma"), Crabs_Connection("sarah", "Visit Sarah")], img="BedroomDresser", condition=gamedate >= 13),
        Crabs_Node(id="isabella", text="Isabella + 1", color="#FF3EA5", connections=[Crabs_Connection("pugman")], img="LockerIsabellaKiss3", condition="isabella" in day13_morning),
        Crabs_Node(id="sarah", text="Sarah + 1", color="#a8e4a0", connections=[Crabs_Connection("pugman")], img="ApartmentSarahSmile", condition=day13_morning == "scene_sarah_apartment"),
        Crabs_Node(id="jules", text="Jules + 1", color="#ffd300", connections=[Crabs_Connection("pugman")], img="CoffeeJulesTalk", condition="jules" in day13_morning),
        Crabs_Node(id="emmaconfront", text="Emma + 1", color="#ff8243", connections=[Crabs_Connection("pugman")], img="HomeEmmaTalk", condition=day13_morning == "scene_emma_confront"),
        Crabs_Node(id="pugman", text="Watch Pugman", connections=[Crabs_Connection("rachel"), Crabs_Connection("zoey"), Crabs_Connection("jenna"), Crabs_Connection("malia")], img="OutsideTheater", condition=crabs_current_time >= 51),
        Crabs_Node(id="rachel", text="Rachel + 1", color="#0038a8", connections=[Crabs_Connection("evening")], img="TheaterRachelSurprised", condition=day13_afternoon == "scene_rachel_pugman"),
        Crabs_Node(id="zoey", text="Zoey + 1", color="#d8b2d1", connections=[Crabs_Connection("evening")], img="TheaterZoeyShy", condition=day13_afternoon == "scene_zoey_pugman"),
        Crabs_Node(id="jenna", text="Jenna + 1", color="#253529", connections=[Crabs_Connection("evening")], img="TheaterJennaTalk", condition=day13_afternoon == "scene_jenna_pugman"),
        Crabs_Node(id="malia", text="Malia + 1", color="#860111", connections=[Crabs_Connection("evening")], img="TheaterMaliaThink", condition=day13_afternoon == "scene_malia_pugman"),
        Crabs_Node(id="evening", text="Evening", connections=[Crabs_Connection("emma", "Answer the back door"), Crabs_Connection("trish", "Saturday Night Patrol"), Crabs_Connection("penny", "Date with Penny"), Crabs_Connection("chloe", "Visit Chloe's house"), Crabs_Connection("holmes", "Answer the front door")], img="HomeInside-Night", condition=crabs_current_time >= 52),
        Crabs_Node(id="emma", text="Emma + 1", color="#ff8243", connections=[], img="HomeEmmaHurt", condition=day13_evening == "scene_emma_backdoor"),
        Crabs_Node(id="trish", text="Trish + 1", color="#2e1a47", connections=[], img="RooftopTrishWrithe3", condition=day13_evening == "scene_trish_florist"),
        Crabs_Node(id="penny", text="Penny + 1", color="#602f6b", connections=[], img="BowlingPennySmile2", condition=day13_evening == "scene_penny_bowling"),
        Crabs_Node(id="chloe", text="Chloe + 1", color="#fc89ac", connections=[], img="TrailerChloeTouch3", condition=day13_evening == "scene_chloe_practice"),
        Crabs_Node(id="holmes", text="Holmes + 1", color="#135DD8", connections=[], img="HomeHolmesPorch", condition="holmes" in day13_evening),
        ]
    $ screen_size = calc_graph_size(graph)
    ###Flowchart
    viewport:
        scrollbars "vertical"
        mousewheel True
        arrowkeys True
        pagekeys True
        vbox:
            xsize 1920
            ysize screen_size
            use crabs_days(13)
            use graph(graph)


    ### Tooltips
    nearrect:
        focus "isabella"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 150
            add "#FF3EA5"
            vbox:
                xcenter 0.5
                spacing 10
                null
                add "GymIsabellaBarbell3" zoom 0.09 xcenter 0.5
                text "Day 12 scene if you missed it" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "sarah"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 50
            add "#a8e4a0"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if seen Sarah on day 11 or day 12" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "jules"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 150
            add "#ffd300"
            vbox:
                xcenter 0.5
                spacing 10
                null
                add "BedroomJulesLay5" zoom 0.09 xcenter 0.5
                text "Day 12 scene if you missed it" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "emmaconfront"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 125
            add "#ff8243"
            vbox:
                xcenter 0.5
                spacing 10
                text "Required scene for Emma if you missed it on day 12" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Available if seen two men asking you about Emma on day 11 or day 12" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "emma"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 100
            add "#ff8243"
            vbox:
                xcenter 0.5
                spacing 10
                text "Required scene for Emma" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Available if confronted Emma on day 12 or day 13 and didn't reject her" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "rachel"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 50
            add "#0038a8"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if seen Rachel at her dorm on day 9 or day 10" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "malia"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 50
            add "#860111"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if seen Malia at her house on day 11 or day 12" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "zoey"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 175
            add "#d8b2d1"
            vbox:
                xcenter 0.5
                spacing 10
                null
                add "BedroomZoeyCunnilingus3" zoom 0.09 xcenter 0.5
                text "Cunnilingus scene available if ZOey has 7+ points" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "jenna"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 150
            add "#233529"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if Jenna has 5+ points and seen Jenna on day 8 or day 9" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Required scene for Jenna" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Not kissing Jenna after the movie closes her route" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "trish"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 100
            add "#2e1a47"
            vbox:
                xcenter 0.5
                spacing 10
                text "Required scene for Trish" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Taking or destroying the Florist's capsule will affect Trish's route" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "penny"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 50
            add "#602f6b"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if Penny has 4+ points" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "chloe"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 50
            add "#fc89ac"
            vbox:
                xcenter 0.5
                spacing 10
                text "Optional scene if you missed in on day 12 evening" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "holmes"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 125
            add "#135DD8"
            vbox:
                xcenter 0.5
                spacing 10
                text "Day 12 scene if you missed it" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
