screen crabs_day1():
    modal not crabs_tutorial
    tag flowchart
    add "#aaaa" xsize 1900 ysize 1080

    key ["K_ESCAPE"] action [Hide("crabs_day1"), If(show_clock, true=Show("clock"))]
    key ["shift_K_RIGHT"] action [Hide("crabs_day1"), Show("crabs_day2")]
    key ["mousedown_4"] action NullAction() #disable rollback while the screen is open

    $ graph = [Crabs_Node(id="start", text="Start", connections=[Crabs_Connection("pen")], img="gui/main_menu1.png"),
        Crabs_Node(id="pen", text="Meet Zoey", connections=[Crabs_Connection("zoey", "Give her a pen"), Crabs_Connection("nopen", "Ignore her"), ], img="ZoeyWhisper", condition=flag_gave_pen or "ignored her" in desc_zoey),
        Crabs_Node(id="zoey", text="Zoey + 1", color="#d8b2d1", connections=[Crabs_Connection("caleb")], img="ZoeyPen2", condition=flag_gave_pen),
        Crabs_Node(id="nopen", text="Ignored Zoey", connections=[Crabs_Connection("caleb")], img="ZoeyPen3", condition="ignored her" in desc_zoey),
        Crabs_Node(id="caleb", text="Meet Caleb", connections=[Crabs_Connection("punch", "Punch him"), Crabs_Connection("nopunch", "Ignore him")], img="BeanShack-Day1006", condition=flag_gave_pen or "ignored her" in desc_zoey),
        Crabs_Node(id="punch", text="Meet Caroline", connections=[Crabs_Connection("caroline", "It won't happen again"), Crabs_Connection("holmes", "I'll do what I want")], img="Caroline02", condition=flag_day1_punch),
        Crabs_Node(id="nopunch", text="Meet Joey", connections=[Crabs_Connection("ash", "Sure, I'll check it out"), Crabs_Connection("isabella", "No thanks. I'll pass")], img="BeanShack-10016", condition="walked away" in desc_caleb),
        Crabs_Node(id="ash", text="Ash + 1", color="#4a646c", connections=[], img="Ash12", condition=flag_day1_club),
        Crabs_Node(id="caroline", text="Caroline + 1", color="#b11226", connections=[], img="Caroline04", condition=flag_day1_apologize),
        Crabs_Node(id="isabella", text="Isabella + 1", color="#FF3EA5", connections=[], img="SoccerField-Afternoon01006", condition="you weren't interested" in desc_joey),
        Crabs_Node(id="holmes", text="Holmes + 1", color="#135DD8", connections=[], img="Holmes01", condition=flag_day1_backtalk)
        ]

    viewport:
        scrollbars "vertical"
        mousewheel True
        arrowkeys True
        pagekeys True
        $ screen_size = calc_graph_size(graph)
        vbox:
            xsize 1900
            ysize screen_size

            use crabs_days(1)
            use graph(graph)


    ###Tooltips
    nearrect:
        focus "zoey"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 150
            add "#d8b2d1"
            vbox:
                spacing 10
                text "Required to meet Katy on day 2" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Required to meet Chloe on day 2" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Required to see Ash on day 2" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Required to talk with Isabella on day 2" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "nopen"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 125
            add "#232323"
            vbox:
                xcenter 0.5
                spacing 10
                text "Required to meet Trish on day 2" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Required to meet Madison on day 2" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Required for one of Rachel's scenes on day 2" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "punch"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 175
            add "#232323"
            vbox:
                xcenter 0.5
                spacing 10
                text "Required to meet Emma on day 2" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Required to meet Malia on day 2" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Required to see Holmes on day 3" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Required to see Trish on day 3" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Required to see Caroline on day 4" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
    nearrect:
        focus "nopunch"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 100
            add "#232323"
            vbox:
                xcenter 0.5
                spacing 10
                text "Required to meet Jules on day 2" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Required for one of Rachel's scenes on day 2" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "ash"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 200
            add "#4a646c"
            vbox:
                xcenter 0.5
                spacing 10
                text "Required for Ash route" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Required to meet Chloe on day 2" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Required to meet Jenna on day 3 and see her on day 4" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Required to see Sarah on day 3 and day 5" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "isabella"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 75
            add "#FF3EA5"
            vbox:
                xcenter 0.5
                spacing 10
                text "Required to see Isabella on day 3" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Required to see Rachel on day 3" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
