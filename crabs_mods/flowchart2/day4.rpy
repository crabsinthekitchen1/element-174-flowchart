screen crabs_day4():
    modal True
    tag flowchart
    add "#aaaa" xsize 1920 ysize 1080
    key ["K_ESCAPE"] action [Hide("crabs_day4"), If(show_clock, true=Show("clock"))]
    key ["shift_K_LEFT"] action [Hide("crabs_day4"), Show("crabs_day3")]
    key ["shift_K_RIGHT"] action [Hide("crabs_day4"), Show("crabs_day5")]
    ###Flowchart
    viewport:
        scrollbars "vertical"
        mousewheel True
        arrowkeys True
        pagekeys True

        $ graph = [Crabs_Node(id="start", text=" ", connections=[Crabs_Connection("julesjenna", "Jenna > Madison"), Crabs_Connection("julesmadison", "Madison >= Jenna")], img="Day4", condition=gamedate>=4),
        Crabs_Node(id="julesjenna", text="Jules bullying Jenna", connections=[Crabs_Connection("jenna", "Defend Jenna"), Crabs_Connection("jules", "Bully Jenna")], img="JulesJennaBerate2", condition="bullying Jenna" in desc_jenna),
        Crabs_Node(id="julesmadison", text="Jules bullying Madison", connections=[Crabs_Connection("jules", "Bully Madison"), Crabs_Connection("madison", "Defend Madison")], img="JulesMadisonBerate2", condition="bullying Madison" in desc_jules),
        Crabs_Node(id="jenna", text="Jenna + 1", color="#253529", connections=[Crabs_Connection("afternoon")], img="LawnJennaSmile", condition=flag_day4_jenna_defend),
        Crabs_Node(id="madison", text="Madison + 1", color="#d470a2", connections=[Crabs_Connection("afternoon")], img="LawnMadisonSmile", condition=flag_day4_madison_defend),
        Crabs_Node(id="jules", text="Jules + 1", color="#ffd300", connections=[Crabs_Connection("afternoon")], img="ArtJulesSmile", condition=flag_day4_jenna_bully or flag_day4_madison_bully),
        Crabs_Node(id="afternoon", text="Afternoon", connections=[Crabs_Connection("trish", "Visit the science lab"), Crabs_Connection("penny", "Don't visit"), Crabs_Connection("rachel", "Visit the dorms")], img="CollegeOutside-Afternoon", condition=flag_day4_jenna_bully or flag_day4_jenna_defend or flag_day4_madison_bully or flag_day4_madison_defend),
        Crabs_Node(id="trish", text="Trish + 1", color="#2e1a47", connections=[Crabs_Connection("chloe", "Order"), Crabs_Connection("emma", "Chaos")], img="LabTrishExcited", condition=flag_day4_trish),
        Crabs_Node(id="penny", text="Penny + 1", color="#602f6b", connections=[Crabs_Connection("caroline", "Caroline Kelly"), Crabs_Connection("malia", "Dean Blakeman")], img="AdminPennySurprise",condition=flag_day4_penny),
        Crabs_Node(id="rachel", text="Rachel + 1", color="#0038a8", connections=[Crabs_Connection("katy", "Horror movie"), Crabs_Connection("sarah", "Classic movie")], img="DormRachelClimb2", condition=flag_day4_rachel),
        Crabs_Node(id="chloe", text="Chloe + 1", color="#fc89ac", connections=[Crabs_Connection("zoey")], img="LabChloeSmile3", condition=flag_day4_chloe),
        Crabs_Node(id="emma", text="Emma + 1", color="#ff8243", connections=[Crabs_Connection("zoey")], img="LabEmmaSmile", condition=flag_day4_emma),
        Crabs_Node(id="caroline", text="Caroline + 1", color="#b11226", connections=[Crabs_Connection("zoey")], img="DeansCarolineSmile", condition=flag_day4_caroline),
        Crabs_Node(id="malia", text="Malia + 1", color="#860111", connections=[Crabs_Connection("zoey")], img="AdminMaliaCoy", condition=flag_day4_malia),
        Crabs_Node(id="katy", text="Katy + 1", color="#592720", connections=[Crabs_Connection("zoey")], img="DormKatyBlanket", condition=flag_day4_katy),
        Crabs_Node(id="sarah", text="Sarah + 1", color="#a8e4a0", connections=[Crabs_Connection("zoey")], img="DormSarahBlanket", condition=flag_day4_sarah),
        Crabs_Node(id="zoey", text="Zoey + 1", color="#d8b2d1", connections=[Crabs_Connection("rachelintro", "Didn't meet Rachel")], img="HomeZoeyShy", condition=flag_day4_zoey),
        Crabs_Node(id="rachelintro", text="Meet Rachel", img="HomeZoeyRachel", condition=flag_day4_zoey and not (flag_day2_rachel or flag_day2_rachel2 or flag_day3_rachel or flag_day4_rachel)),
        ]
        $ screen_size = calc_graph_size(graph)

        vbox:
            xsize 1920
            ysize screen_size
            use crabs_days(4)
            use graph(graph)


    ###Tooltips
    nearrect:
        focus "rachel"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 50
            add "#0038a8"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if Rachel has more points than Trish, Chloe and Emmma" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "trish"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 50
            add "#2e1a47"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if Trish, Chloe or Emma have more points than Rachel" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]                

    nearrect:
        focus "caroline"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 50
            add "#b11226"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if met Caroline on day 1" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "malia"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 50
            add "#860111"
            vbox:
                xcenter 0.5
                spacing 10
                text "Gives some background about another girl" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

