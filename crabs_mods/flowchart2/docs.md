# Flowchart v2 documentation
## Difference between v1 and v2
v2 splits the flowchart into 15 different flowcharts, split by days. Doing so solves the
main performance issue of the v1 flowchart. Instead of having hundreds of icons with
tooltips on the same screen, which causes the flowchart to lag when scrolling, and the
window itself is more than 10 screens long, each day has less than 30 icons. Having
fewer icons also allows me to make icons bigger and move some information from
tooltips directly to the screen

## Technical details
The code for the main calendar screen is located in [new_flowchart.rpy](new_flowchart.rpy).
That file also contains the code for the custom displayables I use to draw lines between
icons because I didn't want to use static images for that

Buttons on the calendar are automatically generated if
1. you have a file DayX.jpg, which will be used as an image for the button (except day 1, which uses default main menu background)
2. you have defined a screen crabs_dayX that has the flowchart

Each daily flowchart is defined in a separate file, like [day1.rpy](day1.rpy).
Each screen looks more or less the same, background and keyboard shortcuts at the top,
then a variable `graph` with a huge list of objects that will be drawn
then this part, which tells the screen to show two other screens inside a scrollable viewport
```renpy
viewport:
    scrollbars "vertical"
    mousewheel True
    arrowkeys True
    pagekeys True
    $ screen_size = calc_graph_size(graph)
    vbox:
        xsize 1920
        ysize screen_size
        use crabs_days(1)
        use graph(graph)
```
and then tooltips for all the buttons that you want to show.

The first screen, `crabs_days` is just a list of buttons that switches between days
TODO: (need to figure out how to highlight the selected day)

`screen_size` variable is needed because if I don't set the size for vbox, the viewport becomes too big

second screen `graph` is the screen where all the buttons and arrows are actually being drawn.
it uses that huge list that's defined at the beginning of the screen and draws one button for each object in the list. 

### Explaining the `graph` variable
The list contains Python objects which vaguely represent all I need to draw buttons, each object has
- id: the way to identify buttons on the screen, has to be unique within the screen
- image: the image for the button.
you can also pass the list of images, so they will be horizontally next to each other
- text: the text below the image.
if you don't want any text, put a space there because otherwise it will just have an empty area below the image
- color: background color for the button. although since it has an image it's only visible in the text area. black (#000000) by default.
you can also pass the list of colors to split the background into different colors like I do for subroute scenes
- connections: the list of `Crabs_Connection` objects which have parameters I need for arrows.
they have an id of a button they should connect to and the text that should show above the arrow
- condition: if True, it will add a blue checkmark to the text

### Explaining the flowchart drawing
It uses a very primitive version of "Layered graph drawing"
* First, it decides vertical order of the buttons so that the arrows from buttons can only go down and assign each button to a "layer"
When that happens, it also adds fake "buttons" that have a vertical line as an image so that arrows can only be drawn between neighboring layers
* Next, it orders the buttons on the same "layer" to avoid crosses.
  * I order buttons in the same order as their "parents" (buttons that have arrows to the button) are ordered.
  For example, if on day 1 I have "punch Caleb" on the left, that means Caroline and Holmes scenes will also be on the left, while Ash and Isabella will be on the right
  * When ordering buttons that have the same "parent", I order them like they are ordered in `connections` parameter
* After ordering is done, I assign some absolute positions to each button
  * Vertically, each "layer" is 183 pixels apart. Button size is 173 (for buttons with one image, can get wider) x 133, so that leaves me with 50 pixels for arrows
  * Horizontally
    * First I place buttons 400 pixels apart (between centers)
    * Then I move buttons so that "parents" are placed between children to make arrows look nicer
    * If after that there's less than 200 pixels between buttons, I move them again to have at least 200 so there's at least some space between buttons
* Then I go through the `graph` list and place the buttons at their assigned positions with text, image, background color and checkmarks based on the parameters in the list
* After that, I draw arrows between all the "inactive" buttons
* Once all the "inactive" arrows are drawn, I draw "active" ones, using the background color of the button they go into as the line color. 
This might cause text to overlap with other arrows but it shouldn't be a big problem