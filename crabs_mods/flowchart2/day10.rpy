screen crabs_day10():
    modal True
    tag flowchart
    add "#aaaa" xsize 1920 ysize 1080
    key ["K_ESCAPE"] action [Hide("crabs_day10"), If(show_clock, true=Show("clock"))]
    key ["shift_K_LEFT"] action [Hide("crabs_day10"), Show("crabs_day9")]
    key ["shift_K_RIGHT"] action [Hide("crabs_day10"), Show("crabs_day11")]
    $ crabs_current_time = crabs_current_time_hashcode()

    $ graph = [Crabs_Node(id="start", text=" ", connections=[Crabs_Connection("morning")], img="Day10", condition=gamedate >= 10),
        Crabs_Node(id="morning", text="Morning", connections=[Crabs_Connection("holmesflorist", "Ask Holmes about Florist"), Crabs_Connection("rachel", "Go to Rachel's dorm"), Crabs_Connection("sarah", "Skip class"), Crabs_Connection("madison", "Talk to Madison")], img="BedroomZoeySleep", condition=gamedate >= 10),
        Crabs_Node(id="holmesflorist", text="Holmes + 1", color="#135DD8", connections=[Crabs_Connection("afternoon")], img="ClosetHolmesKiss", condition=day10_morning == "scene_holmes_help"),
        Crabs_Node(id="rachel", text="Rachel + 1", color="#0038a8", connections=[Crabs_Connection("afternoon")], img="DormRachelWow", condition="rachel" in day10_morning),
        Crabs_Node(id="sarah", text="Sarah + 1", color="#a8e4a0", connections=[Crabs_Connection("afternoon")], img="CollegeSarahHandjob2", condition="sarah" in day10_morning),
        Crabs_Node(id="madison", text="Madison + 1", color="#d470a2", connections=[Crabs_Connection("afternoon")], img="RoomMadisonKiss", condition=day10_morning == "scene_madison_classroom"),
        Crabs_Node(id="afternoon", text="Afternoon", connections=[Crabs_Connection("isabella", "Eat with Isabella"), Crabs_Connection("ash", "Wednesday club meeting"), Crabs_Connection("trish", "Visit Trish"), Crabs_Connection("jenna", "Check up on Jenna")], img="CollegeOutside-Afternoon", condition=crabs_current_time >= 38),
        Crabs_Node(id="isabella", text="Isabella + 1", color="#FF3EA5", connections=[Crabs_Connection("evening")], img="RestaurantIsabellaSad2", condition=day10_afternoon == "scene_isabella_lunch"),
        Crabs_Node(id="trish", text="Trish + 1", color="#2e1a47", connections=[Crabs_Connection("evening")], img="AutoTrishKiss", condition="trish" in day10_afternoon),
        Crabs_Node(id="ash", text="Ash + 1", color="#4a646c", connections=[Crabs_Connection("evening")], img="ClubAshKiss2", condition=day10_afternoon == "scene_ash_secret"),
        Crabs_Node(id="jenna", text="Jenna + 1", color="#253529", connections=[Crabs_Connection("evening")], img="PoolJennaSmile2", condition=day10_afternoon == "scene_jenna_pool"),
        Crabs_Node(id="evening", text="Evening", connections=[Crabs_Connection("caroline", "Visit Caroline's apartment"), Crabs_Connection("malia", "Spy lessons"), Crabs_Connection("holmes", "Eat with Holmes"), Crabs_Connection("penny", "Date with Penny"), Crabs_Connection("madisonstrip", "Explore downtown side streets")], img="HomeInside-Night", condition=crabs_current_time >= 39),
        Crabs_Node(id="caroline", text="Caroline + 1", color="#b11226", connections=[], img="ApartmentCarolineSmile", condition=day10_evening == "scene_caroline_apartment"),
        Crabs_Node(id="madisonstrip", text="Madison + 1", color="#d470a2", connections=[], img="StripMadisonDance2", condition=day10_evening == "scene_escort_club"),
        Crabs_Node(id="holmes", text="Holmes + 1", color="#135DD8", connections=[], img="BarHolmesSmile", condition=day10_evening == "scene_holmes_restaurant"),
        Crabs_Node(id="penny", text="Penny + 1", color="#602f6b", connections=[], img="BarPennySing2", condition=day10_evening == "scene_penny_karaoke"),
        Crabs_Node(id="malia", text="Malia + 1", color="#860111", connections=[], img="ParkMaliaSexy", condition="malia" in day10_evening),
        ]
    $ screen_size = calc_graph_size(graph)
    ###Flowchart
    viewport:
        scrollbars "vertical"
        mousewheel True
        arrowkeys True
        pagekeys True
        vbox:
            xsize 1920
            ysize screen_size
            use crabs_days(10)
            use graph(graph)


    ### Tooltips
    nearrect:
        focus "holmesflorist"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 50
            add "#135DD8"
            vbox:
                xcenter 0.5
                spacing 10
                text "Required scene for Holmes if you missed it on day 9" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "rachel"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 200
            add "#0038a8"
            vbox:
                xcenter 0.5
                spacing 10
                text "Required scene for Rachel which can be seen on day 11" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

                add "BeanRachelSmug" zoom 0.09 xcenter 0.5
                text "Day 9 scene if you missed it" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "sarah"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 150
            add "#a8e4a0"
            vbox:
                xcenter 0.5
                spacing 10
                null
                add "HallwaySarahSmile" zoom 0.09 xcenter 0.5
                text "Day 8 scene if you missed it" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "madison"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 50
            add "#d470a2"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if seen Madison on day 9" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "madison"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 50
            add "#d470a2"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if seen Madison on day 9" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "isabella"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 100
            add "#FF3EA5"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if saw Isabella on day 6" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Required scene for Isabella which can be seen on day 11" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "trish"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 150
            add "#2e1a47"
            vbox:
                xcenter 0.5
                spacing 10
                null
                add "RestroomTrishSurprise" zoom 0.09 xcenter 0.5
                text "Day 9 scene if you missed it" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "ash"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 100
            add "#4a646c"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if Ash has 3+ points" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Required scene for Ash which can be seen on day 11" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "jenna"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 50
            add "#253529"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if seen Jenna on day 8 or day 9" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "caroline"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 175
            add "#b11226"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if seen Caroline on day 9" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Sleeping with Caroline lets you have sex with her on day 12" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Not sleeping with Caroline and not kissing her on day 7 unlocks anal option on day 14" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "malia"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 150
            add "#860111"
            vbox:
                xcenter 0.5
                spacing 10
                null
                add "FastfoodMaliaWonder" zoom 0.09 xcenter 0.5
                text "Day 8 scene if you missed it" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "holmes"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 50
            add "#135DD8"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if seen Holmes on day 9" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "penny"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 50
            add "#602f6b"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if got Penny's phone number on day 6" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "madisonstrip"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 50
            add "#d470a2"
            vbox:
                xcenter 0.5
                spacing 10
                text "Required scene for Madison if you missed it on day 9" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]