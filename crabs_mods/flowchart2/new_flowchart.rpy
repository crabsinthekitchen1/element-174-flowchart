#main file containing the new walkthrough
init python:

    config.overlay_screens.append('crabs_wt_overlay')
    """
    Taken from StackExchange
    https://codereview.stackexchange.com/questions/70143/drawing-a-dashed-line-with-pygame
    """
    class Crabs_Point:
        # constructed using a normal tupple
        def __init__(self, point_t = (0,0)):
            self.x = float(point_t[0])
            self.y = float(point_t[1])
        # define all useful operators
        def __add__(self, other):
            return Crabs_Point((self.x + other.x, self.y + other.y))
        def __sub__(self, other):
            return Crabs_Point((self.x - other.x, self.y - other.y))
        def __mul__(self, scalar):
            return Crabs_Point((self.x*scalar, self.y*scalar))
        def __div__(self, scalar):
            return Crabs_Point((self.x/scalar, self.y/scalar))
        def __len__(self):
            import math
            return int(math.sqrt(self.x**2 + self.y**2))
        def __str__(self):
            return str((self.x, self.y))
        def __repr__(self):
            return str((self.x, self.y))

        # get back values in original tuple format
        def get(self):
            return (self.x, self.y)

    """
    Class containing parameters for a button displayed on screen
    :param string id: Identificator for the button. Has to be unique within the screen
    :param string text: Text under the image. If there is no text there will be an empty space below an image because all buttons have fixed ysize
    :param string color: Hex representation of a background color. Can be a list of colors,
        in which case the background will be split vertically and will have several sections with different background color
    :param string img: Image used for the button. Can be a list of images
    :param bool condition: Whether the button should be active or greyed out
    :param list connections: List of Crabs_Connection objects representing arrows
    :param position: Effectively ignored since the position is reassigned when the screen is opened
    """
    class Crabs_Node:
        def __init__(self, id=None, text="", connections=[], img=None, position=Crabs_Point((0,0)), color="#000000", condition=True):
            self.position = position
            self.id = id
            self.connections = connections
            self.img = img
            self.text = text
            self.color = color
            self.condition = condition
   
        def __eq__(self, other):
            return isinstance(other, Crabs_Node) and self.id == other.id
   
        def __str__(self):
            return self.id

        def __repr__(self):
            return self.id

    """
    Class containing parameters for arrows
    :param string id: id field of the Crabs_Node object it should connect to
    :param string text: Text displayed on the arrow. Optional
    """
    class Crabs_Connection:
        def __init__(self, id=None, text=None):
            self.id = id
            self.text = text
        
        def __eq__(self, other):
            return isinstance(other, Crabs_Connection) and self.id == other.id

        def __str__(self):
            return self.id

        def __repr__(self):
            return self.id

    # 1. places each scene approximately 400 pixels in width and 183 pixels in height away from each other (button size is 173x133)
    # 2. then going from the bottom, changes "parent" horizontal position so that it's placed between "children"
    # 3. if after step 2 buttons get too close then move them apart to make sure there's at least 200 pixels horizontally between the centers so they don't overlap
    def assign_position(graph, layers):
        #default positioning
        for node in graph:
            layers_reversed = assign_layers_recursive(graph)
            layer = layers_reversed[node.id]
            nodes = layers[layer]
            middle_node_index = (len(nodes) - 1) / 2.0
            current_node_index = nodes.index(node.id)
            
            node.position = Crabs_Point((960 + min(1920 // len(nodes), 400) * (current_node_index - middle_node_index), 5 + 183*layer))
        # make sure "parents" are placed between "children"
        for node in graph:
            
            if len(node.connections) > 1:
                child_positions = list(map(lambda x: x.position.x, filter(lambda x, node=node: x.id in map(lambda x: x.id, node.connections), graph)))
                if len(child_positions) > 0:
                    node.position.x = sum(child_positions)//len(child_positions)

            # If two fake line buttons connect to each other, put them at the same horizontal positions so they have less bends
            if len(node.connections) == 1 and isinstance(node.img, Crabs_Line):
                connected = [connection for connection in graph if node.connections[0].id == connection.id][0]
                if isinstance(connected.img, Crabs_Line):
                    if node.position.x > 960:
                        node.position.x = max(node.position.x, connected.position.x)
                        connected.position.x = max(node.position.x, connected.position.x)
                    elif node.position.x < 960:
                        node.position.x = min(node.position.x, connected.position.x)
                        connected.position.x = min(node.position.x, connected.position.x)

        # to the right
        for i in range(0, len(graph) - 2):
            node = graph[i]
            neighbor = graph[i + 1]
            if neighbor.position.y == node.position.y:
                if abs(neighbor.position.x - node.position.x) < 200 and neighbor.position.x < 1720:
                    distance = neighbor.position.x - node.position.x
                    neighbor.position.x = node.position.x + 200
        # to the left
        for i in range(len(graph) - 1, 1):
            node = graph[i]
            neighbor = graph[i - 1]
            if neighbor.position.y == node.position.y:
                if abs(neighbor.position.x - node.position.x) < 200 and neighbor.position.x > 200:
                    neighbor.position.x = node.position.x - 200
        return graph

    # Orders scene buttons vertically. Basically if one scene has an arrow going into another, the arrow has to go down
    def assign_layers(graph):
        layers = assign_layers_recursive(graph)
        # since the result is in {scene1 : 1, scene2 : 2, scene3: 2} format we need to reverse it to make it easier to use
        return reverse_layers(layers)

    
    def assign_layers_recursive(graph):
        layers = {}
        # set a layer as a layer of the source button + 1
        # calculate max in case there's a situation where a button has sources at different layers
        for node in graph:
            for sink in node.connections:
                layers[sink.id] = max(layers.setdefault(sink.id, 0), layers.setdefault(node.id, 0) + 1)

        for node in graph:
            for sink in node.connections:
                node_layer = layers[node.id]
                connected_layer = layers[sink.id]

                # adds fake buttons with vertical lines if there's more than one layer between connected scenes
                if node_layer != (connected_layer - 1):
                    target = [s for s in graph if sink.id == s.id][0]
                    clr = target.color
                    if not isinstance(clr, str):
                        if node.color in clr:
                            clr = node.color
                        else:
                            clr = clr[0]
                    graph.append(Crabs_Node(id=node.id + sink.id, color="{}00".format(clr), img=Crabs_Line(color="{}00".format(clr), lines=[(6,0), (6, 133)], arrow=False),
                                            connections=[Crabs_Connection(sink.id)], condition=target.condition))
                    node.connections.insert(node.connections.index(sink), Crabs_Connection(node.id + sink.id))
                    node.connections.remove(sink)
                    layers = assign_layers_recursive(graph)
        return layers


    # reverses the dictionary so I can get {1: [scene1], 2: [scene2, scene3]} dict
    def reverse_layers(layers):
        layers_reversed = {}
        for k, v in layers.items():
            layers_reversed.setdefault(v, []).append(k)
        return layers_reversed

    # To minimize arrow crosses, order the scenes on lower level in the order of their "parents"
    def order_layer(layers, graph):
        for layer, nodes in layers.items():
            if (layer > 0):
                layers[layer] = sorted(nodes, key=lambda x, graph=graph, layers=layers, layer=layer: find_relative_position(x, graph, layers, layer))

    def find_relative_position(node_id, graph, layers, current_layer):
        sources = find_sources(node_id, graph)
        # check relative positioning within the "parent" level to sort items in order of their parents first
        # that's the part that gets multiplied by 100
        # then check "parent's" children and order them in the order in which children are defined in "connections" list
        # if there are more than one parent, only check left
        left_parent = min(sources, key=lambda node: layers[current_layer - 1].index(node.id))
        return sum([layers[current_layer - 1].index(left_parent.id) * 100 + 10 * left_parent.connections.index(Crabs_Connection(node_id))])

    # Finds scenes which have arrow to the "id"
    def find_sources(id, graph):
        return list(filter(lambda x: id in map(lambda y: y.id, x.connections), graph))

    # makes sure that the line only has two 90 degree bends
    # well, if there's a fake "scene" with a vertical line it can have more because that's technically separate lines 
    # if there is more than one line coming out from source, place the starting points 20 pixels away from each other
    # if there is more than one line coming into targer and they both have text, place ending points also 20 pixels away from each other
    # if there are more than 3 lines coming out from source, then try to place them 20 pixels apart vertically so they don't overlap
    # outside scenes have higher lines, inside have lower scenes
    # only works with 4 or 5 scenes on the same level
    # I didn't have 6 or more because I usually can split them into different levels
    def get_line_points(source, target, graph):
        import math

        middle_node_index = (len(source.connections) - 1) / 2.0
        current_node_index = source.connections.index(Crabs_Connection(target.id))
        start_point = (source.position.x + 20 * (current_node_index - middle_node_index), source.position.y + 133)
        y_disp = 0
        if len(source.connections) > 3:
            idx = [connection.id for connection in source.connections].index(target.id)
            if idx == 0 or idx == (len(source.connections) - 1):
                y_disp = -20
            elif idx == middle_node_index:
                y_disp = 0
            elif abs(idx - middle_node_index) <= 1:
                y_disp = 20
            else:
                y_disp = 0
        bend_point = (source.position.x + 20 * (current_node_index - middle_node_index), (target.position.y + source.position.y + 133 + y_disp) // 2)
        sources = find_sources(target.id, graph)
        connections = []
        for s in sources:
            connections.extend(s.connections)
        text_connections = [connection.text for connection in connections if connection.id == target.id and connection.text is not None]
        if len(text_connections) > 1:
            middle_node_index = (len(sources) - 1) / 2.0
            current_node_index = sources.index(source)

        else:
            middle_node_index = current_node_index = 0
        second_bend_point = (target.position.x + 20 * (current_node_index - middle_node_index), (target.position.y + source.position.y + 133 + y_disp) // 2)

        end_point = (target.position.x + 20 * (current_node_index - middle_node_index), target.position.y)

        return (start_point, bend_point, second_bend_point, end_point)

    # calculates the size that the flowchart will take
    # calculates positions of every element
    # then takes any scene from the last level
    # checks the position and adds extra 225 pixels to account for the top of the screen with day switcher and some place at the bottom
    def calc_graph_size(graph):
        graph_layers = assign_layers(graph)
        order_layer(graph_layers, graph)
        assign_position(graph, graph_layers)
        last_layer = sorted(graph_layers.keys())[-1]
        last_layer_scene = graph_layers[last_layer][0]
        last_ypos = [node.position for node in graph if node.id == last_layer_scene][0].y
        return int(last_ypos) + 225

    # used for drawing dashed lines but I don't actually use it
    def draw_dashed_line(canvas, color, start_pos, end_pos, width=1, dash_length=10):
        origin = Crabs_Point(start_pos)
        target = Crabs_Point(end_pos)
        displacement = target - origin
        length = len(displacement)
        slope = displacement/length

        for index in range(0, length/dash_length, 2):
            start = origin + (slope *    index    * dash_length)
            end   = origin + (slope * (index + 1) * dash_length)
            canvas.line(color, start.get(), end.get(), width)

    def median(lst):
        n = len(lst)
        s = sorted(lst)
        return (s[n//2-1]/2.0+s[n//2]/2.0, s[n//2])[n % 2] if n else None

    """
    Custom displayable that draws lines using pygame.draw#lines method
    http://www.pygame.org/docs/ref/draw.html
    The displayable size is determined by coordinates passed to "lines" parameter
    (max X coordinate and max Y coordinate, with 2*line_width added to either X or Y depending on line direction in case you have a line right at the edge)
    For the easiest usage, put it inside of a "fixed" displayable that has the same width as your screen
    Then you'll have coordinates mostly correspond to the position on the screen
    I haven't tested setting the actual pos/align/offset values for this displayable
    """
    class Crabs_Line(renpy.Displayable):
        """
        @params:
            color: line color. default: black
            lines: the list of coordinates sent to PyGame's "lines" method
            direction: if "arrow" parameter is True, the direction in which this arrow should point
            line_style: can be either "solid" or "dashed". If "dashed", then it draws a dashed line
            line_width: line width in pixels
            """
        def __init__(self, color="#000000", lines=[], direction="down", line_style="solid", line_width=3, arrow=True, **kwargs):
            if len(color) == 5:
                color = color[0:4]
            if len(color) == 9:
                color = color[0:7]

            self.color = color
            self.line_style = line_style
            self.direction = direction
            self.line_width = line_width
            self.lines = lines
            self.arrow = arrow
            renpy.Displayable.__init__(self, **kwargs)

        def render(self, width, height, st, at):
            import math
            render = None
            max_x = max(map(lambda position : position[0], self.lines)) + (2*self.line_width if self.direction in ["up", "down"] else self.line_width)
            max_y = max(map(lambda position : position[1], self.lines)) + (2*self.line_width if self.direction in ["right", "left"] else self.line_width)
            render = renpy.Render(max_x, max_y)
            canvas = render.canvas()
            if self.line_style == "dashed":
                line_len = 0
                for i in range(1, len(self.lines)):
                    p1, p2 = self.lines[i-1], self.lines[i]
                    dist = math.hypot(p2[0]-p1[0], p2[1]-p1[1])
                    draw_dashed_line(canvas, self.color, p1, p2, self.line_width, 10)
                    line_len += dist
            else:
                canvas.lines(self.color, False, self.lines, width=self.line_width)

            if self.arrow:
                point_x = self.lines[-1][0]
                point_y = self.lines[-1][1]
                if self.direction == "down":
                    canvas.polygon(self.color,  [[point_x, point_y], [point_x - 2*self.line_width, point_y - 3*self.line_width], [point_x + 2*self.line_width, point_y - 3*self.line_width]])
                if self.direction == "left":
                    canvas.polygon(self.color,  [[point_x, point_y], [point_x + 3*self.line_width, point_y - 2*self.line_width], [point_x + 3*self.line_width, point_y + 2*self.line_width]])
                if self.direction == "right":
                    canvas.polygon(self.color,  [[point_x, point_y], [point_x - 3*self.line_width, point_y - 2*self.line_width], [point_x - 3*self.line_width, point_y + 2*self.line_width]])
                
            return render

    """
    Custom line that also has text label
    The text is positioned in the middle of the line
    X position is calculated from min and max X coordinates and it's placed in such a way that the text center is in the middle between these coordinates
    Y position is either also in the middle (for vertical lines) or if you have a horizontal line, it's wherever the horizontal line is
    """
    class Crabs_Labeled_Line(Crabs_Line):
        def __init__(self, color="#000000", lines=[], direction="down", line_style="solid", line_width=3, label="", arrow=True, **kwargs):
            super(Crabs_Labeled_Line, self).__init__(color, lines, direction, line_style, line_width, arrow, **kwargs)
            self.label = label

        def render(self, width, height, st, at):
            line = Crabs_Line(self.color, self.lines, self.direction, self.line_style, self.line_width, self.arrow)

            max_x = max(map(lambda position : position[0], self.lines))
            min_x = min(map(lambda position : position[0], self.lines))
            median_y = median([position[1] for position in self.lines])
            if not self.label:
                return line.render(width, height, st, at)
            else:
                text_displayable = Text(self.label, color="#ffffff", size=18, text_xcenter=0.5, ycenter=0.5, outlines=[ (absolute(2), "#000000", absolute(0), absolute(0)) ])

                # The position of text, should be in the middle of the arrow on the horizontal line
                x_text_pos = (max_x + min_x) // 2 - text_displayable.size()[0]//2
                y_text_pos = median_y - text_displayable.size()[1]//2

                return Composite((width, height), (0, 0), line, (int(x_text_pos), int(y_text_pos)), text_displayable).render(width, height, st, at)

    # finds a font size that fits the text within text_xsize pixels
    def get_text_size(text, text_xsize):
        text_xsize = text_xsize - 30
        text_size = 22
        text_width = Text(text, size=text_size, xsize=text_xsize).size()[0]
        text_height = Text(text, size=text_size, xsize=text_xsize).size()[1]

        # RenPy sometimes does two lines even if width is lower than xsize, probably because of outlines
        # so make sure the text width is at least 5 pixels lower than xsize
        # if height is less or equal 17, then we can do two lines because all buttons have ysize of 133, which gives ~35 pixels of space
        # please don't use text so long that it would take 3 lines
        while (text_width >= text_xsize - 5 and text_height > 17):
            text_size -= 1
            text_width = Text(text, size=text_size, xsize=text_xsize).size()[0]
            text_height = Text(text, size=text_size, xsize=text_xsize).size()[1]
        return text_size

#Calendar with buttons for days 1-28
screen crabs_calendar():
    key ["K_ESCAPE"] action [Hide("crabs_calendar"), If(show_clock, true=Show("clock"))]

    modal not crabs_tutorial
    frame:
        xalign 0.5
        yalign 0.5
        background "#aaaa"
        vpgrid cols 7 rows 4:
            #Since there's no Day1 image, use main menu from the game start for the first day
            button:
                add "gui/main_menu1.png" zoom 0.1
                xsize 200
                ysize 200
                action [ Hide("crabs_calendar"), Show("crabs_day1")]
                at hover_purple_tint
            for i in range(2, 29):
                #Buttons should only be active if there is a DayX image and I have defined a screen with a flowchart for this day 
                if (renpy.loadable("images/Day%s.jpg" % i) or renpy.loadable("images2/Day%s.jpg" % i)) and renpy.has_screen("crabs_day%s" %i):
                    button:
                        add "Day[i]" zoom 0.1
                        xsize 200
                        ysize 200
                        action [ Hide("crabs_calendar"), Show("crabs_day%s" %i)]
                        at hover_purple_tint
                #Otherwise, if main_menu3 exists, use it for week 3+, if it doesn't, use main_menu2
                else:
                    $ button_img = "gui/main_menu3.png" if i >= 15 and renpy.loadable("gui/main_menu3.png") else "gui/main_menu2.png" if i >= 8 else "gui/main_menu1.png"
                    button:
                        add button_img zoom 0.1
                        xsize 200
                        ysize 200
                        action NullAction()
                        at hover_purple_tint

#Buttons to switch between days when the flowchart is open, and ESC hint 
screen crabs_days(day):
    vbox:
        text "Press ESC or click the logo again to hide the flowchart" size 18 color "#000000"
        hbox:
            for i in range(1, 29):
                if renpy.has_screen("crabs_day%s" %i):
                    $ is_selected = i == day
                    textbutton "Day [i]" action [Hide("crabs_day%s" %day), Show("crabs_day%s" %i)] text_size 15 selected is_selected text_selected_color "#3d86c6"


screen graph(graph):
    # big brain graph theory calls this stuff "Layered graph drawing"
    # very primitive version of it because I don't really want to implement GraphViz in RenPy
    # 1. put scenes in layers so that arrows can only go down (kinda annoying limitation because in some cases I think it would look better if they could go sideways but oh well)
    # if there's an arrow that goes through multiple layers
    # (for example an arrow from day 2 Trish scene into evening decision),
    # place fake buttons with vertical lines as background. Not sure why the guy who designed this algorithm did it this way but it sure makes coding easier
    $ graph_layers = assign_layers(graph)
    # 2. order scenes on the same layer so that arrows don't cross
    # basically orders scenes on one layer in the same order as the scenes which have arrows to them
    # and if two scenes have arrows coming from the same scene, order them in the same order as in that scene "connections" list
    # Ideally it should prevent crosses but if you want to be completely sure then the graph should contain scenes in "left-to-right, top-to-bottom" order
    $ order_layer(graph_layers, graph)
    # 3. assign absolute position which will be used in xpos and ypos parameters for a button
    # places each scene approximately between the scenes it has arrows to
    $ assign_position(graph, graph_layers)
    fixed:
        # Place buttons
        for node in graph:
            $ x_coord = int(node.position.x)
            $ y_coord = int(node.position.y)
            $ img = node.img
            $ txt = node.text
            $ crabs_hbox = HBox()
            $ button_xsize = 173
            # for the case when there's more than 1 background color or image
            if not isinstance(node.img, str) and not isinstance(node.img, Crabs_Line):
                $ button_xsize = 173 if isinstance(img, str) else len(img) * 173 + 5 * (len(img) - 1)
            if not isinstance(node.color, str):
                for clr in node.color:
                    $ crabs_hbox.add(Solid(clr, xsize=button_xsize//len(node.color) + 5 if button_xsize > 173 else button_xsize//len(node.color)))

            button:
                ysize 133
                id "{}".format(node.id)
                xpos x_coord
                ypos y_coord
                action NullAction()
                xcenter 0.5
                hovered CaptureFocus("{}".format(node.id))
                unhovered ClearFocus("{}".format(node.id))
                padding (0,0)
                background crabs_hbox
                if not isinstance(node.color, str):
                    background crabs_hbox
                else:
                    background node.color
                if not isinstance(img, Crabs_Line):
                    at hover_purple_tint
                if not node.condition and not isinstance(img, Crabs_Line):
                    at sepia_tint
                vbox:
                    # when there's a long line, it actually places fake buttons with vertical lines as images
                    # no need to zoom and no text there
                    if isinstance(img, Crabs_Line):
                        add img
                    else:
                        # if not a single string, I expect list of images. guess I could just use a list with one element for a single image but meh, too many brackets already
                        if not isinstance(img, str):
                            hbox:
                                spacing 5
                                for i in img:
                                    add i zoom 0.09
                            $ text_xsize = len(img) * 173
                            # really doubt there will be such a long text it would need smaller font
                            $ text_size = 22
                        else:
                            add img zoom 0.09 xcenter 0.5
                            $ text_xsize = 173

                            $ text_size = get_text_size(node.text, text_xsize)

                        text node.text:
                            color "#FFFFFF"
                            size text_size
                            xsize text_xsize
                            xcenter 0.5
                            ycenter 0.5
                            outlines [ (absolute(2), "#000000", absolute(0), absolute(0)) ]

        # Draw arrows
        for node in graph:
            if len(node.connections) > 0:
                # Done from the sides to the center because if I draw arrows with text from left to right,
                # the text on the middle arrow will be under the right arrow
                for i in range(0, len(node.connections)//2 + 1):
                    $ connection = node.connections[i]
                    $ target = [x for x in graph if x.id == connection.id][0]

                    $ line_points = get_line_points(node, target, graph)

                    $ txt = connection.text
                    $ target_fake = isinstance(target.img, Crabs_Line)

                    $ clr = target.color
                    if not isinstance(clr, str):
                        if node.color in clr:
                            $ clr = node.color
                        else:
                            $ clr = clr[0]
                    add Crabs_Labeled_Line(color="{}".format(clr), arrow=not target_fake, label=txt, lines=line_points)
                    # copypaste for the arrow on the right side
                    if node.connections[i] != node.connections[-i - 1]:
                        $ connection = node.connections[-i - 1]
                        $ target = [x for x in graph if x.id == connection.id][0]

                        $ line_points = get_line_points(node, target, graph)

                        $ txt = connection.text
                        $ target_fake = isinstance(target.img, Crabs_Line)
                        $ clr = target.color
                        if not isinstance(clr, str):
                            if node.color in clr:
                                $ clr = node.color
                            else:
                                $ clr = clr[0]

                        add Crabs_Labeled_Line(color="{}".format(clr), arrow=not target_fake, label=txt, lines=line_points)


screen crabs_wt_overlay():
    #zorder to keep it showing after toggling the screen
    zorder 100

    if show_wt:
        key ["w","W"] action [ If(show_clock, true=ToggleScreen("clock")), If(renpy.get_screen("flowchart"), false=ToggleScreen("crabs_calendar")), Hide("flowchart")]

        key ["c","C"] action [ShowMenu("characters")]
        hbox:
            xalign 1.0
            yalign 0.0
            spacing 20
            button:
                keyboard_focus False
                add "title" crop (1320, 720, 600, 400) zoom 0.25
                action [ ToggleScreen("clock"),  If(renpy.get_screen("flowchart"), false=ToggleScreen("crabs_calendar")), Hide("flowchart")]


#Hover tint transform for buttons
transform hover_purple_tint:
    on idle:
        matrixcolor None
    on hover:
        matrixcolor TintMatrix("#aaf")

transform sepia_tint:
    matrixcolor SepiaMatrix()