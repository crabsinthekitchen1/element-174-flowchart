screen crabs_day11():
    modal True
    tag flowchart
    add "#aaaa" xsize 1920 ysize 1080
    key ["K_ESCAPE"] action [Hide("crabs_day11"), If(show_clock, true=Show("clock"))]
    key ["shift_K_LEFT"] action [Hide("crabs_day11"), Show("crabs_day10")]
    key ["shift_K_RIGHT"] action [Hide("crabs_day11"), Show("crabs_day12")]
    $ crabs_current_time = crabs_current_time_hashcode()

    $ graph = [Crabs_Node(id="start", text=" ", connections=[Crabs_Connection("caroline"), Crabs_Connection("morning")], img="Day11", condition=gamedate >= 11),
        Crabs_Node(id="caroline", text="Went to Caroline's apartment", color="#b11226", connections=[Crabs_Connection("morning")], img="ApartmentCarolineSleep3", condition=day10_evening=="scene_caroline_apartment" and not flag_caroline_apartment_decline and gamedate >= 11),
        Crabs_Node(id="morning", text="Morning", connections=[Crabs_Connection("emma", "Find Emma"), Crabs_Connection("rachel", "Visit Rachel's dorm"), Crabs_Connection("sarah", "Give Sarah her photo"), Crabs_Connection("chloe", "Contact Chloe")], img="BedroomZoeySleep", condition=gamedate >= 11),
        Crabs_Node(id="emma", text="Emma + 1", color="#ff8243", connections=[Crabs_Connection("emmamen")], img="ClosetEmmaKiss", condition=day11_morning == "scene_emma_closet"),
        Crabs_Node(id="rachel", text="Rachel + 1", color="#0038a8", connections=[Crabs_Connection("afternoon")], img="DormRachelNervous", condition=day11_morning == "scene_rachel_explain"),
        Crabs_Node(id="sarah", text="Sarah + 1", color="#a8e4a0", connections=[Crabs_Connection("afternoon")], img="RooftopSarahKiss", condition=day11_morning == "scene_sarah_secret"),
        Crabs_Node(id="chloe", text="Chloe + 1", color="#fc89ac", connections=[Crabs_Connection("afternoon")], img="TrailerChloeTouch", condition=day11_morning == "scene_chloe_confront"),
        Crabs_Node(id="emmamen", text="Two men asking about Emma", color="#ff8243", connections=[Crabs_Connection("afternoon")], img="CollegeMenSerious", condition=flag_emma_men),
        Crabs_Node(id="afternoon", text="Afternoon", connections=[Crabs_Connection("isabella", "Lunch with Isabella"), Crabs_Connection("ash", "Thursday club meeting"), Crabs_Connection("jules", "Check up on Jules"), Crabs_Connection("jenna", "Drop off Jenna's bag")], img="CollegeOutside-Afternoon", condition=crabs_current_time >= 42),
        Crabs_Node(id="isabella", text="Isabella + 1", color="#FF3EA5", connections=[Crabs_Connection("evening")], img="RestaurantIsabellaSad", condition=day11_afternoon == "scene_isabella_lunch"),
        Crabs_Node(id="jules", text="Jules + 1", color="#ffd300", connections=[Crabs_Connection("evening")], img="BedroomJulesSleep", condition=day11_afternoon == "scene_jules_secret"),
        Crabs_Node(id="ash", text="Ash + 1", color="#4a646c", connections=[Crabs_Connection("evening")], img="ClubAshSmile7", condition=(flag_ash_secret and not day10_afternoon == "scene_ash_secret") or (flag_ash_liefor and not day12_afternoon == "scene_ash_liefor")),
        Crabs_Node(id="jenna", text="Jenna + 1", color="#253529", connections=[Crabs_Connection("evening")], img="BedroomJennaAsh2", condition=flag_jenna_ash),
        Crabs_Node(id="evening", text="Evening", connections=[Crabs_Connection("malia", "Visit Malia's house"), Crabs_Connection("katy", "Visit Katy's house"), Crabs_Connection("zoey", "See Zoey early"), Crabs_Connection("chloemask", "Chloe's house")], img=["CollegeOutside-Afternoon","ThomasLivingRoom","GrandHallAlone","NeighborhoodEvening"], condition=crabs_current_time >= 43),
        Crabs_Node(id="chloemask", text="Chloe + 1", color="#fc89ac", connections=[Crabs_Connection("stranger")], img="TrailerChloeNervous", condition=day11_evening == "scene_chloe_secret"),
        Crabs_Node(id="katy", text="Katy + 1", color="#592720", connections=[Crabs_Connection("stranger")], img="BedroomKatyKiss", condition=day11_evening == "scene_katy_house"),
        Crabs_Node(id="zoey", text="Zoey + 1", color="#d8b2d1", connections=[Crabs_Connection("stranger")], img="LivingZoeyNervous", condition=day11_evening == "scene_zoey_house" or flag_zoey_house),
        Crabs_Node(id="malia", text="Malia + 1", color="#860111", connections=[Crabs_Connection("stranger")], img="BedroomMaliaKiss", condition=day11_evening=="scene_malia_house"),
        Crabs_Node(id="stranger", text="Stranger attack", connections=[], img="RoofStrangerSpecial3", condition="Zoey fought her on the rooftop" in desc_zoey),
        ]
    $ screen_size = calc_graph_size(graph)
    ###Flowchart
    viewport:
        scrollbars "vertical"
        mousewheel True
        arrowkeys True
        pagekeys True
        vbox:
            xsize 1920
            ysize screen_size
            use crabs_days(11)
            use graph(graph)


    ### Tooltips
    nearrect:
        focus "emma"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 50
            add "#ff8243"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if seen Emma on day 8 or day 9" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "emmamen"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 75
            add "#ff8243"
            vbox:
                xcenter 0.5
                spacing 10
                text "Required scene for Emma that triggers automatically if you've seen either her classroom or closet scene" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "rachel"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 50
            add "#0038a8"
            vbox:
                xcenter 0.5
                spacing 10
                text "Required scene for Rachel if you missed it on day 10" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "sarah"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 50
            add "#a8e4a0"
            vbox:
                xcenter 0.5
                spacing 10
                text "Required scene for Sarah which can be seen on day 12" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "chloe"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 175
            add "#fc89ac"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if seen Chloe on day 9" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Required scene for Chloe which can be seen on day 12" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Adds points for watching SluttyDemon streams on day 2 or day 8" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "chloemask"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 50
            add "#fc89ac"
            vbox:
                xcenter 0.5
                spacing 10
                text "Required scene for Chloe if you missed it on day 9" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "isabella"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 50
            add "#FF3EA5"
            vbox:
                xcenter 0.5
                spacing 10
                text "Required scene for Isabella if you missed it on day 10" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "jules"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 125
            add "#FFd300"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if went to Jules's mansion on day 9" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Required scene for Jules which can be seen on day 12" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "ash"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 200
            add "#4a646c"
            vbox:
                xcenter 0.5
                spacing 10
                text "Required scene for Ash which can be seen on day 12" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                add "ClubAshSpace" zoom 0.09 xcenter 0.5
                text "Day 10 scene if you missed it" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "jenna"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 125
            add "#253529"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if seen Jenna on day 8 or day 9" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Potential start of the subroute with Ash and Jenna" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#4a646c", absolute(0), absolute(0)) ]

    nearrect:
        focus "katy"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 50
            add "#592720"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if Katy has 4+ points and you've seen her on day 6" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "malia"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 125
            add "#860111"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if seen Malia on day 8 or day 10" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Required scene for Malia which can be seen on day 12" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "zoey"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 175
            add "#d8b2d1"
            vbox:
                xcenter 0.5
                spacing 10
                null
                add "LivingZoeyHandjob" zoom 0.09 xcenter 0.5
                text "Handjob available if Zoey has 5+ points" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
