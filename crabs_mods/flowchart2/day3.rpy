screen crabs_day3():
    modal True
    tag flowchart
    add "#aaaa" xsize 1920 ysize 1080
    key ["K_ESCAPE"] action [Hide("crabs_day3"), If(show_clock, true=Show("clock"))]
    key ["shift_K_LEFT"] action [Hide("crabs_day3"), Show("crabs_day2")]
    key ["shift_K_RIGHT"] action [Hide("crabs_day3"), Show("crabs_day4")]

    ###Flowchart
    viewport:
        scrollbars "vertical"
        mousewheel True
        arrowkeys True
        pagekeys True

        $ graph = [Crabs_Node(id="start", text=" ", connections=[Crabs_Connection("zoeyintro", "Ignored Zoey"), Crabs_Connection("newgirl")], img="Day3", condition=gamedate >= 3),
        Crabs_Node(id="zoeyintro", text="Meet Zoey", connections=[Crabs_Connection("newgirl")], img="ZoeyAsk", condition=gamedate >= 3 and "ignored her" in desc_zoey),
        Crabs_Node(id="newgirl", text="Meet Penny", connections=[Crabs_Connection("zoey", "Sorry, professor"), Crabs_Connection("penny", "Sure, no problem")], img="NewGirl2", condition="When Penny arrived" in desc_schafer),
        Crabs_Node(id="zoey", text="Zoey + 1", color="#d8b2d1", connections=[Crabs_Connection("english")], img="EnglishZoeyAsk", condition=flag_day3_zoey),
        Crabs_Node(id="penny", text="Penny + 1", color="#602f6b", connections=[Crabs_Connection("english")], img="PennyGrin", condition=flag_day3_penny),
        Crabs_Node(id="english", text="English class", connections=[Crabs_Connection("jenna1", "Joined the ero-games club"), Crabs_Connection("afternoon", "Did neither"), Crabs_Connection("holmes1", "Punched Caleb")], img="EnglishPennySit", condition=flag_day3_zoey or flag_day3_penny),
        Crabs_Node(id="jenna1", text="Jenna + 1", color="#253529", connections=[Crabs_Connection("jenna2", "Tell him"), Crabs_Connection("sarah", "Don't tell him")], img="ClubJennaHappy", condition="You met Jenna inside the ero-games club room" in desc_jenna),
        Crabs_Node(id="holmes1", text="Holmes + 1", color="#135DD8", connections=[Crabs_Connection("holmes2", "I started it"), Crabs_Connection("trish", "He started it")], img="OutsideHolmesAngry", condition="he ambushed you" in desc_caleb),
        Crabs_Node(id="afternoon", text="After lessons", connections=[Crabs_Connection("rachel", "Explore college"), Crabs_Connection("isabella", "Go home")], img="CollegeHallway02", condition=flag_day3_rachel or flag_day3_isabella),
        Crabs_Node(id="jenna2", text="Jenna + 1", color="#253529", connections=[Crabs_Connection("evening")], img="ClubJoeyDisappointed", condition=flag_day3_told),
        Crabs_Node(id="sarah", text="Sarah + 1", color="#a8e4a0", connections=[Crabs_Connection("evening")], img="SarahHeadLap", condition=flag_day3_notold),
        Crabs_Node(id="isabella", text="Isabella + 1", color="#FF3EA5", connections=[Crabs_Connection("evening")], img="ParkIsabellaWhisper", condition=flag_day3_isabella),
        Crabs_Node(id="rachel", text="Rachel + 1", color="#0038a8", connections=[Crabs_Connection("evening")], img="DormRachelSmile", condition=flag_day3_rachel),
        Crabs_Node(id="holmes2", text="Holmes + 1", color="#135DD8", connections=[Crabs_Connection("evening")], img="OutsideHolmesSmile", condition=flag_day3_blamed_self),
        Crabs_Node(id="trish", text="Trish + 1", color="#2e1a47", connections=[Crabs_Connection("evening")], img="OutsideTrishThumbsup", condition=flag_day3_blamed_caleb),

        Crabs_Node(id="evening", text="Evening", connections=[Crabs_Connection("katy", "See if Katy is working"), Crabs_Connection("emma", "Order pizza"), Crabs_Connection("jules", "Explore downtown"), Crabs_Connection("madison", "Look for macarons")], img="NeighborhoodEvening", condition=flag_day3_katy or flag_day3_emma or flag_day3_jules or flag_day3_madison),
        Crabs_Node(id="katy", text="Katy + 1", color="#592720", connections=[], img="CafeKatyFlirty2", condition=flag_day3_katy),
        Crabs_Node(id="emma", text="Emma + 1", color="#ff8243", connections=[], img="HomeEmmaPizza2", condition=flag_day3_emma),
        Crabs_Node(id="jules", text="Jules + 1", color="#ffd300", connections=[], img="QuinceJulesSmug", condition=flag_day3_jules),
        Crabs_Node(id="madison", text="Madison + 1", color="#d470a2", connections=[], img="PastryMadisonSmile2", condition=flag_day3_madison),
        ]
        $ screen_size = calc_graph_size(graph)

        vbox:
            xsize 1920
            ysize screen_size
            use crabs_days(3)
            use graph(graph)


    ###Tooltips
    nearrect:
        focus "isabella"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 50
            add "#FF3EA5"
            vbox:
                xcenter 0.5
                spacing 10
                text "Required to see Isabella on day 6 if haven't met Katy" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "jenna1"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 50
            add "#253529"
            vbox:
                xcenter 0.5
                spacing 10
                text "Required to keep Jenna's route open by the end of the first week" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "holmes1"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 50
            add "#135DD8"
            vbox:
                xcenter 0.5
                spacing 10
                text "Required to see Holmes on day 6" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "sarah"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 150
            add "#a8e4a0"
            vbox:
                xcenter 0.5
                spacing 10
                null
                add "SarahGreet" zoom 0.09 xcenter 0.5
                text "Day 2 scene if haven't met Sarah" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "rachel"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 150
            add "#0038a8"
            vbox:
                xcenter 0.5
                spacing 10
                null
                add "LibraryRachelPoint" zoom 0.09 xcenter 0.5
                text "Day 2 scene if haven't met Rachel" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "katy"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 50
            add "#592720"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if met Katy on day 2" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]                

    nearrect:
        focus "emma"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 75
            add "#FF8243"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if haven't met Katy" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Shorter scene if haven't met Emma" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "madison"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 50
            add "#d470a2"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if met Madison on day 2" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "jules"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 50
            add "#ffd300"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if haven't met Madison" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

