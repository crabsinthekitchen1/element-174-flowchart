screen crabs_day2():
    modal True
    tag flowchart
    add "#aaaa" xsize 1900 ysize 1080
    key ["K_ESCAPE"] action [Hide("crabs_day2"), If(show_clock, true=Show("clock"))]
    key ["shift_K_LEFT"] action [Hide("crabs_day2"), Show("crabs_day1")]
    key ["shift_K_RIGHT"] action [Hide("crabs_day2"), Show("crabs_day3")]

    $ graph = [Crabs_Node(id="start", text=" ", connections=[Crabs_Connection("text", "Punched Caleb"), Crabs_Connection("hallway", "Didn't punch Caleb")], img="Day2", condition=gamedate >= 2),
        Crabs_Node(id="text", text="Text from Emma", connections=[Crabs_Connection("emma", "Text back"), Crabs_Connection("malia", "Ignore the text")], img="CollegeHallwayPhone", condition=flag_day1_punch),
        Crabs_Node(id="hallway", text="Art class", connections=[Crabs_Connection("jules", "Attend art class"), Crabs_Connection("rachel1", "Skip the class")], img="CollegeHallway03", condition="walked away" in desc_caleb),
        Crabs_Node(id="emma", text="Emma + 1", color="#ff8243", connections=[Crabs_Connection("day2afternoon")], img="EmmaShudder", condition=day2_met_emma),
        Crabs_Node(id="malia", text="Malia + 1", color="#860111", connections=[Crabs_Connection("day2afternoon")], img="MaliaPeek", condition=day2_met_malia),
        Crabs_Node(id="rachel1", text="Rachel + 1", color="#0038a8", connections=[Crabs_Connection("day2afternoon")], img="RachelPoint", condition=flag_day2_rachel),
        Crabs_Node(id="jules", text="Jules + 1", color="#ffd300", connections=[Crabs_Connection("day2afternoon")], img="JulesAnnoyed", condition=flag_day2_jules),
        Crabs_Node(id="day2afternoon", text="Day 2 afternoon", connections=[Crabs_Connection("zoeyintro", "Gave Zoey a pen"), Crabs_Connection("tutoring", "Ignored Zoey")], img="CollegeHallway05", condition=day2_met_emma or day2_met_malia or flag_day2_rachel or flag_day2_jules),
        Crabs_Node(id="zoeyintro", text="Zoey intro", connections=[Crabs_Connection("zoey", "Check out the city"), Crabs_Connection("zoeydecline", "Decline")], img="ZoeyAsk", condition=flag_day2_zoey or flag_day2_ash or flag_day2_isabella),
        Crabs_Node(id="tutoring", text="Tutoring", connections=[Crabs_Connection("madison", "Study history"), Crabs_Connection("trish1", "Study science")], img="BulletinBoard", condition=flag_day2_madison or flag_day2_trish),
        Crabs_Node(id="zoey", text="Zoey + 1", color="#d8b2d1", connections=[Crabs_Connection("katy1")], img="ZoeyHappy2", condition=flag_day2_zoey),
        Crabs_Node(id="zoeydecline", text="Declined Zoey", connections=[Crabs_Connection("ash1", "Joined the club"), Crabs_Connection("isabella1", "Didn't join the club")], img="ZoeyDisappointed", condition=flag_day2_ash or flag_day2_isabella),
        Crabs_Node(id="madison", text="Madison + 1", color="#d470a2", connections=[Crabs_Connection("aftermadison")], img="MadisonSit2", condition=flag_day2_madison),
        Crabs_Node(id="trish1", text="Trish + 1", color="#2e1a47", connections=[Crabs_Connection("day2night")], img="TrishWave", condition=flag_day2_trish),
        Crabs_Node(id="katy1", text="Katy + 1", color="#592720", connections=[Crabs_Connection("day2night")], img="ZoeyIntroduce", condition=flag_day2_zoey),
        Crabs_Node(id="ash1", text="Ash + 1", color="#4a646c", connections=[Crabs_Connection("ash2", "Take the blame"), Crabs_Connection("chloe", "Act offended")], img="AshChloePoint", condition=flag_day2_ash),
        Crabs_Node(id="isabella1", text="Isabella + 1", color="#FF3EA5", connections=[Crabs_Connection("isabella2", "Sharks"), Crabs_Connection("sarah", "Reapers")], img="IsabellaExcited", condition=flag_day2_isabella),
        Crabs_Node(id="aftermadison", text="After tutoring", connections=[Crabs_Connection("sarah", "Go home"), Crabs_Connection("rachel2", "Go to library")], img="CollegeOutside-AfternoonB", condition=flag_day2_madison),
        Crabs_Node(id="ash2", text="Ash + 1", color="#4a646c", connections=[Crabs_Connection("day2night")], img="AshJoeyExcited", condition=flag_day2_ash2),
        Crabs_Node(id="chloe", text="Chloe + 1", color="#fc89ac", connections=[Crabs_Connection("day2night")], img="ChloeUnsure", condition=flag_day2_chloe),
        Crabs_Node(id="isabella2", text="Isabella + 1", color="#FF3EA5", connections=[Crabs_Connection("day2night")], img="IsabellaSmile", condition=flag_day2_isabella2),
        Crabs_Node(id="sarah", text="Sarah + 1", color="#a8e4a0", connections=[Crabs_Connection("day2night")], img="SarahGreet", condition=flag_day2_sarah or flag_day2_no_library),
        Crabs_Node(id="rachel2", text="Rachel + 1", color="#0038a8", connections=[Crabs_Connection("day2night")], img="LibraryRachelCurious", condition=flag_day2_rachel2),
        Crabs_Node(id="day2night", text="Day 2 evening", connections=[Crabs_Connection("laptop", "Met Katy or Trish")], img="HomeInside-Night", condition=flag_day2_zoey or flag_day2_isabella2 or flag_day2_sarah or flag_day2_rachel2 or flag_day2_no_library or flag_day2_trish or flag_day2_ash2 or flag_day2_chloe),
        Crabs_Node(id="laptop", text="Browse internet", connections=[Crabs_Connection("katy2", "Doom metal"), Crabs_Connection("sluttydemon", "Watch camgirls"), Crabs_Connection("trish2", "Research")], img="HomeComputer", condition=flag_day2_katy2 or flag_day2_camgirl or flag_day2_trish2),
        Crabs_Node(id="katy2", text="Katy + 1", color="#592720", connections=[], img="HomeComputerMetal", condition=flag_day2_katy2),
        Crabs_Node(id="trish2", text="Trish + 1", color="#2e1a47", connections=[], img="HomeComputerScience", condition=flag_day2_trish2),
        Crabs_Node(id="sluttydemon", text="Watched SluttyDemon", color="#fc89ac", connections=[], img="SluttyDemon3", condition=flag_day2_camgirl),
        ]

    ###Flowchart
    viewport:
        scrollbars "vertical"
        mousewheel True
        arrowkeys True
        pagekeys True
        $ screen_size = calc_graph_size(graph)
        vbox:
            xsize 1900
            ysize screen_size

            use crabs_days(2)
            use graph(graph)

    ###Tooltips
    nearrect:
        focus "trish2"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 50
            add "#2e1a47"
            vbox:
                xcenter 0.5
                spacing 10
                text "Only available if met Trish" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "katy1"
        prefer_top True
        fixed:
            xcenter 0.5

            xmaximum 375
            ymaximum 75
            add "#592720"
            vbox:
                xcenter 0.5
                spacing 10
                text "Required to see Katy on day 3" size 18 xalign 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Blocks seeing Emma on day 3" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "katy2"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 50
            add "#592720"
            vbox:
                xcenter 0.5
                spacing 10
                text "Only available if met Katy" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "sluttydemon"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 75
            add "#fc89ac"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if met Katy or Trish" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Alternative point for Chloe on week 2" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
    
    nearrect:
        focus "madison"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 75
            add "#d470a2"
            vbox:
                xcenter 0.5
                spacing 10
                text "Required to see Madison on day 3" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Blocks seeing Jules on day 3" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "isabella1"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 175
            add "#FF3EA5"
            vbox:
                xcenter 0.5
                spacing 10
                null
                add "SoccerField-Afternoon01006" zoom 0.09 xcenter 0.5
                text "Day 1 introduction if haven't met Isabella" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
    nearrect:
        focus "rachel2"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 175
            add "#0038a8"
            vbox:
                xcenter 0.5
                spacing 10
                null
                add "LibraryRachelPoint" zoom 0.09 xcenter 0.5
                text "Different dialogue if haven't met Rachel in the morning" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

