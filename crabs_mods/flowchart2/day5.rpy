screen crabs_day5():
    modal True
    tag flowchart
    add "#aaaa" xsize 1920 ysize 1080
    key ["K_ESCAPE"] action [Hide("crabs_day5"), If(show_clock, true=Show("clock"))]
    key ["shift_K_LEFT"] action [Hide("crabs_day5"), Show("crabs_day4")]
    key ["shift_K_RIGHT"] action [Hide("crabs_day5"), Show("crabs_day6")]
    ###Flowchart
    viewport:
        scrollbars "vertical"
        mousewheel True
        arrowkeys True
        pagekeys True
        $ graph = [Crabs_Node(id="start", text=" ", connections=[Crabs_Connection("bully", "Bullied Jenna or Madison"), Crabs_Connection("classroom", "Didn't bully")], img="Day5", condition=gamedate >= 5),
        Crabs_Node(id="bully", text="Sent to Caroline's office", connections=[Crabs_Connection("caroline", "Go to Caroline's office"), Crabs_Connection("malia", "Think about it")], img="EnglishSchaferTell", condition="bullying incident" in desc_schafer),
        Crabs_Node(id="classroom", text="Classroom", connections=[Crabs_Connection("chloe", "Watch video"), Crabs_Connection("penny", "Sit down")], img="EnglishSchaferChloe", condition=flag_day5_chloe or flag_day5_penny),
        Crabs_Node(id="penny", text="Penny + 1", color="#602f6b", connections=[Crabs_Connection("madisonschafer")], img="EnglishPennyTired2", condition=flag_day5_penny),
        Crabs_Node(id="chloe", text="Chloe + 1", color="#fc89ac", connections=[Crabs_Connection("madisonschafer")], img="EnglishChloeDisgust", condition=flag_day5_chloe),
        Crabs_Node(id="caroline", text="Caroline + 1", color="#b11226", connections=[Crabs_Connection("madisonschafer")], img="DeansCarolineCoy", condition=flag_day5_caroline),
        Crabs_Node(id="malia", text="Malia + 1", color="#860111", connections=[Crabs_Connection("madisonschafer")], img="BeanMaliaSuck", condition=flag_day5_malia),
        Crabs_Node(id="madisonschafer", text="Madison and Schafer", connections=[Crabs_Connection("madison", "Eavesdrop"), Crabs_Connection("emma", "Leave them")], img="EnglishMadisonPeak", condition=flag_day5_emma or flag_day5_madison),
        Crabs_Node(id="madison", text="Madison + 1", color="#d470a2", connections=[Crabs_Connection("afternoon")], img="EnglishMadisonPeak4", condition=flag_day5_madison),
        Crabs_Node(id="emma", text="Emma + 1", color="#ff8243", connections=[Crabs_Connection("afternoon")], img="OutsideEmmaInnocent", condition=flag_day5_emma),
        Crabs_Node(id="afternoon", text="Afternoon", connections=[Crabs_Connection("nothing", "Joined the ero-games club"), Crabs_Connection("jennaash")], img="CollegeOutside-Afternoon", condition=flag_day5_madison or flag_day5_emma),
        Crabs_Node(id="nothing", text="Afternoon", connections=[Crabs_Connection("ash", "Go to ero-games club"), Crabs_Connection("sarah", "Do something else")], img="CollegeOutside-Afternoon", condition=(flag_day5_emma or flag_day5_madison) and flag_day1_club),
        Crabs_Node(id="jennaash", text="Meet Ash and Jenna", connections=[Crabs_Connection("evening")], img="HallwayAshJennaSmile", condition="you met Ash while she was looking for club recruits" in desc_ash),
        Crabs_Node(id="ash", text="Ash + 1", color="#4a646c", connections=[Crabs_Connection("evening")], img="ClubAshSmile2", condition=flag_day5_ash),
        Crabs_Node(id="sarah", text="Sarah + 1", color="#a8e4a0", connections=[Crabs_Connection("evening")], img="LibrarySarahSmug", condition=flag_day5_sarah),
        Crabs_Node(id="evening", text="Evening", connections=[Crabs_Connection("jules", "Lie"), Crabs_Connection("holmes", "Give the information")], img="ClothingJulesChanging", condition="You ran into Jules at a clothing store downtown" in desc_jules),
        Crabs_Node(id="jules", text="Jules + 1", color="#ffd300", connections=[Crabs_Connection("maliaintro", "Haven't met Malia")], img="ClothingJulesLeave", condition=flag_day5_jules),
        Crabs_Node(id="holmes", text="Holmes + 1", color="#135DD8", connections=[Crabs_Connection("maliaintro", "Haven't met Malia")], img="ClothingHolmesSmirk", condition=flag_day5_holmes),
        Crabs_Node(id="maliaintro", text="Malia + 1", color="#860111", img="DowntownMaliaCoy", condition="You met Malia downtown after the Jules shoplifting incident" in desc_malia),
        ]
        $ screen_size = calc_graph_size(graph)

        vbox:
            xsize 1920
            ysize screen_size
            use crabs_days(5)
            use graph(graph)

    ###Tooltips
    nearrect:
        focus "madison"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 50
            add "#d470a2"
            vbox:
                xcenter 0.5
                spacing 10
                text "Required to keep Madison route open" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "jules"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 75
            add "#ffd300"
            vbox:
                xcenter 0.5
                spacing 10
                text "Required to keep Jules route open" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Required to see Jenna on day 7" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "malia"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 150
            add "#860111"
            vbox:
                xcenter 0.5
                spacing 10
                null
                add "AdminMaliaSmile" zoom 0.09 xcenter 0.5
                text "Day 4 scene if haven't met Malia" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "chloe"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 50
            add "#fc89ac"
            vbox:
                xcenter 0.5
                spacing 10
                text "Counts as watching SluttyDemon" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]