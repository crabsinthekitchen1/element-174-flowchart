screen crabs_day15():
    modal True
    tag flowchart
    add "#aaaa" xsize 1920 ysize 1080
    key ["K_ESCAPE"] action [Hide("crabs_day15"), If(show_clock, true=Show("clock"))]
    key ["shift_K_LEFT"] action [Hide("crabs_day15"), Show("crabs_day14")]
    key ["shift_K_RIGHT"] action If(renpy.has_screen("crabs_day16"), true=[Hide("crabs_day15"), Show("crabs_day16")])
    $ crabs_current_time = crabs_current_time_hashcode()

    $ graph = [Crabs_Node(id="start", text=" ", connections=[Crabs_Connection("elementroom")], img="Day15", condition=gamedate >= 15),
        Crabs_Node(id="elementroom", text="Talk with Zoey", connections=[Crabs_Connection("emmazoey", "Emma is at your house"), Crabs_Connection("morning")], img="ElementZoeyEnter", condition=gamedate >= 15),
        Crabs_Node(id="emmazoey", text="Zoey meets Emma", color="#ff8243", connections=[Crabs_Connection("morning")], img="HomeEmmaShake", condition=flag_emma_backdoor and not gone_emma),
        Crabs_Node(id="morning", text="Morning", connections=[Crabs_Connection("zoey", "Take Zoey upstairs"), Crabs_Connection("joey", "Get to the college")], img="HomeZoeySmile2", condition=gamedate >= 15),
        Crabs_Node(id="zoey", text="Zoey + 1", color="#d8b2d1", connections=[Crabs_Connection("joey")], img="BedroomZoeyKiss2", condition=day15_morning == "scene_zoey_late"),
        Crabs_Node(id="joey", text="Talk to Joey", connections=[Crabs_Connection("english")], img="CollegeJoeyExplain2", condition=gamedate >= 15),
        Crabs_Node(id="english", text="English class", connections=[Crabs_Connection("caroline", "Go to Caroline's office"), Crabs_Connection("jules", "Talk with Jules"), Crabs_Connection("malia", "Talk with Malia"), Crabs_Connection("sarah", "Talk with Sarah")], img="EnglishSchaferLecture3", condition=gamedate >= 15),
        Crabs_Node(id="caroline", text="Caroline + 1", color="#b11226", connections=[Crabs_Connection("afternoon")], img="DeansCarolineSmile5", condition=day15_morning == "scene_caroline_talk"),
        Crabs_Node(id="jules", text="Jules + 1", color="#ffd300", connections=[Crabs_Connection("afternoon")], img="ClassroomJulesCry2", condition=day15_morning == "scene_jules_talk"),
        Crabs_Node(id="malia", text="Malia + 1", color="#860111", connections=[Crabs_Connection("afternoon")], img="AutoMaliaKiss", condition=day15_morning == "scene_malia_element"),
        Crabs_Node(id="sarah", text="Sarah + 1", color="#a8e4a0", connections=[Crabs_Connection("afternoon")], img="ClosetSarahKiss", condition=day15_morning == "scene_sarah_element"),
        Crabs_Node(id="afternoon", text="Afternoon", connections=[Crabs_Connection("jenna", "Visit Jenna"), Crabs_Connection("chloe", "Go home with Chloe"), Crabs_Connection("holmes", "Coffee with Holmes"), Crabs_Connection("trish", "Visit Trish")], img="HallwayEmpty", condition=crabs_current_time >= 58),
        Crabs_Node(id="jenna", text="Jenna + 1", color="#253529", connections=[Crabs_Connection("evening")], img="LivingJennaKiss3", condition=day15_afternoon == "scene_jenna_bi"),
        Crabs_Node(id="holmes", text="Holmes + 1", color="#135DD8", connections=[Crabs_Connection("evening")], img="BeanHolmesSmile2", condition=day15_afternoon == "scene_holmes_leads"),
        Crabs_Node(id="chloe", text="Chloe + 1", color="#fc89ac", connections=[Crabs_Connection("evening")], img="TrailerChloeKiss", condition=day15_afternoon == "scene_chloe_blackmailed"),
        Crabs_Node(id="trish", text="Trish + 1", color="#2e1a47", connections=[Crabs_Connection("evening")], img="CampbellParentsTV", condition=day15_afternoon == "scene_trish_house"),
        Crabs_Node(id="evening", text="Evening", connections=[Crabs_Connection("ash", "Check on Ash"), Crabs_Connection("isabella", "Invite Isabella over"), Crabs_Connection("penny", "Text Penny")], img="HomeInside-Night2", condition=crabs_current_time >= 59),
        Crabs_Node(id="ash", text="Ash + 1", color="#4a646c", connections=[Crabs_Connection("night")], img="MotelAshSmile", condition=day15_evening == "scene_ash_motel"),
        Crabs_Node(id="isabella", text="Isabella + 1", color="#FF3eA5", connections=[Crabs_Connection("night")], img="HomeIsabellaKiss2", condition=day15_evening == "scene_isabella_invite"),
        Crabs_Node(id="penny", text="Penny + 1", color="#602f6b", connections=[Crabs_Connection("night")], img="HomePennyKiss2", condition=day15_evening == "scene_penny_text"),
        Crabs_Node(id="night", text="Night", connections=[Crabs_Connection("emmamadison", "Tell Emma about Madison"), Crabs_Connection("emmajules", "Have Jules meet Emma"), Crabs_Connection("besties", "Invite Katy, Rachel and Zoey")], img="HomeInside-Night2", condition=crabs_current_time >= 60),
        Crabs_Node(id="emmamadison", text="Emma/Madison subroute", color=["#ff8243", "#d470a2"], connections=[Crabs_Connection("emmajules", "Have Jules meet Emma")], img="HomeEmmaSurprised2", condition=flag_emmamadison_secrets),
        Crabs_Node(id="emmajules", text="Jules/Emma subroute", color=["#ffd300", "#ff8243"], img="HomeEmmaJules3", condition=flag_emmajules_meeting),
        Crabs_Node(id="besties", text="Katy/Zoey/Rachel subroute", color=["#592720", "#d8b2d1", "#0038a8"], img="HomeKatyRachelZoey2", condition=flag_katyrachel_meeting),
        ]
    $ screen_size = calc_graph_size(graph) + 50
    ###Flowchart
    viewport:
        scrollbars "vertical"
        mousewheel True
        arrowkeys True
        pagekeys True
        vbox:
            xsize 1920
            ysize screen_size
            spacing 10
            use crabs_days(15)
            text "Starting from week 3, you only get points if you choose to have sex with girls" color "#000" size 22 xcenter 0.5 yalign 0.5
            use graph(graph)

    nearrect:
        focus "elementroom"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 225
            add "#232323"
            vbox:
                xcenter 0.5
                spacing 10
                text "If you have no open routes at that point, the game will end" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                add "ElementZoeyConcerned" zoom 0.09 xcenter 0.5
                text "On Holmes route, telling Zoey the truth or lying will affect her route" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#135DD8", absolute(0), absolute(0)) ]

    nearrect:
        focus "zoey"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 125
            add "#d8b2d1"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if Zoey has 7+ points" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Telling Zoey that you love her might affect her route" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Blocks options after English class" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "english"
        prefer_top True
        grid 2 2:
            fixed:
                xcenter 0.5
                xmaximum 375
                ymaximum 300
                add "#d8b2d1"
                vbox:
                    xcenter 0.5
                    spacing 10
                    null
                    add "EnglishSchaferLecture2" zoom 0.09 xcenter 0.5
                    text "If you took Zoey upstairs, you will be late for class" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
            fixed:
                xcenter 0.5
                xmaximum 375
                ymaximum 300
                add "#fc89ac"
                vbox:
                    xcenter 0.5
                    spacing 10
                    null
                    add "EnglishChloeSad" zoom 0.09 xcenter 0.5
                    text "On Chloe's route, if you didn't take Zoey upstairs, you can sit with Chloe" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                    text "On Chloe's route, you will overhear other girls mentioning receiving the same letter" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
            fixed:
                xcenter 0.5
                xmaximum 375
                ymaximum 300
                add "#2e1a47"
                vbox:
                    xcenter 0.5
                    spacing 10
                    null
                    add "EnglishAllenDaniel7" zoom 0.09 xcenter 0.5
                    text "If you didn't take Zoey upstairs, you will hear Allen and Daniel talking about the Florist kidnapping another woman" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                    text "Different dialogue on Trish route and whether or not you went with her on day 13" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

            fixed:
                xcenter 0.5
                xmaximum 375
                ymaximum 300
                add "#d470a2"
                vbox:
                    xcenter 0.5
                    spacing 10
                    null
                    add "EnglishMadisonDefend" zoom 0.09 xcenter 0.5
                    text "On Madison route, there will be a small interaction between you, Schafer and Madison during the class" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "caroline"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 125
            add "#b11226"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if seen Caroline on day 14 and didn't reject her" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Can tell Caroline about the Element, which might affect her route" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "jules"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 125
            add "#ffd300"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if seen Jules on day 14 and didn't reject her" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Can tell Jules about the Element, which might affect her route" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "malia"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 150
            add "#860111"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if seen Malia on day 14 and didn't reject her" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Choosing to get a weapon or not when Malia suggests it might affect her route" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "sarah"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 225
            add "#a8e4a0"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if saved Sarah on day 14" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "If you're on Holmes route and told her about Sarah, Sarah will leave" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "If you decided to stay friends with Sarah, her route will end" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "The decision when Sarah says she wants a normal life might affect her route" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "jenna"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 225
            add "#253529"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if watched Pugman with Jenna on day 13 and kissed her afterwards" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Telling Joey about you and Jenna might affect Jenna's route" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "If you've seen Ash and Jenna on day 11, telling Jenna about it starts the subroute" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#4a646c", absolute(0), absolute(0)) ]

    nearrect:
        focus "holmes"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 75
            add "#135DD8"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if met Holmes on day 14 and told her about the Element (and everything else you know)" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "chloe"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 125
            add "#fc89ac"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if seen Chloe on day 14 and didn't reject her" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Can tell Chloe about the Element, which might affect her route" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "trish"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 250
            add "#2e1a47"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if went on patrol with Trish on day 13" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "If you took the capsule after the fight, you can tell Trish about it" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Can tell Trish about the Element, which might affect her route" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Florist's victim is either Katy or Isabella, depending on who has more points" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "ash"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 800
            ymaximum 275
            add "#4a646c"
            
            hbox:
                spacing 10
                vbox:
                    spacing 10
                    xsize 200
                    text "Available if went to arcade with Ash and lied to Holmes on day 14" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                    text "If seen Jenna in the afternoon and seen Jenna and Ash on day 11, shapeshifting into Jenna leads to a threesome suggestion" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#253529", absolute(0), absolute(0)) ]
                vbox:
                    xsize 200
                    spacing 10
                    xcenter 0.5
                    null
                    add "MotelAshHug" zoom 0.09
                    text "Ash will leave if you rejected her on day 14" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                vbox:
                    xsize 400
                    xcenter 0.5
                    spacing 5
                    grid 2 2:
                        spacing 5
                        add "MotelAshSex5" zoom 0.09
                        add "MotelChloeSex2" zoom 0.09
                        add "MotelJennaSex2" zoom 0.09
                        add "MotelJulesSex2" zoom 0.09
                    text "Can ask Ash to shapeshift into Chloe, Jenna or Jules before sex" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "isabella"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 275
            add "#FF3EA5"
            vbox:
                spacing 10
                xcenter 0.5
                text "Available if went to the swimming lesson and Isabella didn't drown" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Can tell Isabella about the Element, which might affect her route" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                add "HomeIsabellaEmma" zoom 0.09 xcenter 0.5
                text "Small scene with Emma if on her route" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "penny"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 275
            add "#602f6b"
            vbox:
                spacing 10
                xcenter 0.5
                text "Available if Penny has 7+ points and didn't die" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Suggesting to patch things up with Zoey start her subroute" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                add "HomePennyEmma3" zoom 0.09 xcenter 0.5
                text "Small scene with Emma if on her route" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "besties"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 175
            add "#d8b2d1"
            vbox:
                spacing 10
                xcenter 0.5
                text "Starts subroutes for Rachel, Katy and Zoey" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Available if at least on two of these routes" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "If not on Katy or Rachel route, only two girls will be there" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
