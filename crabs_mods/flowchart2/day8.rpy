screen crabs_day8():
    modal True
    tag flowchart
    add "#aaaa" xsize 1920 ysize 1080
    key ["K_ESCAPE"] action [Hide("crabs_day8"), If(show_clock, true=Show("clock"))]
    key ["shift_K_LEFT"] action [Hide("crabs_day8"), Show("crabs_day7")]
    key ["shift_K_RIGHT"] action [Hide("crabs_day8"), Show("crabs_day9")]

    ###Flowchart
    viewport:
        scrollbars "vertical"
        mousewheel True
        arrowkeys True
        pagekeys True
        $ day8_sarah_photo = flag_sarah_photo and not day10_morning == "scene_sarah_photograph"
        $ crabs_current_time = crabs_current_time_hashcode()
        $ graph = [Crabs_Node(id="start", text=" ", connections=[Crabs_Connection("zoey")], img="Day8", condition=gamedate >= 8),
        Crabs_Node(id="zoey", text="Zoey + 1", color="#d8b2d1", connections=[Crabs_Connection("morning")], img="BedroomZoeyTransform3", condition=flag_day8_zoey),
        Crabs_Node(id="morning", text="Morning", connections=[Crabs_Connection("emma", "Trail Emma"), Crabs_Connection("class"), Crabs_Connection("sarah", "Slowly go to class")], img="CollegeJoeyGreet2", condition="Joey told you about the Florist" in desc_joey),
        Crabs_Node(id="emma", text="Emma + 1", color="#ff8243", connections=[Crabs_Connection("class")], img="CollegeEmmaFollow7", condition=flag_emma_trailed and not (day8_sarah_photo or flag_madison_sit or flag_trish_coffee)),
        Crabs_Node(id="class", text="English class", connections=[Crabs_Connection("madison", "Hurry to English Class"), Crabs_Connection("afternoon"), Crabs_Connection("trish", "Coffee with Trish")], img="EnglishAllenDaniel3", condition="escort club" in desc_allen),
        Crabs_Node(id="sarah", text="Sarah + 1", color="#a8e4a0", connections=[Crabs_Connection("class")], img="OldPhotograph", condition=day8_sarah_photo),
        Crabs_Node(id="madison", text="Madison + 1", color="#d470a2", connections=[Crabs_Connection("afternoon")], img="EnglishMadisonSmile2", condition=flag_madison_sit),
        Crabs_Node(id="trish", text="Trish + 1", color="#2e1a47", connections=[Crabs_Connection("afternoon")], img="BeanTrishDrink", condition=flag_trish_coffee),
        Crabs_Node(id="afternoon", text="Afternoon", connections=[Crabs_Connection("isabella", "Take CPR class"), Crabs_Connection("penny", "Go with Penny"), Crabs_Connection("evening"), Crabs_Connection("jenna", "Coffee with Jenna"), Crabs_Connection("caroline", "Visit Caroline's office")], img="CollegeOutside-Afternoon", condition=crabs_current_time >= 31),
        Crabs_Node(id="isabella", text="Isabella + 1", color="#FF3EA5", connections=[Crabs_Connection("evening")], img="HallwayIsabellaFlex", condition=flag_learned_CPR),
        Crabs_Node(id="penny", text="Penny + 1", color="#602f6b", connections=[Crabs_Connection("evening")], img="ClosetPennyFlirt1", condition=flag_penny_closet),
        # Caroline and Isabella are not currently compatible with Jenna so their flags would be False anyway but that might change
        Crabs_Node(id="jenna", text="Jenna + 1", color="#253529", connections=[Crabs_Connection("evening")], img="BeanJennaUnsure", condition=flag_jenna_coffee and not flag_penny_closet and not flag_caroline_hungover and not flag_learned_CPR),
        Crabs_Node(id="caroline", text="Caroline + 1", color="#b11226", connections=[Crabs_Connection("evening")], img="DeansCarolineSmile3", condition=flag_caroline_hungover),
        Crabs_Node(id="evening", text="Evening", connections=[Crabs_Connection("katy", "Hope Katy is working"), Crabs_Connection("sluttydemon", "Watch camgirls"), Crabs_Connection("malia", "Fastfood with Malia")], img="NeighborhoodEvening", condition=flag_katy_manager or flag_day8_sluttydemon or flag_malia_fastfood),
        Crabs_Node(id="katy", text="Katy + 1", color="#592720", connections=[], img="CafeKatySmirk", condition=flag_katy_manager),
        Crabs_Node(id="sluttydemon", text="Watched SluttyDemon", color="#fc89ac", connections=[], img="SluttyDemon18", condition=flag_day8_sluttydemon),
        Crabs_Node(id="malia", text="Malia + 1", color="#860111", connections=[], img="FastfoodMaliaSerious", condition=flag_malia_fastfood and not flag_katy_manager and not flag_day8_sluttydemon),
        ]
        $ screen_size = calc_graph_size(graph)

        vbox:
            xsize 1920
            ysize screen_size
            use crabs_days(8)
            use graph(graph)


    ### Tooltips
    nearrect:
        focus "emma"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 100
            add "#ff8243"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if Emma has 3+ points" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Required scene for Emma but can be seen on day 9" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "sarah"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 100
            add "#a8e4a0"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if Sarah has 3+ points" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Required scene for Sarah but can be seen on day 10" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "madison"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 75
            add "#d470a2"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if Madison has 3+ points and you listened to her talk with Schafer on day 5" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "trish"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 50
            add "#2e1a47"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if Trish has 3+ points and you seen her on day 6" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "isabella"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 100
            add "#FF3EA5"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if Isabella has 3+ points and you seen her on day 6" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Required scene for Isabella" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "penny"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 75
            add "#602f6b"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if helped Penny at least once on the first week (day 3, 4, 6 or 7)" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "jenna"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 125
            add "#253529"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if Jenna has 3+ points and you seen her on day 7" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Required scene for Jenna which can be seen on day 9" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "caroline"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 50
            add "#b11226"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if Caroline has 3+ points" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "katy"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 50
            add "#592720"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if Katy has 3+ points and you seen her on day 6" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "malia"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 125
            add "#860111"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if Malia has 3+ points and you seen her on day 7" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Required scene for Malia which can be seen on day 10" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
