screen crabs_day12():
    modal True
    tag flowchart
    add "#aaaa" xsize 1920 ysize 1080
    key ["K_ESCAPE"] action [Hide("crabs_day12"), If(show_clock, true=Show("clock"))]
    key ["shift_K_LEFT"] action [Hide("crabs_day12"), Show("crabs_day11")]
    key ["shift_K_RIGHT"] action [Hide("crabs_day12"), Show("crabs_day13")]
    $ crabs_current_time = crabs_current_time_hashcode()

    $ graph = [Crabs_Node(id="start", text=" ", connections=[Crabs_Connection("morning")], img="Day12", condition=gamedate >= 12),
        Crabs_Node(id="morning", text="Morning", connections=[Crabs_Connection("zoey", "Unwind with Zoey"), Crabs_Connection("chloesd", "Contact Chloe")], img="LivingZoeyTalk4", condition=gamedate >= 12),
        Crabs_Node(id="zoey", text="Zoey + 1", color="#d8b2d1", connections=[Crabs_Connection("class")], img="LivingZoeyPhone3", condition=flag_zoey_unwind),
        Crabs_Node(id="chloesd", text="Chloe + 1", color="#fc89ac", connections=[Crabs_Connection("class")], img="TrailerChloeTouch", condition=day12_morning == "scene_chloe_confront"),
        Crabs_Node(id="class", text="English class", connections=[Crabs_Connection("caroline", "Talk to Caroline"), Crabs_Connection("isabella", "Workout with Isabella"), Crabs_Connection("sarah", "Give Sarah her photo"), Crabs_Connection("rachel", "Text Rachel after class")], img="EnglishPennySit", condition=gamedate >= 12),
        Crabs_Node(id="caroline", text="Caroline + 1", color="#b11226", connections=[Crabs_Connection("rodriguez")], img="DeansCarolineKiss", condition=day12_morning == "scene_caroline_incident"),
        Crabs_Node(id="isabella", text="Isabella + 1", color="#FF3EA5", connections=[Crabs_Connection("rodriguez")], img="GymIsabellaKiss", condition=day12_morning == "scene_isabella_workout"),
        Crabs_Node(id="sarah", text="Sarah + 1", color="#a8e4a0", connections=[Crabs_Connection("rodriguez")], img="RooftopSarahKiss", condition=day12_morning == "scene_sarah_secret"),
        Crabs_Node(id="rachel", text="Rachel + 1", color="#0038a8", connections=[Crabs_Connection("rodriguez")], img="LibraryRachelFlirty", condition=day12_morning == "scene_rachel_search"),
        Crabs_Node(id="rodriguez", text="Holmes route", color="#135DD8", connections=[Crabs_Connection("blakeman")], img="CollegeRodriguezTalk", condition=flag_day12_rodriguez),
        Crabs_Node(id="blakeman", text="Talk with Blakeman", connections=[Crabs_Connection("emma", "Confront Emma"), Crabs_Connection("ash", "Lessons for Ash"), Crabs_Connection("madison", "Friday Downtown"), Crabs_Connection("jules", "Visit Jules")], img="CollegeBlakemanSmile", condition=flag_day12_blakeman),
        Crabs_Node(id="emma", text="Emma + 1", color="#ff8243", connections=[Crabs_Connection("evening")], img="HomeEmmaTalk", condition="emma" in day12_afternoon),
        Crabs_Node(id="ash", text="Ash + 1", color="#4a646c", connections=[Crabs_Connection("evening")], img="ClubAshSmile8", condition="ash" in day12_afternoon),
        Crabs_Node(id="madison", text="Madison + 1", color="#d470a2", connections=[Crabs_Connection("evening")], img="EscortMadisonSmile", condition=day12_afternoon == "scene_schafer_follow"),
        Crabs_Node(id="jules", text="Jules + 1", color="#ffd300", connections=[Crabs_Connection("evening")], img="BedroomJulesStand", condition="jules" in day12_afternoon),
        Crabs_Node(id="evening", text="Evening", connections=[Crabs_Connection("katy", "Visit Katy"), Crabs_Connection("trish", "Friday Night Patrol"), Crabs_Connection("holmes", "Invite Holmes"), Crabs_Connection("chloe", "Visit Chloe's house"), Crabs_Connection("malia", "Visit Malia's house")], img=["HomeInside-Morning", "CollegeOutside-Afternoon", "EscortOutside1", "GrandHallAlone"], condition=crabs_current_time >= 47),
        Crabs_Node(id="katy", text="Katy + 1", color="#592720", connections=[], img="BedroomKatySmile", condition=day12_evening == "scene_katy_tentacles"),
        Crabs_Node(id="trish", text="Trish + 1", color="#2e1a47", connections=[], img="AlleywayTrishFight34", condition=day12_evening == "scene_trish_patrol"),
        Crabs_Node(id="malia", text="Malia + 1", color="#860111", connections=[], img="BedroomMaliaKiss", condition=day12_evening == "scene_malia_house"),
        Crabs_Node(id="holmes", text="Holmes + 1", color="#135DD8", connections=[], img="HomeHolmesSmile", condition=day12_evening == "scene_holmes_secret"),
        Crabs_Node(id="chloe", text="Chloe + 1", color="#fc89ac", connections=[], img="TrailerChloeTouch3", condition=day12_evening == "scene_chloe_practice"),
        ]
    $ screen_size = calc_graph_size(graph)
    ###Flowchart
    viewport:
        scrollbars "vertical"
        mousewheel True
        arrowkeys True
        pagekeys True
        vbox:
            xsize 1920
            ysize screen_size
            use crabs_days(12)
            use graph(graph)

    ### Tooltips
    nearrect:
        focus "zoey"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 50
            add "#d8b2d1"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if Zoey has 5+ points" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "chloesd"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 125
            add "#fc89ac"
            vbox:
                xcenter 0.5
                spacing 10
                text "Required scene for Chloe if you missed it on day 11" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Adds points for watching SluttyDemon streams on day 2 or day 8" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "chloe"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 125
            add "#fc89ac"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if confronted Chloe about being SluttyDemon on day 11 or day 12" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Optional scene with Chloe which can be seen on day 13" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "caroline"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 225
            add "#b11226"
            vbox:
                xcenter 0.5
                spacing 10
                text "Required scene for Caroline" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Available if seen Caroline on day 9" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                add "DeansCarolinePresex2" zoom 0.09 xcenter 0.5
                text "Sex scene available if slept with Caroline on day 10" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "isabella"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 125
            add "#FF3EA5"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if seen Isabella on day 10 or day 11" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Required scene for Isabella which can be seen on day 13" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "sarah"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 125
            add "#a8e4a0"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if seen Sarah on day 8 or day 10" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Required scene for Sarah if you missed it on day 11" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "rachel"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 50
            add "#0038a8"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if seen Rachel on day 10 or day 11" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "emma"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 275
            add "#ff8243"
            vbox:
                xcenter 0.5
                spacing 10
                text "Required scene for Emma which can be seen on day 13" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Available if seen two men asking you about Emma on day 11" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                add "CollegeMenPhoto" zoom 0.09 xcenter 0.5
                text "Day 9 scene if followed by two men asking about if that didn't happen yet" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "ash"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 150
            add "#4a646c"
            vbox:
                xcenter 0.5
                spacing 10
                null
                add "HallwayHolmesAngry" zoom 0.09 xcenter 0.5
                text "Day 11 scene if you missed it" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "madison"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 100
            add "#d470a2"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if seen Madison at BSC on day 9 or day 10" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Required scene for Madison" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "jules"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 225
            add "#ffd300"
            vbox:
                xcenter 0.5
                spacing 10
                text "Required scene for Jules which can be seen on day 13" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Available if visited Jules on day 11" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                add "BedroomGerardLook" zoom 0.09 xcenter 0.5
                text "Day 11 scene if you missed it" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "katy"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 225
            add "#592720"
            vbox:
                xcenter 0.5
                spacing 10
                text "Required scene for Katy" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Available if Katy has 5+ points" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                add "BedroomKatyTentacles8" zoom 0.09 xcenter 0.5
                text "Avoidable scene with tentacles" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "trish"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 100
            add "#2e1a47"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if seen Trish on day 9 or day 10" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Required scene for Trish" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "malia"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 50
            add "#860111"
            vbox:
                xcenter 0.5
                spacing 10
                text "Required scene for Malia if you missed it on day 12" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "holmes"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 125
            add "#135DD8"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if asked Holmes about Florist on day 9 or day 10" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Required scene for Holmes which can be seen on day 13" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]