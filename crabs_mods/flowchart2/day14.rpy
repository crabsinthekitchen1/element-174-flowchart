screen crabs_day14():
    modal True
    tag flowchart
    add "#aaaa" xsize 1920 ysize 1080
    key ["K_ESCAPE"] action [Hide("crabs_day14"), If(show_clock, true=Show("clock"))]
    key ["shift_K_LEFT"] action [Hide("crabs_day14"), Show("crabs_day13")]
    key ["shift_K_RIGHT"] action [Hide("crabs_day14"), Show("crabs_day15")]
    $ crabs_current_time = crabs_current_time_hashcode()

    $ graph = [Crabs_Node(id="start", text=" ", connections=[Crabs_Connection("morning")], img="Day14", condition=gamedate >= 14),
        Crabs_Node(id="morning", text="Morning", connections=[Crabs_Connection("emma"), Crabs_Connection("bloodstains")], img="BedroomDresser", condition=gamedate >= 14),
        Crabs_Node(id="emma", text="Opened the door for Emma", color="#ff8243", connections=[Crabs_Connection("caroline", "Get explanation from Caroline"), Crabs_Connection("ash", "Meet with Ash"), Crabs_Connection("chloe", "Meet with Chloe"), Crabs_Connection("malia", "Meet with Malia")], img="BedroomEmmaAwake2", condition=gamedate >= 14 and flag_emma_backdoor),
        Crabs_Node(id="bloodstains", text="Confronted Emma but didn't open the door", connections=[], img="HomeBackLook2", condition=gamedate >= 14 and flag_emma_confront and not flag_emma_backdoor),        
        Crabs_Node(id="caroline", text="Caroline + 1", color="#b11226", connections=[Crabs_Connection("afternoon")], img="ApartmentCarolineKiss2", condition=day14_morning == "scene_caroline_secret"),
        Crabs_Node(id="ash", text="Ash + 1", color="#4a646c", connections=[Crabs_Connection("afternoon")], img="ArcadeAshKiss2", condition=day14_morning == "scene_ash_arcade"),
        Crabs_Node(id="chloe", text="Chloe + 1", color="#fc89ac", connections=[Crabs_Connection("afternoon")], img="TrailerChloeLetter", condition=day14_morning == "scene_chloe_letter"),
        Crabs_Node(id="malia", text="Malia + 1", color="#860111", connections=[Crabs_Connection("afternoon")], img="OfficesMaliaRun3", condition=day14_morning == "scene_malia_chase"),
        Crabs_Node(id="afternoon", text="Afternoon", connections=[Crabs_Connection("madison", "See Madison at the BSC"), Crabs_Connection("rachel", "Meet with Rachel"), Crabs_Connection("holmes", "Meet up with Holmes"), Crabs_Connection("isabella", "Swimming lesson with Isabella")], img=["ApartmentCarolineRoom", "ArcadeEnter", "TrailerDoor2", "BedroomMaliaBoard", "BedroomDresser"], condition=crabs_current_time >= 54),
        Crabs_Node(id="rachel", text="Rachel + 1", color="#0038a8", connections=[Crabs_Connection("evening")], img="HomeRachelRitual", condition=day14_afternoon == "scene_rachel_ritual"),
        Crabs_Node(id="holmes", text="Holmes + 1", color="#135DD8", connections=[Crabs_Connection("evening")], img="GastropubHolmesSmile2", condition=day14_afternoon == "scene_holmes_ultimatum"),
        Crabs_Node(id="madison", text="Madison + 1", color="#d470a2", connections=[Crabs_Connection("evening")], img="DressingSchaferExplain2", condition=day14_afternoon == "scene_madison_confront"),
        Crabs_Node(id="isabella", text="Isabella + 1", color="#FF3EA5", connections=[Crabs_Connection("evening")], img="PoolIsabellaCPR5", condition=day14_afternoon == "scene_isabella_lessons"),
        Crabs_Node(id="evening", text="Evening", connections=[Crabs_Connection("katy", "Help Katy with Ritual"), Crabs_Connection("sarah", "Meet up with Sarah"), Crabs_Connection("jules", "Go to Jules's Mansion")], img=["HomeInside-Night", "HomeEmmaPajamas"], condition=crabs_current_time >= 55),
        Crabs_Node(id="katy", text="Katy + 1", color="#592720", connections=[Crabs_Connection("finalfight")], img="BedroomKatyRitual3", condition=day14_evening == "scene_katy_ritual"),
        Crabs_Node(id="jules", text="Jules + 1", color="#ffd300", connections=[Crabs_Connection("finalfight")], img="FacilityJulesTube2", condition=day14_evening == "scene_jules_realize"),
        Crabs_Node(id="sarah", text="Sarah + 1", color="#a8e4a0", connections=[Crabs_Connection("finalfight")], img="RooftopSarahLedge", condition=day14_evening == "scene_sarah_rooftop"),
        Crabs_Node(id="finalfight", text="Penny + 1", color="#602f6b", connections=[Crabs_Connection("escape", "Don't kill her"), Crabs_Connection("death", "Kill her")], img="CollegeZoeyFight45", condition="Penny was the Stranger" in desc_penny),
        Crabs_Node(id="escape", text="Penny runs away", color="#602f6b", connections=[Crabs_Connection("penny", "Go after her"), Crabs_Connection("end", "Don't follow her")], img="CollegeZoeyFight48", condition="Zoey showed mercy" in desc_penny),
        Crabs_Node(id="death", text="Penny dies", connections=[Crabs_Connection("end", " ")], img="CollegePennyDeath7", condition="killed by Zoey" in desc_penny),
        Crabs_Node(id="penny", text="Penny + 1", color="#602f6b", img="BedroomPennyLay", condition="You followed Penny afterward" in desc_penny),
        Crabs_Node(id="end", text=" ", img="LivingZoeyLean", condition=flag_penny_secret and not "You followed Penny" in desc_penny)
        ]
    $ screen_size = calc_graph_size(graph)
    ###Flowchart
    viewport:
        scrollbars "vertical"
        mousewheel True
        arrowkeys True
        pagekeys True
        vbox:
            xsize 1920
            ysize screen_size
            use crabs_days(14)
            use graph(graph)

    ### Tooltips
    nearrect:
        focus "caroline"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 175
            add "#b11226"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if seen Caroline on day 12" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Required scene for Caroline" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "If you didn't kiss Caroline on day 7 and didn't sleep with her on day 10, you can have anal sex with her" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Rejecting Caroline closes her route" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "ash"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 275
            add "#4a646c"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if Holmes asked you about suspicious people after you went to see Ash on day 11 or day 12" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Required scene for Ash" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                add "ArcadeHolmesSuspicious2" zoom 0.09 xcenter 0.5
                text "Telling Holmes about Ash closes her route" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "chloe"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 125
            add "#fc89ac"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if confronted Chloe about being SluttyDemon on day 11 or day 12" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Required scene for Chloe" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Rejecting Chloe closes her route" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "malia"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 150
            add "#860111"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if visited Malia's house on day 11 or day 12" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Required scene for Malia" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "If you turned down Malia 3 times before you can reject her completely" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "madison"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 125
            add "#d470a2"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if followed Schafer on day 12" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Required scene for Madison" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Rejecting Madison closes her route" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "rachel"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 150
            add "#0038a8"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if visited Rachel at her Schafer on day 12" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Required scene for Rachel" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "If you weren't intimate with Rachel before, you can reject her completely" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "holmes"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 250
            add "#135DD8"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if Holmes visited your house at day 12 or day 13" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Required scene for Holmes" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                add "HomeEmmaDead" zoom 0.09 xcenter 0.5
                text "If you saved Emma on day 13 and don't reject Holmes, Emma will die" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "isabella"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 250
            add "#FF3eA5"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if went to the gym with Isabella on day 12 or day 13" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Required scene for Isabella" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                add "PoolIsabellaDrown2" zoom 0.09 xcenter 0.5
                text "If you didn't learn CPR on day 9, Isabella will die" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "katy"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 125
            add "#592720"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if went to Katy's house on day 12" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Required scene for Katy" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Rejecting Katy closes her route" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "jules"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 150
            add "#FFD300"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if Jules woke up from her 'incident' on day 12 or day 13" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Required scene for Jules" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "If you weren't intimate with Jules before, you can reject her completely" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "sarah"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 250
            add "#a8e4a0"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if gave Sarah her photo on day 11 or day 12" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Required scene for Sarah" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                add "RooftopSarahLeap3" zoom 0.09 xcenter 0.5
                text "If you fail to convince Sarah not to jump, she will die" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "escape"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 75
            add "#602f6b"
            vbox:
                xcenter 0.5
                spacing 10
                text "If Penny has 6+ points, she will escape even if you tell Zoey to kill the Stranger" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "penny"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 75
            add "#602f6b"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if Penny has 6+ points" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]


    nearrect:
        focus "end"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 50
            add "#232323"
            vbox:
                xcenter 0.5
                spacing 10
                text "The final thoughts will focus on the character that has most points" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
