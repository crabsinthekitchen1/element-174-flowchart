screen crabs_day7():
    modal True
    tag flowchart
    add "#aaaa" xsize 1920 ysize 1080
    key ["K_ESCAPE"] action [Hide("crabs_day7"), If(show_clock, true=Show("clock"))]
    key ["shift_K_LEFT"] action [Hide("crabs_day7"), Show("crabs_day6")]
    key ["shift_K_RIGHT"] action [Hide("crabs_day7"), Show("crabs_day8")]
    ###Flowchart
    viewport:
        scrollbars "vertical"
        mousewheel True
        arrowkeys True
        pagekeys True

        $ graph = [Crabs_Node(id="start", text=" ", connections=[Crabs_Connection("morning")], img="Day7", condition=gamedate >= 7),
        Crabs_Node(id="morning", text="Morning", connections=[Crabs_Connection("jules", "Visit Jules"), Crabs_Connection("caroline1", "Get breakfast"), Crabs_Connection("malia", "Meet up with Malia")], img="BedroomDresser", condition=flag_day7_jules or flag_day7_malia or flag_day7_caroline),
        Crabs_Node(id="jules", text="Jules + 1", color="#ffd300", connections=[Crabs_Connection("jenna", "Cross street to Jenna"), Crabs_Connection("caroline2", "Walk with Caroline")], img="DiningJulesKiss", condition=flag_day7_jules),
        Crabs_Node(id="caroline1", text="Caroline + 1", color="#b11226", connections=[Crabs_Connection("caroline2", "Walk with Caroline"), Crabs_Connection("rachel", "Go to college")], img="GastropubCarolineLusty", condition=flag_day7_caroline),
        Crabs_Node(id="malia", text="Malia + 1", color="#860111", connections=[Crabs_Connection("rachel", "Rachel's dorm"), Crabs_Connection("chloe1", "Go to library")], img="OverpassMaliaNaked2", condition=flag_day7_malia),
        Crabs_Node(id="jenna", text="Jenna + 1", color="#253529", connections=[Crabs_Connection("beforedate")], img="DowntownTwinsSmile", condition=flag_day7_jenna),
        Crabs_Node(id="caroline2", text="Caroline + 1", color="#b11226", connections=[Crabs_Connection("beforedate")], img="DowntownCarolineStare", condition=flag_day7_caroline2),
        Crabs_Node(id="rachel", text="Rachel + 1", color="#0038a8", connections=[Crabs_Connection("beforedate")], img="DormRachelUndress1", condition=flag_day7_rachel),
        Crabs_Node(id="chloe1", text="Chloe + 1", color="#fc89ac", connections=[Crabs_Connection("beforedate")], img="LibraryChloeApproach2", condition=flag_day7_chloe),
        Crabs_Node(id="beforedate", text="Before the date", connections=[Crabs_Connection("emma", "Meet Emma at home"), Crabs_Connection("penny", "Shopping with Penny"), Crabs_Connection("chloe2", "Listen to Chloe")], img=["DowntownMorning", "CollegeOutside-Afternoon", "LibraryInterior3", "LibraryChloeSit2"], condition=flag_day7_jenna or flag_day7_chloe or flag_day7_caroline2 or flag_day7_rachel),
        Crabs_Node(id="emma", text="Emma + 1", color="#ff8243", connections=[Crabs_Connection("zoey")], img="HomeEmmaExplain", condition=flag_day7_emma),
        Crabs_Node(id="penny", text="Penny + 1", color="#602f6b", connections=[Crabs_Connection("zoey")], img="ClothingPennyOutfit3", condition=flag_day7_penny),
        Crabs_Node(id="chloe2", text="Chloe + 1", color="#fc89ac", connections=[Crabs_Connection("zoey")], img="LibraryChloeSmile3", condition=flag_day7_chloe2),
        Crabs_Node(id="zoey", text="Zoey + 1", color="#d8b2d1", connections=[Crabs_Connection("end")], img="LorenzosZoeySmirk", condition=flag_day7_zoey),
        Crabs_Node(id="end", text="Episode 1 end", img="ElementRoom3", condition="large room with a glowing crystal" in desc_zoey),
        ]
        $ screen_size = calc_graph_size(graph)
        vbox:
            xsize 1920
            ysize screen_size
            use crabs_days(7)
            use graph(graph)


    ### Tooltips
    nearrect:
        focus "malia"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 200
            add "#860111"
            vbox:
                xcenter 0.5
                spacing 10
                null
                add "OverpassMaliaBlowjob4" zoom 0.09 xcenter 0.5
                text "Blowjob available at 3+ points" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Required scene for Malia" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "jules"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 200
            add "#ffd300"
            vbox:
                xcenter 0.5
                spacing 10
                null
                add "DiningJulesTopless" zoom 0.09 xcenter 0.5
                text "Available if lied to Holmes on day 5" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Jules will show you her tits at 3+ points" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "caroline1"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 50
            add "#b11226"
            vbox:
                xcenter 0.5
                spacing 10
                text "Shorter scene if haven't met Caroline before" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "caroline2"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 150
            add "#b11226"
            vbox:
                xcenter 0.5
                spacing 10
                null
                add "DowntownCarolineKiss" zoom 0.09 xcenter 0.5
                text "Available if Caroline has 3+ points" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "jenna"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 75
            add "#253529"
            vbox:
                xcenter 0.5
                spacing 10
                text "Required scene for Jenna" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "A dream scene if saw Jenna, Trish or Holmes on day 6" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "rachel"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 750
            ymaximum 200
            add "#0038a8"
            hbox:
                vbox:
                    xsize 375
                    xcenter 0.5
                    spacing 10
                    null
                    add "DormRachelMasturbate" zoom 0.09 xcenter 0.5
                    text "Masturbation scene if Rachel has 3+ points" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                vbox:
                    xsize 375
                    xcenter 0.5
                    spacing 10
                    null
                    add "DormRachelSmile" zoom 0.09 xcenter 0.5
                    text "Talking about a date with Zoey if didn't see Malia on day 7 and only saw Rachel on day 4 Night" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "emma"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 200
            add "#ff8243"
            vbox:
                xcenter 0.5
                spacing 10
                null
                add "HomeEmmaTopless2" zoom 0.09 xcenter 0.5
                text "Titjob scene if Emma has 3+ points" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Available if got Emma's phone number on day 4 or day 5" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "chloe2"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 100
            add "#fc89ac"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if talked to Chloe before day 7 and went to the library" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
                text "Required scene for Chloe" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]

    nearrect:
        focus "penny"
        prefer_top True
        fixed:
            xcenter 0.5
            xmaximum 375
            ymaximum 50
            add "#602f6b"
            vbox:
                xcenter 0.5
                spacing 10
                text "Available if got Penny's phone number on day 6" size 18 xcenter 0.5 yalign 0.5 outlines [ (absolute(2), "#000", absolute(0), absolute(0)) ]
