screen crabs_day16():
    modal True
    tag flowchart
    add "#aaaa" xsize 1920 ysize 1080
    key ["K_ESCAPE"] action [Hide("crabs_day16"), If(show_clock, true=Show("clock"))]
    key ["shift_K_LEFT"] action [Hide("crabs_day16"), Show("crabs_day15")]
    key ["shift_K_RIGHT"] action If(renpy.has_screen("crabs_day17"), true=[Hide("crabs_day16"), Show("crabs_day17")])
    $ crabs_current_time = crabs_current_time_hashcode()

    $ graph = [Crabs_Node(id="start", text=" ", connections=[Crabs_Connection("morning")], img="Day16", condition=gamedate >= 16),
        Crabs_Node(id="morning", text="Morning", connections=[Crabs_Connection("zoey15", "{size=-5}Invite Zoey over before class{/size}"), Crabs_Connection("jules15", "{size=-5}Try to find Jules{/size}"), Crabs_Connection("emma", "{size=-5}Stay home with Emma{/size}"), Crabs_Connection("caroline", "{size=-5}Visit Caroline later{/size}"), Crabs_Connection("malia", "{size=-5}Try to find Malia{/size}"), Crabs_Connection("katy", "{size=-5}Try to find Katy{/size}"), Crabs_Connection("penny", "{size=-5}Go to English class{/size}")], img="HomeInside-Morning", condition=gamedate >= 16),
        Crabs_Node(id="zoey15", text="Zoey + 1", color="#d8b2d1", connections=[Crabs_Connection("preafternoon")], img="BedroomZoeyKiss2", condition=day16_morning == "scene_zoey_late"),
        Crabs_Node(id="jules15", text="Jules + 1", color="#ffd300", connections=[Crabs_Connection("preafternoon")], img="ClassroomJulesCry2", condition=day16_morning == "scene_jules_talk"),
        Crabs_Node(id="emma", text="Emma + 1", color="#ff8243", connections=[Crabs_Connection("preafternoon")], img="BathroomEmmaKiss", condition=day16_morning == "scene_emma_bathe"),
        Crabs_Node(id="caroline", text="Caroline + 1", color="#b11226", connections=[Crabs_Connection("preafternoon")], img="DeansCarolineBlakeman", condition=day16_morning == "scene_caroline_pressure"),
        Crabs_Node(id="malia", text="Malia + 1", color="#860111", connections=[Crabs_Connection("preafternoon")], img="CollegeThemWatching", condition=day16_morning == "scene_malia_chase2"),
        Crabs_Node(id="katy", text=" ", color="#592720", connections=[Crabs_Connection("preafternoon")], img="ClassroomKatyKiss", condition=day16_morning == "scene_katy_doppelganger"),
        Crabs_Node(id="penny", text="Penny + 1", color="#602f6b", connections=[Crabs_Connection("preafternoon")], img="EnglishPennyArrive2", condition=day16_morning == "scene_penny_return"),
        Crabs_Node(id="preafternoon", text=" ", connections=[Crabs_Connection("juleschloe", "Seduce Chloe"), Crabs_Connection("madisonchloe"), Crabs_Connection("afternoon")], img="HallwayEmpty2", condition=crabs_current_time >= 62),
        Crabs_Node(id="juleschloe", text="Jules/Chloe subroute", color=["#ffd300", "#fc89ac"], connections=[Crabs_Connection("afternoon"), Crabs_Connection("madisonchloe")], img="EnglishJulesWatch", condition=route_chloejules and route_jules_cruel),
        Crabs_Node(id="madisonchloe", text="Chloe/Madison subroute", color=["#fc89ac", "#d470a2"], connections=[Crabs_Connection("afternoon")], img="HallwayChloeMadisonBump4", condition=flag_chloemadison_secrets),
        Crabs_Node(id="afternoon", text="Afternoon", connections=[Crabs_Connection("sarah", "{size=-5}Show Sarah the Element{/size}"), Crabs_Connection("chloe15", "{size=-5}Go home with Chloe"), Crabs_Connection("jenna15", "{size=-5}Visit Jenna{/size}"), Crabs_Connection("coinflip", "{size=-5}Meet up with Trish{/size}"), Crabs_Connection("zoey", "{size=-5}Meet up with Zoey{/size}"), Crabs_Connection("holmes", "{size=-5}Find Joey at the club room{/size}")], img="HallwayEmpty2", condition=crabs_current_time >= 62),
        Crabs_Node(id="sarah", text="Sarah + 1", color="#a8e4a0", connections=[Crabs_Connection("evening")], img="ElementSarahZoey4", condition=day16_afternoon == "scene_sarah_element2"),
        Crabs_Node(id="zoey", text="Zoey + 1", color="#d8b2d1", connections=[Crabs_Connection("evening")], img="LivingZoeyKiss", condition=day16_afternoon == "scene_zoey_house2"),
        Crabs_Node(id="holmes", text="Holmes + 1", color="#135DD8", connections=[Crabs_Connection("evening")], img="ClubJoeyExplain2", condition=day16_afternoon == "scene_holmes_joey"),
        Crabs_Node(id="coinflip", text="Picked up Florist's capsule", color="#2e1a47", connections=[Crabs_Connection("julescapsule"), Crabs_Connection("rachelcapsule")], img="ArtRoomBag", condition=flag_trish_capsule and day16_afternoon == "scene_trish_joey"),
        Crabs_Node(id="julescapsule", text=" ", color="#ffd300", connections=[Crabs_Connection("trish")], img="ArtJulesCapsule", condition=flag_jules_capsule),
        Crabs_Node(id="rachelcapsule", text=" ", color="#0038a8", connections=[Crabs_Connection("trish")], img="ArtRachelKiss2", condition=flag_rachel_capsule),
        Crabs_Node(id="trish", text="Trish + 1", color="#2e1a47", connections=[Crabs_Connection("evening")], img="ClubTrishJoey", condition=day16_afternoon == "scene_trish_joey"),
        Crabs_Node(id="chloe15", text="Chloe + 1", color="#fc89ac", connections=[Crabs_Connection("evening")], img="TrailerChloeKiss", condition=day16_afternoon == "scene_chloe_blackmailed"),
        Crabs_Node(id="jenna15", text="Jenna + 1", color="#253529", connections=[Crabs_Connection("evening")], img="LivingJennaKiss3", condition=day16_afternoon == "scene_jenna_bi"),
        Crabs_Node(id="evening", text="Evening", connections=[Crabs_Connection("isabella", "Shopping with Isabella"), Crabs_Connection("isabellajules", "Have Isabella and Jules meet"), Crabs_Connection("madison", "Invite Madison over"), Crabs_Connection("rachel", "Help Rachel with curse"), Crabs_Connection("jenna"), Crabs_Connection("ashjenna", "Meet with Ash at Jenna's"), Crabs_Connection("ash", "Check on Ash")], img="HomeInside-Afternoon", condition=crabs_current_time >= 63),
        Crabs_Node(id="ash", text="Ash + 1", color="#4a646c", connections=[Crabs_Connection("night")], img="MotelAshSmile", condition=day15_evening == "scene_ash_motel"),
        Crabs_Node(id="ashjenna", text="Jenna/Ash subroute", color=["#253529","#4a646c"], connections=[Crabs_Connection("jenna", "Take Jenna to the Element")], img="BedroomAshJenna4", condition=day16_earlyevening == "scene_ashjenna_secrets"),
        Crabs_Node(id="jenna", text="Jenna + 1", color="#253529", connections=[Crabs_Connection("night")], img="ElementJennaZoey5", condition=day16_evening == "scene_jenna_element"),
        Crabs_Node(id="isabellajules", text="Isabella/Jules subroute", color=["#FF3eA5", "#ffd300"], connections=[Crabs_Connection("isabella", "Shopping with Isabella")], img="PoolIsabellaJulesTalk", condition=day16_earlyevening == "scene_isabella_shopping"),
        Crabs_Node(id="isabella", text="Isabella + 1", color="#FF3eA5", connections=[Crabs_Connection("night")], img="ClothingIsabellaSwimsuit", condition=day16_evening == "scene_isabella_shopping"),
        Crabs_Node(id="madison", text="Madison + 1", color="#d470a2", connections=[Crabs_Connection("night")], img="HomeMadisonKiss", condition=day16_evening == "scene_madison_past"),
        Crabs_Node(id="rachel", text="Rachel + 1", color="#0038a8", connections=[Crabs_Connection("night")], img="DormRachelKiss", condition=day16_evening == "scene_rachel_cure"),
        Crabs_Node(id="night", text="Night", connections=[Crabs_Connection("jennazoey", "Encourage Zoey and Jenna"), Crabs_Connection("pennyzoey", "Meet with Penny and Zoey"), Crabs_Connection("ashsarah", "Meet with Ash and Sarah")], img="HomeInside-Night2", condition=crabs_current_time >= 64),
        Crabs_Node(id="jennazoey", text="Zoey/Jenna subroute", color=["#d8b2d1", "#253529"], img="ElementJennaZoeyKiss7", condition=day16_night == "scene_jennazoey_element"),
        Crabs_Node(id="pennyzoey", text="Penny/Zoey subroute", color=["#602f6b", "#d8b2d1"], img="HomePennyZoeyTalk", condition=day16_night == "scene_pennyzoey_discuss"),
        Crabs_Node(id="ashsarah", text="Ash/Sarah subroute", color=["#4a646c", "#a8e4a0"], img="ApartmentAshSarahKiss", condition=day16_night == "scene_ashsarah_secrets"),
    ]

    $ screen_size = calc_graph_size(graph) + 50
    ###Flowchart
    viewport:
        scrollbars "vertical"
        mousewheel True
        arrowkeys True
        pagekeys True
        vbox:
            xsize 1920
            ysize screen_size
            spacing 10
            use crabs_days(16)
            text "Starting from week 3, you only get points if you choose to have sex with girls" color "#000" size 22 xcenter 0.5 yalign 0.5
            use graph(graph)

