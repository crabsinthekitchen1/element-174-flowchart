init offset = 3

init 3 python:
    def crabs_calc_max_points(girl):
        crabs_current_time = crabs_current_time_hashcode()
        mp_dict = {
                    "ash":flag_day1_club + flag_day1_punch + bool(points_isabella) + \
                        flag_day2_ash + flag_day2_zoey + flag_day2_madison + flag_day2_trish + flag_day2_isabella + \
                        flag_day2_ash2 + flag_day2_zoey + flag_day2_madison + flag_day2_trish + flag_day2_isabella + \
                        flag_day5_ash + flag_day5_sarah + ("After class on Friday, you met Ash" in desc_ash) + \
                        flag_day6_ash + flag_day6_sarah + flag_day6_madison + (flag_ash_clothing or (crabs_current_time >= 35)) + \
                        (flag_ash_secret or (day10_afternoon and not (day10_afternoon == "scene_ash_secret")) or (crabs_current_time >= 39)) + \
                        (flag_ash_liefor or (day11_afternoon and not (day11_afternoon == "scene_ash_liefor")) or (crabs_current_time >= 43)) + \
                        (flag_ash_lesson2 or (day12_afternoon and not (day12_afternoon == "scene_ash_lesson2")) or (crabs_current_time >= 47)) +\
                        (flag_ash_arcade or (day14_morning and not (day14_morning == "scene_ash_arcade")) or (crabs_current_time >= 54)) +\
                        (flag_ash_motel or (day15_evening and not (day15_evening == "scene_ash_motel")) or (crabs_current_time >= 60)),

                    "caroline":flag_day1_apologize + flag_day1_backtalk + (not flag_day1_punch) + \
                        flag_day4_caroline + flag_day4_malia + flag_day4_trish + flag_day4_rachel + \
                        flag_day5_caroline + flag_day5_malia + flag_day5_chloe + flag_day5_penny + \
                        flag_day7_caroline + flag_day7_malia + flag_day7_jules + \
                        flag_day7_caroline2 + flag_day7_rachel + flag_day7_jenna + \
                        (flag_caroline_hungover or flag_learned_CPR or (flag_penny_closet or flag_jenna_coffee) or (crabs_current_time >= 31)) + \
                        (flag_caroline_confront or flag_ash_clothing or (crabs_current_time >= 35)) + \
                        (flag_caroline_apartment or (day10_evening and not (day10_evening == "scene_caroline_apartment")) or (crabs_current_time >= 40)) + \
                        (flag_caroline_incident or flag_zoey_unwind or (day12_morning and not (day12_morning == "scene_caroline_incident")) or (crabs_current_time >= 46)) + \
                        (flag_caroline_secret or (day14_morning and not (day14_morning == "scene_caroline_secret")) or (crabs_current_time >= 54)) +\
                        (flag_caroline_talk or (day15_morning and not (day15_morning == "scene_caroline_talk")) or (crabs_current_time >= 58)),

                    "chloe":flag_day2_chloe + flag_day2_zoey + flag_day2_ash2 + flag_day2_madison + flag_day2_trish + \
                        flag_day4_chloe + flag_day4_emma + flag_day4_rachel + flag_day4_penny + \
                        flag_day5_chloe + flag_day5_penny + flag_day5_malia + flag_day5_caroline + \
                        (flag_day7_chloe or flag_day7_rachel or flag_day7_caroline or flag_day7_jules) + \
                        (flag_day7_chloe2 or flag_day7_jules or flag_day7_malia or flag_day7_penny or flag_day7_emma or flag_day7_zoey) + \
                        ("she is SluttyDemon" in desc_chloe or (crabs_current_time >= 36)) + \
                        (flag_chloe_confront or flag_zoey_unwind or (day11_morning and not (day11_morning == "scene_chloe_confront")) or (crabs_current_time >=42)) + \
                        (crabs_current_time >= 42) + \
                        (flag_chloe_practice or (day12_evening and not (day12_evening == "scene_chloe_practice")) or crabs_current_time >= 48) + \
                        (flag_chloe_letter or (day14_morning and not (day14_morning == "scene_chloe_letter")) or (crabs_current_time >= 54)) +\
                        (flag_chloe_blackmailed or (day15_afternoon and not (day15_afternoon == "scene_chloe_blackmailed")) or (crabs_current_time >= 59)),

                    "emma":day2_met_emma + day2_met_malia + flag_day2_rachel + flag_day2_jules + \
                        flag_day3_emma + flag_day3_katy + flag_day3_jules + flag_day3_madison + \
                        flag_day4_emma + flag_day4_chloe + flag_day4_rachel + flag_day4_penny + \
                        flag_day5_emma + flag_day5_madison + \
                        (flag_day7_emma or flag_day7_penny or flag_day7_chloe2 or flag_day7_zoey) + \
                        ("Emma surreptitiously taking pictures" in desc_emma or flag_madison_sit or flag_trish_coffee or flag_sarah_photo or (crabs_current_time >= 30)) + \
                        (flag_emma_classroom or flag_trish_secret or flag_ash_clothing or flag_caroline_confront or (crabs_current_time >= 35)) + \
                        (flag_emma_closet or (day11_morning and not (day11_morning == "scene_emma_closet")) or (crabs_current_time >= 42)) + \
                        (flag_emma_confront or (day12_afternoon and not (day12_afternoon == "scene_emma_confront")) or (crabs_current_time >= 50)) + \
                        (flag_emma_backdoor or (day13_evening and not (day13_evening == "scene_emma_backdoor")) or (crabs_current_time >= 53)),

                    "holmes":flag_day1_backtalk + flag_day1_apologize + (not flag_day1_punch) + \
                        (crabs_current_time >= 10) + \
                        (flag_day3_blamed_self or flag_day3_blamed_caleb or bool(points_jenna) or flag_day3_isabella or flag_day3_rachel) + \
                        (flag_day5_holmes or flag_day5_jules) + \
                        (flag_day6_holmes or flag_day6_trish or flag_day6_isabella or flag_day6_katy or flag_day6_jenna) + \
                        ("you were introduced to Sergeant Rodriguez by Holmes" in desc_holmes or flag_rachel_coffee or flag_katy_cultist or flag_jenna_zoey or crabs_current_time >= 34) + \
                        (flag_holmes_restaurant or (day10_evening and not (day10_evening == "scene_holmes_restaurant")) or crabs_current_time >= 40) + \
                        (flag_holmes_secret or (day12_evening and not (day12_evening == "scene_holmes_secret")) or crabs_current_time >= 48) + \
                        (flag_holmes_frontdoor or (day13_evening and not (day13_evening == "scene_holmes_frontdoor")) or crabs_current_time >= 52) + \
                        (flag_holmes_ultimatum or (day14_afternoon and not (day14_afternoon == "scene_holmes_ultimatum")) or crabs_current_time >= 55) + \
                        (flag_holmes_leads or (day15_afternoon and not (day15_afternoon == "scene_holmes_leads")) or (crabs_current_time >= 59)),

                    "isabella":(flag_day1_punch or flag_day1_club or bool(points_isabella)) + \
                        (flag_day2_isabella or flag_day2_ash or flag_day2_zoey or flag_day2_madison or flag_day2_trish) + \
                        (flag_day2_isabella2 or flag_day2_sarah or flag_day2_ash or flag_day2_zoey or flag_day2_madison or+ flag_day2_trish) + \
                        (flag_day3_isabella or flag_day3_rachel or bool(points_jenna) or flag_day3_blamed_caleb or flag_day3_blamed_self) + \
                        (flag_day6_isabella or flag_day6_katy or flag_day6_jenna or flag_day6_trish or flag_day6_holmes) + \
                        (flag_caroline_hungover or flag_learned_CPR or (flag_penny_closet or flag_jenna_coffee) or (crabs_current_time >= 31)) + \
                        (flag_isabella_lunch or (day10_afternoon and not (day10_afternoon == "scene_isabella_lunch")) or (crabs_current_time >= 39)) + \
                        (flag_isabella_workout or (day12_morning and not (day12_morning == "scene_isabella_workout")) or crabs_current_time >= 46) + \
                        (flag_isabella_soccer or (day13_morning and not (day13_morning == "scene_isabella_soccer")) or crabs_current_time >= 50) + \
                        (flag_isabella_lessons or (day14_afternoon and not (day14_afternoon == "scene_isabella_lessons")) or crabs_current_time >= 54) + \
                        (flag_isabella_invite or (day15_evening and not (day15_evening == "scene_isabella_invite")) or crabs_current_time >= 60),

                    "jenna":(bool(points_jenna) or flag_day3_blamed_caleb or flag_day3_blamed_self or flag_day3_sarah or flag_day3_rachel) + \
                        (flag_day3_told or flag_day3_notold or flag_day3_blamed_caleb or flag_day3_blamed_self or flag_day3_sarah or flag_day3_rachel) + \
                        (flag_day4_jenna_defend or flag_day4_jenna_bully or flag_day4_madison_defend or flag_day4_madison_bully) + \
                        (flag_day6_jenna or flag_day6_isabella or flag_day6_katy or flag_day6_trish or flag_day6_holmes) + \
                        (flag_day7_jenna or flag_day7_caroline2 or flag_day7_caroline or flag_day7_malia) + \
                        ("Jenna told you of how she didn't see herself" in desc_jenna or flag_learned_CPR or flag_penny_closet or flag_caroline_hungover or (crabs_current_time >= 31)) + \
                        ("You introduced Zoey" in desc_jenna or flag_holmes_help or flag_rachel_coffee or flag_katy_cultist or (crabs_current_time >= 34)) + \
                        (flag_jenna_pool or (day10_afternoon and not (day10_afternoon == "scene_jenna_pool")) or (crabs_current_time >= 39)) + \
                        (flag_jenna_ash or flag_ash_liefor or bool(day11_afternoon) or (crabs_current_time >= 43)) + \
                        (flag_jenna_pugman or (day13_afternoon and not (day13_afternoon == "scene_jenna_pugman")) or (crabs_current_time >= 51)) + \
                        (flag_jenna_bi or (day15_afternoon and not (day15_afternoon == "scene_jenna_bi")) or (crabs_current_time >= 59)),

                    "jules":flag_day2_jules + flag_day2_rachel + day2_met_malia + day2_met_emma + \
                        flag_day3_jules + flag_day3_katy + flag_day3_madison + flag_day3_emma + \
                        flag_day4_jenna_bully + flag_day4_madison_bully + flag_day4_jenna_defend + flag_day4_madison_defend + \
                        flag_day5_jules + flag_day5_holmes + \
                        flag_day7_jules + flag_day7_caroline + flag_day7_malia + \
                        ("she went motionless" in desc_jules or flag_chloe_secret or flag_escort_club or (crabs_current_time >= 36)) + \
                        (flag_jules_secret or (day11_afternoon and not (day11_afternoon == "scene_jules_secret")) or (crabs_current_time >= 43)) + \
                        (flag_jules_wakesup or (day12_afternoon and not (day12_afternoon == "scene_jules_wakesup")) or (crabs_current_time >= 47)) + \
                        (flag_jules_date or (day13_morning and not (day13_morning == "scene_jules_date")) or (crabs_current_time >= 50)) + \
                        (flag_jules_realize or (day14_evening and not (day14_evening == "scene_jules_realize")) or (crabs_current_time >= 56)) + \
                        (flag_jules_talk or (day15_morning and not (day15_morning == "scene_jules_talk")) or (crabs_current_time >= 58)),

                    "katy":(flag_day2_zoey or flag_day2_isabella or flag_day2_ash or flag_day2_madison or flag_day2_trish) + \
                        (flag_day2_katy2 or flag_day2_camgirl or flag_day2_isabella or flag_day2_ash or flag_day2_madison or flag_day2_trish) + \
                        (flag_day3_katy or flag_day3_madison or flag_day3_emma or flag_day3_jules) + \
                        (flag_day4_katy or flag_day4_sarah or flag_day4_penny or flag_day4_trish) + \
                        (flag_day6_katy or flag_day6_isabella or flag_day6_jenna or flag_day6_trish or flag_day6_holmes) + \
                        (flag_katy_manager or flag_malia_fastfood or flag_day8_sluttydemon) + \
                        (flag_katy_cultist or flag_holmes_help or flag_rachel_coffee or flag_jenna_zoey or (crabs_current_time >= 34)) + \
                        (flag_katy_house or (day11_evening and not (day11_evening == "scene_katy_house")) or (crabs_current_time >= 44)) + \
                        (flag_katy_tentacles or (day12_evening and not (day12_evening == "scene_katy_tentacles")) or (crabs_current_time >= 48)) + \
                        (flag_katy_ritual or (day14_evening and not (day14_evening == "scene_katy_ritual")) or (crabs_current_time >= 56)) + \
                        ((bool(day15_night) or crabs_current_time >= 61) / 2),

                    "madison":flag_day2_madison + flag_day2_trish + flag_day2_zoey + flag_day2_ash + flag_day2_isabella + \
                        flag_day3_madison + flag_day3_emma + flag_day3_katy + flag_day3_jules + \
                        flag_day4_madison_defend + flag_day4_jenna_defend + flag_day4_madison_bully + flag_day4_jenna_bully + \
                        flag_day5_madison + flag_day5_emma + \
                        flag_day6_madison + flag_day6_ash + flag_day6_sarah + \
                        ("sat next to her" in desc_madison or flag_emma_trailed or flag_sarah_photo or flag_trish_coffee or (crabs_current_time >= 30)) + \
                        (flag_jules_fingerbang or flag_chloe_secret or "Branchfield Social Club" in desc_madison or (crabs_current_time >= 36)) + \
                        (flag_madison_classroom or (day10_morning and not (day10_morning == "scene_madison_classroom")) or (crabs_current_time >= 38)) + \
                        (flag_schafer_follow or (day12_afternoon and not (day12_afternoon == "scene_schafer_follow")) or crabs_current_time >= 47) + \
                        (flag_madison_confront or (day14_afternoon and not (day14_afternoon == "scene_madison_confront")) or crabs_current_time >= 54),

                    "malia":(day2_met_malia or day2_met_emma or flag_day2_rachel or flag_day2_jules) + \
                        (flag_day4_malia or flag_day4_caroline or flag_day4_rachel or flag_day4_trish) + \
                        (flag_day5_malia or flag_day5_caroline or flag_day5_penny or flag_day5_chloe) + \
                        (flag_day6_malia or flag_day6_penny) + \
                        (flag_day7_malia or flag_day7_caroline or flag_day7_jules) + \
                        (flag_day8_sluttydemon or flag_malia_fastfood or flag_katy_manager) + \
                        (flag_malia_lessons or (day10_evening and not (day10_evening == "scene_malia_lessons")) or (crabs_current_time >= 40)) + \
                        (flag_malia_house or (day11_evening and not (day11_evening == "scene_malia_house")) or (crabs_current_time >= 44)) + \
                        (flag_malia_pugman or (day13_afternoon and not (day13_afternoon == "scene_malia_pugman")) or (crabs_current_time >= 51)) + \
                        (flag_malia_chase or (day14_morning and not (day14_morning == "scene_malia_chase")) or (crabs_current_time >=54)) + \
                        (flag_malia_element or (day15_morning and not (day15_morning == "scene_malia_element")) or (crabs_current_time >= 58)),

                    "penny":(flag_day3_penny or flag_day3_zoey) + \
                        (flag_day4_penny or flag_day4_trish or flag_day4_rachel) + \
                        (flag_day5_penny or flag_day5_chloe or flag_day5_caroline or flag_day5_malia) + \
                        (flag_day6_penny or flag_day6_malia) + \
                        (flag_day7_penny or flag_day7_emma or flag_day7_chloe2 or flag_day7_zoey) + \
                        (flag_penny_closet or flag_learned_CPR or flag_caroline_hungover or flag_jenna_coffee or (crabs_current_time >= 31)) + \
                        (flag_penny_karaoke or (day10_evening and not (day10_evening == "scene_penny_karaoke")) or (crabs_current_time >= 40)) + \
                        (flag_penny_bowling or (day13_evening and not (day13_evening == "scene_penny_bowling")) or (crabs_current_time >= 52)) + \
                        flag_penny_secret + flag_penny_secret + \
                        (flag_penny_text or (day15_evening and not (day15_evening == "scene_penny_text")) or (crabs_current_time >= 60)),

                    "rachel":(flag_day2_rachel or flag_day2_jules or day2_met_emma or day2_met_malia) + \
                        (flag_day2_rachel2 or flag_day2_sarah or flag_day2_trish or flag_day2_zoey or flag_day2_ash or flag_day2_isabella) + \
                        (flag_day3_rachel or flag_day3_isabella or bool(points_jenna) or flag_day3_blamed_caleb or flag_day3_blamed_self) + \
                        (flag_day4_rachel or flag_day4_trish or flag_day4_penny) + \
                        (flag_day7_rachel or flag_day7_caroline2 or flag_day7_chloe or flag_day7_jules) + \
                        ("had coffee with Rachel" in desc_rachel or flag_katy_cultist or flag_holmes_help or crabs_current_time >= 34) + \
                        (flag_rachel_explain or (day10_morning and not (day10_morning == "scene_rachel_explain")) or crabs_current_time >= 38) + \
                        (flag_rachel_search or flag_zoey_unwind or (day12_morning and not (day12_morning == "scene_rachel_search")) or crabs_current_time >= 46) + \
                        (flag_rachel_pugman or (day13_afternoon and not (day13_afternoon == "scene_rachel_pugman")) or crabs_current_time >= 51) + \
                        (flag_rachel_ritual or (day14_afternoon and not (day14_afternoon == "scene_rachel_ritual")) or crabs_current_time >= 55) + \
                        ((bool(day15_night) or crabs_current_time >= 61) / 2),

                    "sarah":(flag_day2_sarah or flag_day2_trish or flag_day2_rachel2 or flag_day2_zoey or flag_day2_ash) + \
                        (flag_day3_notold or flag_day3_told or flag_day3_blamed_self or flag_day3_blamed_caleb or flag_day3_rachel or flag_day3_isabella) + \
                        (flag_day4_sarah or flag_day4_katy or flag_day4_penny or flag_day4_trish) + \
                        (flag_day5_sarah or flag_day5_ash or ("After class on Friday, you met Ash" in desc_ash)) + \
                        (flag_day6_sarah or flag_day6_ash or flag_day6_madison) + \
                        ("dropped an old photograph" in desc_sarah or flag_madison_sit or flag_emma_trailed or flag_sarah_photo or flag_trish_coffee or (crabs_current_time >= 30)) + \
                        (flag_sarah_statue or (day10_morning and not (day10_morning == "scene_sarah_statue")) or crabs_current_time >= 38) + \
                        (flag_sarah_secret or (day11_morning and not (day11_morning == "scene_sarah_secret")) or crabs_current_time >= 42) + \
                        (flag_sarah_apartment or (day13_morning and not (day13_morning == "scene_sarah_apartment")) or crabs_current_time >= 50) + \
                        (flag_sarah_rooftop or (day14_evening and not (day14_evening == "scene_sarah_rooftop")) or crabs_current_time >= 56) + \
                        (flag_sarah_element or (day15_morning and not (day15_morning == "scene_sarah_element")) or (crabs_current_time >= 58)),

                    "trish":(flag_day2_trish or flag_day2_zoey or flag_day2_madison or flag_day2_ash or flag_day2_isabella) + \
                        (flag_day2_trish2 or flag_day2_camgirl or flag_day2_zoey or flag_day2_madison or flag_day2_ash or flag_day2_isabella) + \
                        (flag_day3_blamed_caleb or flag_day3_blamed_self or bool(points_jenna) or flag_day3_rachel or flag_day3_isabella) + \
                        (flag_day4_trish or flag_day4_penny or flag_day4_rachel) + \
                        (flag_day6_trish or flag_day6_holmes or flag_day6_katy or flag_day6_isabella or flag_day6_jenna) +\
                        ("rise in crime" in desc_trish or flag_emma_trailed or flag_madison_sit or flag_sarah_photo or crabs_current_time >= 30) + \
                        (flag_trish_secret or flag_emma_classroom or flag_ash_clothing or flag_caroline_confront or crabs_current_time >= 35) + \
                        (flag_trish_explain or (day10_afternoon and not (day10_afternoon == "scene_trish_explain")) or crabs_current_time >= 39) + \
                        (flag_trish_patrol or (day12_evening and not (day12_evening == "scene_trish_patrol")) or crabs_current_time >= 48) + \
                        (flag_trish_florist or (day13_evening and not (day13_evening == "scene_trish_florist")) or crabs_current_time >= 52) + \
                        (flag_trish_house or (day15_afternoon and not (day15_afternoon == "scene_trish_house")) or crabs_current_time >= 61),

                    "zoey":bool(desc_zoey) + \
                        (flag_day2_zoey or flag_day2_ash or flag_day2_isabella or flag_day2_madison or flag_day2_trish) + \
                        (flag_day3_zoey or flag_day3_penny) + \
                        flag_day4_zoey + flag_day7_zoey + flag_day8_zoey + flag_stranger_attack + \
                        (crabs_current_time >= 45) + \
                        (flag_zoey_unwind or bool(day12_morning) or (crabs_current_time >= 46)) + \
                        (flag_zoey_pugman or (bool(day13_afternoon) and not day13_afternoon == "scene_zoey_pugman") or (crabs_current_time >= 51)) + \
                        (flag_zoey_late or (day15_morning and not (day15_morning == "scene_zoey_late")) or crabs_current_time >= 60) + \
                        ((bool(day15_night) or crabs_current_time >= 61) / 2),}

        return mp_dict.get(girl, 0)

    def get_sheetinfo(girl):
        sheetinfo_dict = {
        "ash":("Ash Jones","Age: 18\nBirthday: Sep 12\nHeight: 5\'3\"\nWeight: 105 lbs."),
        "caroline":("Caroline Kelly","Age: 33\nBirthday: Mar 18\nHeight: 5\'9\"\nWeight: 169 lbs."),
        "chloe":("Chloe Whitaker","Age: 18\nBirthday: Dec 20\nHeight: 5\'1\"\nWeight: 108 lbs."),
        "emma":("Emma McIntosh","Age: 18\nBirthday: Oct 1\nHeight: 5\'2\"\nWeight: 112 lbs."),
        "holmes":("Valerie Holmes","Age: 34\nBirthday: Jul 25\nHeight: 6\'0\"\nWeight: 161 lbs."),
        "isabella":("Isabella Olmos","Age: 18\nBirthday: Apr 8\nHeight: 5\'7\"\nWeight: 144 lbs."),
        "jenna":("Jenna Thomas", "Age: 18\nBirthday: Jun 7\nHeight: 5\'5\"\nWeight: 122 lbs."),
        "jules":("Jules Frederickson","Age: 18\nBirthday: Jan 29\nHeight: 5\'4\"\nWeight: 135 lbs."),
        "katy":("Katy Curwen","Age: 19\nBirthday: Aug 19\nHeight: 5\'8\"\nWeight: 140 lbs."),
        "madison":("Madison Douglas","Age: 18\nBirthday: Nov 14\nHeight: 5\'5\"\nWeight: 146 lbs."),
        "malia":("Malia Wilson","Age: 18\nBirthday: Aug 22\nHeight: 5\'10\"\nWeight: 151 lbs."),
        "penny":("Penny Stills", "Age: 18\nBirthday: Sep 17\nHeight: 5\'5\"\nWeight: 136 lbs."),
        "rachel":("Rachel Weaver","Age: 18\nBirthday: May 24\nHeight: 5\'6\"\nWeight: 162 lbs."),
        "sarah":("Sarah Walker","Age: 20\nBirthday: Feb 3\nHeight: 5\'6\"\nWeight: 125 lbs."),
        "trish":("Trish Campbell", "Age: 18\nBirthday: Nov 28\nHeight: 5\'3\"\nWeight: 132 lbs."),
        "zoey":("Zoey Gardner","Age: 18\nBirthday: Dec 12\nHeight: 5\'2\"\nWeight: 102 lbs.")}
        return sheetinfo_dict.get(girl, ("Unknown character","Unknown character"))

screen card(girl):

    tag menu

    ## Avoid predicting this screen, as it can be very large.
    predict False

    use game_menu(_(girl), scroll="viewport"): #, yinitial=0.0, draggable=False):
        $ girl = girl.lower()
        $ sheetname, sheetinfo = get_sheetinfo(girl)

        frame:
            xmaximum 1375
            hbox:
                $ sheet_image = "sheet_{}.png".format(girl)
                add sheet_image zoom 0.4
                $ crabs_current_time = crabs_current_time_hashcode()
                $ crabs_max_points = crabs_calc_max_points(girl)
                $ point_text = "{}/{}".format(globals()["points_{}".format(girl)], crabs_max_points)
                if crabs_route_check(girl, crabs_max_points):
                    add Composite((200,200), (0,0), Image("hearticon.png"), (60, 70), Text(point_text, color="#000000")) xpos -0.4 zoom 0.75
                else:
                    add Composite((200,200), (0,0), Image("crabs_mods/gui/hearticon_disabled.png"), (60, 70), Text(point_text, color="#000000")) xpos -0.55 zoom 0.75
                vbox:
                    xpos -0.05
                    text ("{size=+10}[sheetname]")
                    hbox:
                        text ("{size=+2}[sheetinfo]")
                        add Null(width=10)

                        grid 2 4:
                            transpose True
                            if globals()["kissed_{}".format(girl)]:
                                add "iconkissed.png" zoom 0.5
                            else:
                                add Null(width=325, height=50)
                            if globals()["fingered_{}".format(girl)]:
                                add "iconfingered.png" zoom 0.5
                            else:
                                add Null(width=325, height=50)
                            if globals()["handjob_{}".format(girl)]:
                                add "iconhandjob.png" zoom 0.5
                            else:
                                add Null(width=325, height=50)
                            if globals()["blowjob_{}".format(girl)]:
                                add "iconblowjob.png" zoom 0.5
                            else:
                                add Null(width=325, height=50)
                            if globals()["cunnilingus_{}".format(girl)]:
                                add "iconcunnilingus.png" zoom 0.5
                            else:
                                add Null(width=325, height=50)
                            if globals()["anal_{}".format(girl)]:
                                add "iconanalsex.png" zoom 0.5
                            else:
                                add Null(width=325, height=50)
                            if globals()["sex_{}".format(girl)]:
                                add "iconsex.png" zoom 0.5
                            else:
                                add Null(width=325, height=50)

                            if globals()["creampie_{}".format(girl)]:
                                add "iconcreampie.png" zoom 0.5
                            else:
                                add Null(width=325, height=50)
                    text ""
                    $ desc_girl = globals()["desc_{}".format(girl)]
                    text ("{size=-4}"+desc_girl) xalign 0.75 xmaximum 900

        textbutton "Show compatible routes" action ShowMenu("crabs_compatible_routes_{}".format(girl))

screen characters():

    tag menu

    ## Avoid predicting this screen, as it can be very large.
    predict False

    use game_menu(_("Characters")): #, yinitial=0.0):

        style_prefix "characters"

        # Row 1

        imagebutton auto "gui/unknown_%s.png" xpos 275 ypos 100 focus_mask True

        if met_sarah:

            imagebutton auto "gui/sarah_%s.png" xpos 400 ypos 100 focus_mask True action ShowMenu("card", girl="Sarah")

        else:

            imagebutton auto "gui/unknown_%s.png" xpos 400 ypos 100 focus_mask True

        if met_isabella:

            imagebutton auto "gui/isabella_%s.png" xpos 525 ypos 100 focus_mask True action ShowMenu("card", girl="Isabella")

        else:

            imagebutton auto "gui/unknown_%s.png" xpos 525 ypos 100 focus_mask True

        imagebutton auto "gui/unknown_%s.png" xpos 650 ypos 100 focus_mask True

        if met_malia:

            imagebutton auto "gui/malia_%s.png" xpos 775 ypos 100 focus_mask True action ShowMenu("card", girl="Malia")

        else:

            imagebutton auto "gui/unknown_%s.png" xpos 775 ypos 100 focus_mask True

        # Row 2

        imagebutton auto "gui/unknown_%s.png" xpos 275 ypos 225 focus_mask True

        if met_katy:

            imagebutton auto "gui/katy_%s.png" xpos 400 ypos 225 focus_mask True action ShowMenu("card", girl="Katy")

        else:

            imagebutton auto "gui/unknown_%s.png" xpos 400 ypos 225 focus_mask True

        if met_rachel:

            imagebutton auto "gui/rachel_%s.png" xpos 525 ypos 225 focus_mask True action ShowMenu("card", girl="Rachel")

        else:

            imagebutton auto "gui/unknown_%s.png" xpos 525 ypos 225 focus_mask True

        if met_caroline:

            imagebutton auto "gui/caroline_%s.png" xpos 650 ypos 225 focus_mask True action ShowMenu("card", girl="Caroline")

        else:

            imagebutton auto "gui/unknown_%s.png" xpos 650 ypos 225 focus_mask True

        if met_emma:

            imagebutton auto "gui/emma_%s.png" xpos 775 ypos 225 focus_mask True action ShowMenu("card", girl="Emma")

        else:

            imagebutton auto "gui/unknown_%s.png" xpos 775 ypos 225 focus_mask True

        # Row 3

        imagebutton auto "gui/unknown_%s.png" xpos 275 ypos 350 focus_mask True

        if met_chloe:

            imagebutton auto "gui/chloe_%s.png" xpos 400 ypos 350 focus_mask True action ShowMenu("card", girl="Chloe")

        else:

            imagebutton auto "gui/unknown_%s.png" xpos 400 ypos 350 focus_mask True

        if met_zoey:

            imagebutton auto "gui/zoey_%s.png" xpos 525 ypos 350 focus_mask True action ShowMenu("card", girl="Zoey")

        else:

            imagebutton auto "gui/unknown_%s.png" xpos 525 ypos 350 focus_mask True

        if met_holmes:

            imagebutton auto "gui/holmes_%s.png" xpos 650 ypos 350 focus_mask True action ShowMenu("card", girl="Holmes")

        else:

            imagebutton auto "gui/unknown_%s.png" xpos 650 ypos 350 focus_mask True

        imagebutton auto "gui/unknown_%s.png" xpos 775 ypos 350 focus_mask True

        # Row 4

        imagebutton auto "gui/unknown_%s.png" xpos 275 ypos 475 focus_mask True

        if met_madison:

            imagebutton auto "gui/madison_%s.png" xpos 400 ypos 475 focus_mask True action ShowMenu("card", girl="Madison")

        else:

            imagebutton auto "gui/unknown_%s.png" xpos 400 ypos 475 focus_mask True

        if met_penny:

            imagebutton auto "gui/penny_%s.png" xpos 525 ypos 475 focus_mask True action ShowMenu("card", girl="Penny")

        else:

            imagebutton auto "gui/unknown_%s.png" xpos 525 ypos 475 focus_mask True

        if met_ash:

            imagebutton auto "gui/ash_%s.png" xpos 650 ypos 475 focus_mask True action ShowMenu("card", girl="Ash")

        else:

            imagebutton auto "gui/unknown_%s.png" xpos 650 ypos 475 focus_mask True

        if met_trish:

            imagebutton auto "gui/trish_%s.png" xpos 775 ypos 475 focus_mask True action ShowMenu("card", girl="Trish")

        else:

            imagebutton auto "gui/unknown_%s.png" xpos 775 ypos 475 focus_mask True

        # Row 5

        imagebutton auto "gui/unknown_%s.png" xpos 275 ypos 600 focus_mask True

        imagebutton auto "gui/unknown_%s.png" xpos 400 ypos 600 focus_mask True

        if met_jules:

            imagebutton auto "gui/jules_%s.png" xpos 525 ypos 600 focus_mask True action ShowMenu("card", girl="Jules")

        else:

            imagebutton auto "gui/unknown_%s.png" xpos 525 ypos 600 focus_mask True

        if met_jenna:

            imagebutton auto "gui/jenna_%s.png" xpos 650 ypos 600 focus_mask True action ShowMenu("card", girl="Jenna")

        else:

            imagebutton auto "gui/unknown_%s.png" xpos 650 ypos 600 focus_mask True

        imagebutton auto "gui/unknown_%s.png" xpos 775 ypos 600 focus_mask True