init offset = 2
style crabs_checkbox:
    properties gui.button_properties("check_button")
    foreground "crabs_mods/gui/button/check_[prefix_]foreground.png"

init python:

    if persistent.crabsinthekitchen_wt_mod and not "crabs_mod_enabled" in config.variants:
        config.variants.insert(0, "crabs_mod_enabled")
    elif not persistent.crabsinthekitchen_wt_mod and "crabs_mod_enabled" in config.variants:
        config.variants.remove("crabs_mod_enabled")

    def crabs_toggle_mod():
        if persistent.crabsinthekitchen_wt_mod and not "crabs_mod_enabled" in config.variants:
            config.variants.insert(0, "crabs_mod_enabled")
        elif not persistent.crabsinthekitchen_wt_mod and "crabs_mod_enabled" in config.variants:
            config.variants.remove("crabs_mod_enabled")

    persistent.crabsinthekitchen_unofficial_patch = True
    def crabs_get_daily_choices():
        crabs_d1_choices = {
            "Give her a pen.": [
                ("ash", "normal", True),#2 Day 2 points
                ("chloe", "normal", True),#Day 2 point
                ("isabella", "normal", True),#2 Day 2 points. Isabella needs 4
                ("katy", "required", True),#4 points total
                ("zoey", "normal", True)#obviously
            ],
            "Ignore her.": [
                ("madison", "normal", True),#Day 2 and Day 3 points
                ("rachel", "normal", True),#Day 2 library point
                ("sarah", "normal", True),#For max points you have to ignore and join ero-club
                ("trish", "normal", True)#2 Day 2 points
            ],
            "Ignore him":[
                ("ash", "required", True),#Required to join the club
                ("chloe", "normal", True),#Day 2 club scene (actually optional but to avoid spoilers)
                ("isabella", "normal", True),#Isabella needs 4 points and you miss 2/5 if you punch him
                ("jenna", "normal", True),#Required to join the club
                ("jules", "normal", True),#Day 2 art class
                ("katy", "normal", flag_gave_pen),#If you punch Caleb and on Katy path, you cannot watch a movie with Rachel
                #if you ignored Zoey, you won't meet Katy anyway, so no need to show it
                ("rachel", "normal", True),#Day 2 art class skip, Day 3 no club
                ("sarah", "normal", True)#You miss 2 club-related scenes and a movie with Rachel if you punch here
            ],
            "Punch him":[
                ("caroline", "normal", True),#Day 1 and Day 4 points
                ("emma", "normal", True),#Day 2 point
                ("holmes", "required", True),#Day 1 and Day 3 points (3 total)
                ("malia", "normal", True),#Day 2 point
                ("trish", "normal", True)#Day 3 point
            ],
            "Sure, I'll check it out.":[
                ("ash", "required", True),#Ash is a club member
                ("chloe", "normal", True),#Day 2 club meeting
                ("jenna", "normal", True),#Day 3 club meeting
                ("sarah", "normal", True),#Day 3 and 5 club-related decisions
            ],
            "No thanks. I'll pass.":[
                ("isabella", "required", True),#You miss all points with Isabella if you join
                ("rachel", "normal", True),#Day 3 point
            ],
            "It won't happen again.":[("caroline", "normal", True)],#Day 1 and Day 4 points
            "I'll do what I want.":[("holmes", "normal", True)]
        }
        crabs_d2_choices = {
            "Skip art class":[
                ("rachel", "normal", True),
                ("katy", "normal", flag_day1_club and flag_gave_pen),#If you join the club and don't ignore Zoey, that's the only way to get Rachel points for movie
                ("sarah", "normal", flag_day1_club and flag_gave_pen)
            ],
            "Attend art class":[("jules", "normal", True)],
            "Text back":[("emma", "normal", True)],
            "Ignore the text":[("malia","normal",True)],
            "Check out city":[
                ("zoey","normal",True),
                ("katy","required",True)#4/5 Katy points
            ],
            "Decline":[
                ("ash","normal",flag_day1_club),
                ("chloe","normal",flag_day1_club),
                ("isabella","normal", not flag_day1_club),
                ("sarah", "normal", not flag_day1_club),#if not in the club you need this Sarah point
                ("emma","normal",True)#Day 3 pizza is only available if you don't meet Katy
            ],
            "Study science":[
                ("trish", "normal", True),
                ("jules", "normal", True)#Day 3 macarons conflict with downtown
            ],
            "Study history":[
                ("madison", "normal", True),
                ("rachel", "normal", True),
                ("sarah", "normal", True),
                ("katy","normal",True)

            ],
            "Doom metal":[("katy","normal",True)],
            "Take the blame":[
                ("ash","normal", True),
                ("rachel","normal", True),#avoid Chloe/Trish/Emma points for Katy/Rachel/Sarah points on day 4
                ("katy","normal", True),
                ("sarah","normal", True)
            ],
            "Act offended":[("chloe","normal",True)],
            "Sharks":[("isabella","normal",True)],
            "Reapers":[("sarah", "normal", True)],
            "Go to library":[("rachel","normal",True)],
            "Go home":[("sarah", "normal", True)],
            "Research":[("trish", "normal", True)],
            "Camgirls":[("chloe","normal",True)]#gives points on Day 11
        }
        crabs_d3_choices = {
            "Sorry, professor.":[("zoey","normal",True)],
            "Sure, no problem.":[("penny","normal",True)],
            "Tell him":[("jenna","normal",True)],
            "Don't tell him":[("sarah","normal",True)],
            "Go home":[("isabella","normal",True)],
            "Explore college":[
                ("rachel","normal",True),
                ("katy","normal",points_rachel == max(points_emma, points_chloe, points_trish)),
                ("sarah","normal",points_rachel == max(points_emma, points_chloe, points_trish))
            ],
            "He started it":[("trish","normal",True)],
            "I started it":[("holmes","normal",True)],
            "Order pizza at home":[
                ("emma","normal",True),
                ("jenna","normal",flag_day2_madison and flag_day3_jenna)
            ],
            "See if Katy is working":[("katy","normal",True)],
            "Look for macarons":[
                ("madison", "normal", True),
                ("rachel","normal",points_rachel == (points_emma + 1) and not flag_day2_zoey),#to avoid Emma point
                ("sarah","normal",points_rachel == (points_emma + 1) and not flag_day2_zoey),
                ("katy","normal", True)
            ],
            "Explore downtown":[
                ("jules", "normal", True),
                ("holmes", "required",points_katy == 2),
                ("rachel","normal",points_rachel == (points_emma + 1) and not flag_day2_zoey),#to avoid Emma point
                ("sarah","normal",points_rachel == (points_emma + 1) and not flag_day2_zoey)
            ]
        }
        crabs_d4_choices = {
            #Chloe and Penny choice comes on Day 5 if you don't bully here
            "Defend Jenna":[
                ("jenna","normal",True),
                ("chloe","normal",True),
                ("penny","normal",True)
            ],
            "Defend Madison":[
                ("madison","normal",True),
                ("chloe","normal",True),
                ("penny","normal",True)
            ],
            #Bullying Jenna or Madison leads to a choice between Caroline and Malia
            "Bully Jenna":[
                ("jules","normal",True),
                ("caroline","normal",True),
                ("malia","normal",True)
            ],
            "Bully Madison":[
                ("jules","normal",True),
                ("caroline","normal",True),
                ("malia","normal",True)
            ],
            "Visit Rachel's dorm":[
                ("rachel","normal",True),
                ("katy","normal",True),
                ("sarah","normal",True)
            ],#needs Rachel > Trish,Emma or Chloe
            "Visit the dormitories":[
                ("rachel","normal",True),
                ("katy","normal",True),
                ("sarah","normal",True)
            ],
            "Visit Trish at lab room":[
                ("trish","normal",True),
                ("chloe","normal",True),
                ("emma","normal",True)
            ],
            "Check out the science building":[
                ("trish","normal",True),
                ("chloe","normal",True),
                ("emma","normal",True)
            ],
            "Don't visit":[
                ("penny","normal",True),
                ("caroline","normal",flag_day1_apologize),
                ("malia","normal",True)
            ],
            "Zombie movie":[("katy","normal",True)],
            "Classic movie":[
                ("sarah","normal",True),
                ("holmes","required",points_katy == 2)
            ],
            "Dean Blakeman":[("malia","normal",True)],
            "Assistant Dean Kelly":[("caroline","normal",True)],
            "Order":[("chloe","normal",True)],
            "Chaos":[("emma","normal",True)]
        }
        crabs_d5_choices = {
            "Watch video":[("chloe","normal",True)],#also counts as watching SluttyDemon
            "Sit down":[("penny","normal",True)],
            "Think about it":[("malia","normal",True)],
            "Go to assistant dean's office":[("caroline","normal",True)],
            "Eavesdrop":[("madison","required",True)],#Required Madison scene
            "Leave them":[("emma","normal",True)],
            "Go to ero-games club":[("ash","normal",True)],
            "Do something else":[
                ("sarah","normal",True),
                ("madison","normal",True)#Day 6 restaurant
            ],
            #Jenna scene on Day 7 happens after visiting Jules so you have to lie here
            "Lie":[
                ("jules","required",True),
                ("jenna","required",True)
            ],
            "Give information":[("holmes","normal",True)]
        }
        crabs_d6_choices = {
            #Caroline here to avoid being locked with Jules/Malia choice on Day 7
            "Help her":[
                ("penny","normal",True),
                ("caroline","normal",flag_day5_jules)
            ],
            #Rachel Day 7 can be got either from Malia or Caroline's scene
            "Answer Malia's call":[
                ("malia","required" if flag_day5_jules else "normal",True),
                ("chloe","required",flag_day5_jules)#Day 7
            ],
            "Go to the movies":[("ash","normal",True)],
            "Go to the restaurant":[("madison","normal",True)],
            "Stay with Sarah":[("sarah","normal",True)],
            "Just watch":[("jenna","normal",True)],
            "Say hello":[
                ("holmes","required",points_holmes >= 3),
                ("trish","required",points_trish >= 3),
            ],
            "Go right":[("holmes","required",True)],
            "Go left":[("trish","normal",True)],
            "Decline":[("jenna","normal",True)],
            "Go to Bob's Corner":[
                ("katy","required",points_katy >= 3),
                ("isabella","required",points_isabella >= 3),
            ],
            "Flirt with Katy":[("katy","required",True)],
            "Focus on Isabella's game":[("isabella","required",True)]
        }
        crabs_d7_choices = {
            "Visit Jules":[
                ("jules","normal",True),
                ("jenna","required",True)
            ],
            "Meet up with Malia":[
                ("malia","required",True),
                ("chloe","required",points_chloe > 0),
                ("rachel","normal",flag_day5_jules)
            ],
            "Don't meet up with Malia":[("caroline","normal",True)],
            "Don't visit Jules":[
                ("caroline","normal",True),
                ("rachel","normal",True)
            ],
            "Cross street to Jenna":[("jenna","required",True)],
            "Walk with Caroline":[("caroline","normal",True)],
            "Go to college":[("rachel","normal",True)],
            "Rachel's dorm":[
                ("rachel","normal",True),
                ("emma","normal",points_chloe > 0)
            ],
            "Go to library":[("chloe","required",True)],
            "Keep listening to Chloe":[("chloe","required",True)],
            "Shop with Penny":[("penny","normal",True)],
            "Meet Emma at home":[("emma","normal",True)],
        }
        crabs_d8_choices = {
            "Hurry to English Class":[("madison","normal",True)],
            "Coffee with Trish":[("trish","normal",True)],
            "Trail Emma":[("emma","important",True)],
            "Slowly go to class":[("sarah","important",True)],
            "Take CPR class":[("isabella","required",True)],
            "Talk with Penny":[("penny","normal",True)],
            "Coffee with Jenna":[("jenna","important",True)],
            "Visit Caroline's office":[("caroline","normal",True)],
            "Go to ero-games club":[("jenna","normal",True)],
            "Hope Katy is working":[("katy","normal",True)],
            "Fast food with Malia":[("malia","important",True)],
            "Watch camgirls":[("chloe","normal",True)]
        }
        crabs_d9_choices = {
            "Visit Jules later":[("jules","required",True)],
            "Accept":[("jules/chloe","subroute",True)],
            "Visit Chloe later":[("chloe","important",True)],
            "Explore downtown side streets":[("madison","important",True)],
            "Talk with Rachel":[("rachel","important",True)],
            "Find Katy":[("katy","normal",True)],
            "Have Zoey help Jenna":[("jenna","normal",True)],
            "Try to help Jenna":[("jenna","normal",True)],
            "Coffee with Jenna":[("jenna","required",True)],
            "Ask Holmes about Florist":[("holmes","important",True)],
            "Go with Emma":[("emma","normal",True)],
            "Look for Trish":[("trish","important",True)],
            "Confront Caroline":[("caroline","required",True)],
            "Visit Caroline's office":[("caroline","required",True)],
            "Tuesday club meeting":[("ash","normal",True)],
            "Trail Emma":[("emma","required",True)]
        }
        crabs_d10_choices = {
            "Go to Rachel's dorm":[("rachel","important",True)],
            "Skip class":[("sarah","normal",True)],
            "Slowly go to class":[("sarah","required",True)],
            "Talk to Madison":[("madison","normal",True)],
            "Ask Holmes about Florist":[("holmes","required",True)],
            "Talk with Rachel":[("rachel","required",True)],
            "Wednesday Club Meeting":[("ash","important",True)],
            "Visit Trish":[("trish","normal" if flag_trish_secret else "required",True)],
            "Eat with Isabella":[("isabella","important",True)],
            "Check up on Jenna":[("jenna","normal",True)],
            "Spy lessons":[("malia","normal",True)],
            "Explore downtown side streets":[("madison","required",True)],
            "Fast food with Malia":[("malia","required",True)],
            "Visit Caroline's apartment":[("caroline","normal",True)],
            "Eat with Holmes":[("holmes","normal",True)],
            "Date with Penny":[("penny","normal",True)]
        }
        crabs_d11_choices = {
            "Find Emma":[("emma","normal",True)],
            "Contact Chloe":[("chloe","important",True)],
            "Visit Rachel's dorm":[("rachel","required",True)],
            "Give Sarah her photo":[("sarah","important",True)],
            "Thursday Club Meeting":[
                ("ash","important" if flag_ash_secret else "required",choices1 == "Ash" or choices2 == "Ash"),
                ("jenna","normal",choices1 == "Jenna" or choices2 == "Jenna"),
            ],
            "Check up on Jules":[("jules","important",True)],
            "Lunch with Isabella":[("isabella","required",True)],
            "Visit Malia's house":[("malia","important",True)],
            "Visit Katy's house":[("katy","normal",True)],
            "Chloe's house":[("chloe","required",True)],
            "See Zoey early":[("zoey","normal",True)],
            "Offer to drop it off":[("jenna","normal",True)],
            "Don't offer":[("ash","important" if flag_ash_secret else "required",True)]
        }
        crabs_d12_choices = {
            "Talk to Caroline":[("caroline","required",True)],
            "Workout with Isabella":[("isabella","normal",True)],
            "Unwind with Zoey":[("zoey","normal",True)],
            "Give Sarah her photo":[("sarah","required",True)],
            "Text Rachel after class":[("rachel","normal",True)],
            "Contact Chloe":[("chloe","required",True)],
            "Follow Emma":[("emma","required",True)],
            "Lessons for Ash":[("ash","normal" if flag_ash_liefor else "important",True)],
            "Friday Downtown":[("madison","required",True)],
            "Visit Jules":[("jules","important" if flag_jules_secret else "required",True)],
            "Confront Emma":[("emma","important",True)],
            "Visit Katy":[("katy","required",True)],
            "Friday Night Patrol":[("trish","required",True)],
            "Invite Holmes":[("holmes","important",True)],
            "Visit Malia's house":[("malia","required",True)],
            "Visit Chloe's House":[("chloe","normal",True)],
            "Accept her affection":[("emma","required",not flag_emma_told_element and flag_emma_closet_decline)]
        }
        crabs_d13_choices = {
            "Visit Sarah":[("sarah","normal",True)],
            "Check up on Jules":[("jules","required",True)],
            "Go out with Jules":[("jules","normal",True)],
            "Workout with Isabella":[("isabella","normal",True)],
            "Go to Isabella's soccer game":[("isabella","normal",True)],
            "Confront Emma":[("emma","required",True)],
            "Accept her affection":[("emma","required",not flag_emma_told_element and flag_emma_closet_decline)],
            "Watch Pugman with Jenna":[("jenna","required",True)],
            "Kiss Jenna":[("jenna","required",True)],#says it's needed in the WT
            "Watch Pugman with Malia":[("malia","normal",True)],
            "Watch Pugman with Rachel":[("rachel","normal",True)],
            "Watch Pugman with Zoey":[("zoey","normal",True)],
            "Quickly answer the back door":[("emma","required",True)],
            "Text back about patrol":[("trish","required",True)],
            "Answer the back door":[("emma","required",True)],
            "Answer the front door":[("holmes","normal",True)],
            "Invite Holmes over":[("holmes","required",True)],
            "Go to Chloe's House":[("chloe","normal",True)],
            "Text Penny about going out":[("penny","normal",True)],
            "Saturday night patrol":[("trish","required",True)],
            "Go out with Penny":[("penny","normal",True)],
            "Go on a date with Penny":[("penny","normal",True)]
        }
        crabs_d14_choices = {
            "Meet with Ash":[("ash","required",True)],
            "Lie to Holmes":[("ash","required",True)],
            "As more than a friend":[("ash","required",not blowjob_ash and not sex_ash)],
            "Meet with Malia":[("malia","required",True)],
            "Return her affection":[("malia","required",flag_malia_lessons_decline and flag_malia_house and flag_malia_house_decline and flag_malia_pugman and flag_malia_pugman_decline)],
            "Meet with Chloe":[("chloe","required",True)],
            "Prove yourself":[("chloe","required",True)],
            "Get explanation from Caroline":[("caroline","required",True)],
            "Reassure her as a lover":[("caroline","required",True)],
            "Swimming Lesson with Isabella":[("isabella","required",True)],
            "Kiss her back":[("isabella","required",flag_isabella_workout_decline and flag_isabella_soccer_decline)],
            "Help Rachel with Ritual":[("rachel","required",True)],
            "Meet with Rachel":[("rachel","required",True)],
            "Invite her upstairs":[("rachel","required",not blowjob_rachel and not handjob_rachel)],
            "See Madison at the BSC":[("madison","required",True)],
            "Kiss her":[("madison","required",True)],
            "Meet up with Holmes":[("holmes","required",True)],
            "Tell her what you know":[("holmes","required",True)],
            "Meet up with Sarah":[("sarah","required",True)],
            #you need total of 4+ points to save Sarah,
            #there are 6 choices, so you don't HAVE to pick ALL of them
            #but since sarah_live is a local variable only used in this specific label, I cannot reference it in the function that works on every text line
            "\"You're right\"":[("sarah","important",True)],
            "Tell her about Ash":[("sarah","important",True)],
            "\"Don't you want more?\"":[("sarah","important",True)],
            "\"You've improved other's\"":[("sarah","important",True)],
            "\"There's one person\"":[("sarah","important",True)],
            "Bring up Isabella":[("sarah","important",True)],
            "\"Joy and sorrow\"":[("sarah","important",True)],
            "\"I want to be with you\"":[("sarah","required",True)],
            "Help Katy with Ritual":[("katy","required",True)],
            "Make love to her":[("katy","required",True)],
            "Go to Jules's Mansion":[("jules","required",True)],
            "Something special":[("jules","required",not handjob_jules and not blowjob_jules)],
            "\"No, don't kill her\"":[("penny","required",True)],#Penny will not die if she has 6 points
            "Don't and reject her":[("emma","required",emma_option),("sarah","required",sarah_option)]
            #Holmes will kill Emma and other girls aren't safe either
            #Ash and Holmes are completely separate since day 1 punch, Sarah and Holmes can be achieved after lowering the requirements
        }
        crabs_d15_choices = {
            "Talk to Zoey upstairs":[("zoey","important",True)],
            "Get to the college":[("caroline","normal",route_caroline),("jules","important",route_jules),("malia","normal",route_malia),("sarah","normal",route_sarah)],
            "Go to Caroline's office":[("caroline","normal",True)],
            "Get a titjob":[("caroline","normal",True)],
            "Talk with Jules":[("jules","important",True)],
            "Kiss her before leaving":[("jules","normal",True)],
            "Talk with Sarah":[("sarah","normal",True)],
            "Have sex with Sarah":[("sarah","normal",True)],
            "Talk with Malia":[("malia","normal",True)],
            "Kiss her":[("malia","normal",True)],

            "Visit Jenna":[("jenna","normal",True)],
            "Kiss Jenna":[("jenna","normal",True)],
            "Visit Trish":[("trish","normal",True)],
            "Kiss Trish":[("trish","normal",True)],
            "Go home with Chloe":[("chloe","normal",True)],
            "Become more intimate":[("chloe","normal",True)],
            "Coffee with Holmes":[("holmes","required",True)],
            "Go with Holmes":[("holmes","normal",True)],

            "Check on Ash":[("ash","normal",True)],
            "Have sex with Ash":[("ash","normal",True)],
            "Invite Isabella over":[("isabella","normal",True)],
            "Kiss Isabella":[("isabella","normal",True)],
            "Text Penny":[("penny","normal",True)],
            "Suggest patching things up":[("penny/zoey", "subroute", route_zoey)],
            "Invite her upstairs":[("penny","normal",True)],
            "Have sex with her":[("penny","normal",True)],
            
            "Have Jules meet Emma": [("emma/jules", "subroute", True)],
            "Tell Emma about Madison":[("emma/madison", "subroute", True)],
            "Invite Katy and Zoey":[("katy/zoey","subroute",True)],
            "Invite Rachel and Zoey":[("zoey/rachel","subroute",True)],
            "Invite Katy, Rachel and Zoey":[("katy/zoey/rachel","subroute",True)],
        }
        crabs_d16_choices = {
            "Invite Zoey over before class":[("zoey","required",True)],
            "Go to the college":[("caroline","normal",route_caroline),("jules","required",route_jules and not flag_jules_talk),("malia","normal",route_malia),("penny","normal",route_penny)],#should I show Katy's doppelganger?
            "Visit Caroline later":[("caroline","normal",True)],
            "Make love to Caroline":[("caroline","normal",True)],
            "Try to find Jules":[("jules","required",True)],
            "Kiss her before leaving":[("jules","normal",True)],
            "Stay home with Emma":[("emma","normal",True)],
            "Shower sex with Emma":[("emma","normal",True)],
            "Try to find Malia":[("malia","normal",True)],
            "Get blowjob from Malia":[("malia","normal",True)],
            #"Try to find Katy":[("katy","normal",True)],
            "Go to English class":[("penny", "normal", True)],
            "Unzip your pants":[("penny","normal",True)],

            "Seduce Chloe for pics":[("jules/chloe", "subroute", route_jules_cruel)],
            "Explain in private":[("chloe/madison", "subroute", True)],

            "Show Sarah the Element":[("sarah", "normal", True)],
            "Invite Sarah to your place":[("sarah", "normal", True)],
            "Find Joey at the club room":[("holmes", "normal", True)],
            "Have sex with Holmes":[("holmes", "normal", True)],
            "Meet up with Trish":[("trish", "normal", True)],
            "Fool around with Trish":[("trish", "normal", True)],
            "Meet up with Zoey":[("zoey", "normal", True)],
            "Kiss Zoey":[("zoey", "normal", True)],
            "Visit Jenna":[("jenna","normal",True)],
            "Kiss Jenna":[("jenna","normal",True)],
            "Go home with Chloe":[("chloe","normal",True)],
            "Become more intimate":[("chloe","normal",True)],

            "Take Jenna to the Element":[("jenna", "normal", True), ("jenna/ash", "subroute", flag_ash_motel and not rejected_ash), ("jenna/zoey", "subroute", flag_zoey_late)],
            "Meet with Ash at Jenna's":[("jenna/ash", "subroute", True)],
            "Sleep with Jenna":[("jenna", "normal", True)],
            "Encourage Zoey and Jenna":[("jenna/zoey", "subroute", True)],
            "Shopping with Isabella":[("isabella", "normal", True), ("jules/isabella", "subroute", flag_jules_talk and not rejected_jules)],
            "Have Isabella and Jules meet":[("jules/isabella", "subroute", True)],
            "Fool around":[("isabella", "normal", True)],
            "Invite Madison over":[("madison","normal",True)],
            "Make love to Madison":[("madison","normal",True)],
            "Help Rachel with curse":[("rachel","normal",True)],
            "Give her your seed":[("rachel","normal",True)],
            "Check on Ash":[("ash","normal",True)],
            "Have sex with Ash":[("ash","normal",True)],
            "Invite Penny and Zoey over":[("penny/zoey", "subroute", flag_zoey_late and flag_penny_friendship and flag_penny_return)],
            "Have Ash meet Sarah":[("ash/sarah", "subroute", flag_ash_motel and not rejected_ash and flag_sarah_element2)],
            "Meet with Penny and Zoey":[("penny/zoey", "subroute", flag_zoey_late and flag_penny_friendship and flag_penny_return)],
            "Meet with Ash and Sarah":[("ash/sarah", "subroute", flag_ash_motel and not rejected_ash and flag_sarah_element2)]
        }
        return {
            1:crabs_d1_choices,
            2:crabs_d2_choices,
            3:crabs_d3_choices,
            4:crabs_d4_choices,
            5:crabs_d5_choices,
            6:crabs_d6_choices,
            7:crabs_d7_choices,
            8:crabs_d8_choices,
            9:crabs_d9_choices,
            10:crabs_d10_choices,
            11:crabs_d11_choices,
            12:crabs_d12_choices,
            13:crabs_d13_choices,
            14:crabs_d14_choices,
            15:crabs_d15_choices,
            16:crabs_d16_choices
        }

    def crabs_say_text_filter(text):
        replacement = text
        if crabs_old_menu_filter is not None:
            replacement = crabs_old_menu_filter(text)

        if persistent.crabsinthekitchen_wt_mod:
            return crabs_choice_enhancer(text, replacement)
        else:
            return replacement

    def crabs_choice_enhancer(text, replacement):
        frame = sys._getframe()
        choice_screen = False
        for i in range(0,3,1):
            frame = frame.f_back
            if "label" in frame.f_locals.keys() and frame.f_locals["label"] == text:
                choice_screen = True
        if persistent.crabsinthekitchen_wt_mod and choice_screen:
            crabs_result = ",".join(map(lambda x: x[0] + "_" + x[1],
                                filter(lambda x: x[2], crabs_get_daily_choices()
                                       .get(gamedate, {})
                                       .get(text, []))))
            return replacement if len(crabs_result) == 0 else replacement +"|"+crabs_result
        else:
            return replacement

    crabs_old_menu_filter = config.say_menu_text_filter
    config.say_menu_text_filter = crabs_say_text_filter

screen choice(items):
    variant "crabs_mod_enabled"
    style_prefix "choice"

    hbox:
        for i in items:
            if '|' in i.caption:
                $ enhanced_caption = i.caption.split('|')
                $ orig_caption = enhanced_caption[0]
                $ crabsinthekitchen_wt_mod_names = enhanced_caption[1].split(',')
                textbutton orig_caption action i.action tooltip enhanced_caption[1] hovered CaptureFocus("enhanced_choice") unhovered ClearFocus("enhanced_choice")
            else:
                textbutton i.caption action i.action
    nearrect:
        focus "enhanced_choice"
        prefer_top True
        has hbox
        $ tooltip = GetTooltip(screen="choice")

        if tooltip is not None:
            $ crabsinthekitchen_wt_mod_names = tooltip.split(',')
            for crabsinthekitchen_wt_mod_name in crabsinthekitchen_wt_mod_names:
                $ crabsinthekitchen_wt_mod_name_offset = crabsinthekitchen_wt_mod_names.index(crabsinthekitchen_wt_mod_name)/16.0
                $ color = "#cfcfcf"
                if "required" in crabsinthekitchen_wt_mod_name:
                    $ color = "#f00"
                elif "important" in crabsinthekitchen_wt_mod_name:
                    $ color = "#0f0"
                elif "subroute" in crabsinthekitchen_wt_mod_name:
                    $ color = "#1e90ff"
                
                if "subroute" in crabsinthekitchen_wt_mod_name:
                    $ crabs_subroute_names = crabsinthekitchen_wt_mod_name.split("/")
                    $ crabs_subroute_names[-1] = crabs_subroute_names[-1][0:crabs_subroute_names[-1].index("_")]
                    $ comp_args = []
                    for crabs_sr_name in crabs_subroute_names:
                        $ comp_args.append((5+92*crabs_subroute_names.index(crabs_sr_name), 5))
                        $ comp_args.append(Crop((5,5,82,82), "gui/%s_idle.png" % crabs_sr_name))
                    $ res = Composite((92*len(crabs_subroute_names), 92),
                    (0,0), Solid(color),
                    *comp_args)
                else:
                    $ res = Composite((92,92),
                    (0,0), Solid(color),
                    (5,5), Crop((5,5,82,82), "gui/"+crabsinthekitchen_wt_mod_name[0:crabsinthekitchen_wt_mod_name.index("_")]+"_idle.png"))
                add res
