define crabs_tutorial = False

label tutorial:
    $ _skipping = False
    $ show_clock = True
    $ crabs_tutorial = True
    $ quick_menu = False
    $ toggle_mod = False
    $ renpy.movie_cutscene('movies/element174_op1.webm')
    scene CollegeMorning-OutsideDay1
    with dissolve

    "Welcome to Element-174, a sci-fi visual novel with a focus on choices and how our decisions cause ripples in our story."

    "Almost every decision matters and can lead to a romance with one of 16 different girls."

    scene JulesChloeFaceoff
    with dissolve

    "But this is {i}NOT{/i} a  harem game and you won't be able to romance 'em all."

    scene GastropubHolmesLeaving
    with dissolve

    "Every decision you make can close someone's route permanently."

    scene HomeEmmaDead with dissolve
    scene RooftopSarahLeap4 with dissolve
    scene PoolIsabellaDrown2 with dissolve
    if renpy.loadable("images/images2/CampbellParentsTV.jpg"):
        scene CampbellParentsTV with dissolve
    scene CollegePennyExplosion with dissolve

    "{font=font3.ttf}{size=+20}{color=#f00}PERMANENTLY{/color}{/size}{/font}."

    scene CollegeMorning-OutsideDay1
    with dissolve
    "So if you want to follow a specific girl's route but are not sure what choices you need to make, you can the game has built-in walkthrough."

    if not persistent.crabsinthekitchen_wt_mod:
        $ persistent.crabsinthekitchen_wt_mod = True
        $ crabs_toggle_mod()
        $ toggle_mod = True

    show screen crabs_wt_overlay

    "It will add the game logo button to the top right corner."
    "If you click on it or press \"W\" you will see a calendar"

    show screen crabs_calendar
    "Clicking on each day will show you the chart of all scenes you could see that day"
    hide screen crabs_calendar
    show screen crabs_day1
    "To hide the chart, click the logo again or press \"ESC\""

    "Also, with the walkthrough enabled, you'll see girls' portraits over the important decisions."
    "Let's show an example."

    scene ZoeyWhisper with dissolve

    som "{i}Hey! Do you have something to write with?{/i}"


    som "{i}My pen ran out of ink. Anything will do: pen, pencil...{/i}"

    scene ZoeyWhisperB with dissolve

    call effectchoice

    menu:

        "Give her a pen.":
            call madechoice
            
            scene ZoeyWhisper with dissolve

            n "I reach in my bag, pull out a spare pen and hand it to her."

        "Ignore her.":
            
            call madechoice

            scene ZoeyWhisper with dissolve

            n "I decide not to acknowledge her."

    scene EnglishClass-01005
    "As you could see, some portraits have a red border around them."

    "When a girl has a red border around her portrait, it means that you have to pick that option, or her route will be closed forever."

    "Starting from the second week, some choices can have a green border around them instead of a red one."
    $ gamedate = 8

    scene CollegeEmmaFollow0B with dissolve

    call effectchoice

    menu:

        "Trail Emma":
            pass
    
        "Hurry to English Class":
            pass

    "When a girl has a green border around her portrait, it means that the decision is important."
    "Not seeing the scene following that decision can close her route, but you will have another chance later if you don't choose it now."
    "When the chance to see the scene appears again, the portrait will have a red border around it, so you won't miss it."
    "Sometimes, there can be multiple important decisions in the same conversation, and you have to make a certain amount of the right choices to keep the route open."
    "In that case, right choices will also have a green border around them."
    if renpy.loadable("day15.rpy"):
        scene HomePennyNeutral2 with dissolve
        "Starting from Day 15, some decisions can open shared sub-routes between girls"
        "In these sub-routes, there will be more interactions between girls and these interactions can affect the ending"
        "If the decision possibly opens a sub-route, the tooltip will show two portraits with a blue border"

        $ gamedate = 15

        scene HomePennyNeutral2B with dissolve

        call effectchoice

        menu:

            "Suggest patching things up":
                pass
        
            "Don't suggest it":
                pass

    "The last thing the walkthrough adds is the \"Compatible subroutes\" button on character screens."
    scene blackscreen
    show screen tutorial_char_screen
    "Here, the routes are separated into 3 tiers."
    "At the top you have compatible subroutes that will affect the main route at the end of the game."
    "Below that, you have subroutes that don't conflict with each other until the end of the current version but aren't guaranteed to be compatible in the future."
    "And at the bottom you have routes that are not compatible with this character at all."
    "Clicking on any character portrait will open their own compatible subroutes screen."

    "That's all for now, have fun!"

    if toggle_mod:
        $ persistent.crabsinthekitchen_wt_mod = False
        $ crabs_toggle_mod()

    $ quick_menu = True
    $ suppress_overlay = False

    $_skipping = True
    return

screen tutorial_char_screen():
    frame:
        background "#0099cc"
        ymaximum 600
        yalign 0.5
        vbox:
            text ("{size=+10}Compatible subroutes") xalign 0.35 yalign 0.0
            frame:
                ymaximum 100
                background "#5bf400aa"
                imagebutton auto "gui/madison_%s.png" xalign 0.4 yalign 0.0 action [Hide("crabs_compatible_routes_emma"), ShowMenu("crabs_compatible_routes_madison")]
                imagebutton auto "gui/jules_%s.png" xalign 0.5 yalign 0.0 action [Hide("crabs_compatible_routes_emma"), ShowMenu("crabs_compatible_routes_jules")]

            frame:
                ymaximum 300
                background "#cab50099"
                imagebutton auto "gui/penny_%s.png" xalign 0.6 yalign 0.0 action [Hide("crabs_compatible_routes_emma"), ShowMenu("crabs_compatible_routes_penny")]
                imagebutton auto "gui/rachel_%s.png" xalign 0.3 yalign 0.0 action [Hide("crabs_compatible_routes_emma"), ShowMenu("crabs_compatible_routes_rachel")]
                imagebutton auto "gui/caroline_%s.png" xalign 0.5 yalign 0.0 action [Hide("crabs_compatible_routes_emma"), ShowMenu("crabs_compatible_routes_caroline")]
                imagebutton auto "gui/chloe_%s.png" xalign 0.4 yalign 0.0 action [Hide("crabs_compatible_routes_emma"), ShowMenu("crabs_compatible_routes_chloe")]

                imagebutton auto "gui/zoey_%s.png" xalign 0.3 yalign 0.5 action [Hide("crabs_compatible_routes_emma"), ShowMenu("crabs_compatible_routes_zoey")]
                imagebutton auto "gui/katy_%s.png" xalign 0.4 yalign 0.5 action [Hide("crabs_compatible_routes_emma"), ShowMenu("crabs_compatible_routes_katy")]
                imagebutton auto "gui/sarah_%s.png" xalign 0.6 yalign 0.5 action [Hide("crabs_compatible_routes_emma"), ShowMenu("crabs_compatible_routes_sarah")]
                imagebutton auto "gui/isabella_%s.png" xalign 0.5 yalign 0.5 action [Hide("crabs_compatible_routes_emma"), ShowMenu("crabs_compatible_routes_isabella")]

                imagebutton auto "gui/ash_%s.png" xalign 0.45 yalign 1.0 action [Hide("crabs_compatible_routes_emma"), ShowMenu("crabs_compatible_routes_ash")]
                imagebutton auto "gui/malia_%s.png" xalign 0.55 yalign 1.0 action [Hide("crabs_compatible_routes_emma"), ShowMenu("crabs_compatible_routes_malia")]
                imagebutton auto "gui/jenna_%s.png" xalign 0.35 yalign 1.0 action [Hide("crabs_compatible_routes_emma"), ShowMenu("crabs_compatible_routes_jenna")]

            frame:
                ymaximum 100
                background "#fc4f00"
                imagebutton auto "gui/holmes_%s.png" xalign 0.5 action [Hide("crabs_compatible_routes_emma"), ShowMenu("crabs_compatible_routes_holmes")]
                imagebutton auto "gui/trish_%s.png" xalign 0.4 action [Hide("crabs_compatible_routes_emma"), ShowMenu("crabs_compatible_routes_trish")]

        add "sheet_emma.png" xalign 0 yalign 1 zoom 0.25