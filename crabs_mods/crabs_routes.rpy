screen crabs_compatible_routes_zoey():
    predict False
    use game_menu(_("Zoey"), scroll="viewport"):
        frame:
            background "#0099cc"
            ymaximum 750
            vbox:
                text ("{size=+10}Compatible subroutes{/size}") xalign 0.35
                frame:
                    ymaximum 150
                    background "#5bf400aa"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "Confirmed subroutes" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/penny_%s.png" action [Hide("crabs_compatible_routes_zoey"), ShowMenu("crabs_compatible_routes_penny")]
                            imagebutton auto "gui/katy_%s.png" action [Hide("crabs_compatible_routes_zoey"), ShowMenu("crabs_compatible_routes_katy")]
                            imagebutton auto "gui/rachel_%s.png" action [Hide("crabs_compatible_routes_zoey"), ShowMenu("crabs_compatible_routes_rachel")]
                            imagebutton auto "gui/jenna_%s.png" action [Hide("crabs_compatible_routes_zoey"), ShowMenu("crabs_compatible_routes_jenna")]
                frame:
                    ymaximum 350
                    background "#cab50099"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "No route conflicts in current version" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/ash_%s.png" action [Hide("crabs_compatible_routes_zoey"), ShowMenu("crabs_compatible_routes_ash")]
                            imagebutton auto "gui/isabella_%s.png" action [Hide("crabs_compatible_routes_zoey"), ShowMenu("crabs_compatible_routes_isabella")]
                            imagebutton auto "gui/sarah_%s.png" action [Hide("crabs_compatible_routes_zoey"), ShowMenu("crabs_compatible_routes_sarah")]
                            imagebutton auto "gui/trish_%s.png" action [Hide("crabs_compatible_routes_zoey"), ShowMenu("crabs_compatible_routes_trish")]
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/caroline_%s.png" action [Hide("crabs_compatible_routes_zoey"), ShowMenu("crabs_compatible_routes_caroline")]
                            imagebutton auto "gui/chloe_%s.png" action [Hide("crabs_compatible_routes_zoey"), ShowMenu("crabs_compatible_routes_chloe")]
                            imagebutton auto "gui/madison_%s.png" action [Hide("crabs_compatible_routes_zoey"), ShowMenu("crabs_compatible_routes_madison")]
                            imagebutton auto "gui/jules_%s.png" action [Hide("crabs_compatible_routes_zoey"), ShowMenu("crabs_compatible_routes_jules")]
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/holmes_%s.png" action [Hide("crabs_compatible_routes_zoey"), ShowMenu("crabs_compatible_routes_holmes")]
                            imagebutton auto "gui/malia_%s.png" action [Hide("crabs_compatible_routes_zoey"), ShowMenu("crabs_compatible_routes_malia")]
                            imagebutton auto "gui/emma_%s.png" action [Hide("crabs_compatible_routes_zoey"), ShowMenu("crabs_compatible_routes_emma")]
                    
                frame:
                    ymaximum 200
                    background "#fc4f00"
            add "sheet_zoey.png" xalign 0 yalign 1 zoom 0.4

        textbutton "Back" action Hide("crabs_compatible_routes_zoey")

screen crabs_compatible_routes_penny():
    predict False
    use game_menu(_("Penny"), scroll="viewport"):
        frame:
            background "#0099cc"
            ymaximum 750
            vbox:
                text ("{size=+10}Compatible subroutes{/size}") xalign 0.35
                frame:
                    ymaximum 150
                    background "#5bf400aa"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "Confirmed subroutes" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/zoey_%s.png" action [Hide("crabs_compatible_routes_penny"), ShowMenu("crabs_compatible_routes_zoey")]
                            imagebutton auto "gui/holmes_%s.png" action [Hide("crabs_compatible_routes_penny"), ShowMenu("crabs_compatible_routes_holmes")]
                frame:
                    ymaximum 350
                    background "#cab50099"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "No route conflicts in current version" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/ash_%s.png" action [Hide("crabs_compatible_routes_penny"), ShowMenu("crabs_compatible_routes_ash")]
                            imagebutton auto "gui/madison_%s.png" action [Hide("crabs_compatible_routes_penny"), ShowMenu("crabs_compatible_routes_madison")]
                            imagebutton auto "gui/katy_%s.png" action [Hide("crabs_compatible_routes_penny"), ShowMenu("crabs_compatible_routes_katy")]
                            imagebutton auto "gui/rachel_%s.png" action [Hide("crabs_compatible_routes_penny"), ShowMenu("crabs_compatible_routes_rachel")]
                            imagebutton auto "gui/jules_%s.png" action [Hide("crabs_compatible_routes_penny"), ShowMenu("crabs_compatible_routes_jules")]
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/jenna_%s.png" action [Hide("crabs_compatible_routes_penny"), ShowMenu("crabs_compatible_routes_jenna")]
                            imagebutton auto "gui/trish_%s.png" action [Hide("crabs_compatible_routes_penny"), ShowMenu("crabs_compatible_routes_trish")]
                            imagebutton auto "gui/sarah_%s.png" action [Hide("crabs_compatible_routes_penny"), ShowMenu("crabs_compatible_routes_sarah")]
                            imagebutton auto "gui/isabella_%s.png" action [Hide("crabs_compatible_routes_penny"), ShowMenu("crabs_compatible_routes_isabella")]
                            imagebutton auto "gui/malia_%s.png" action [Hide("crabs_compatible_routes_penny"), ShowMenu("crabs_compatible_routes_malia")]
                        hbox:
                            xcenter 0.5

                            imagebutton auto "gui/emma_%s.png" action [Hide("crabs_compatible_routes_penny"), ShowMenu("crabs_compatible_routes_emma")]
                            imagebutton auto "gui/chloe_%s.png" action [Hide("crabs_compatible_routes_penny"), ShowMenu("crabs_compatible_routes_chloe")]
                            imagebutton auto "gui/caroline_%s.png" action [Hide("crabs_compatible_routes_penny"), ShowMenu("crabs_compatible_routes_caroline")]

                frame:
                    ymaximum 200
                    background "#fc4f00"
            add "sheet_penny.png" xalign 0 yalign 1 zoom 0.4

        textbutton "Back" action Hide("crabs_compatible_routes_penny")

screen crabs_compatible_routes_ash():
    predict False
    use game_menu(_("Ash"), scroll="viewport"):
        frame:
            background "#0099cc"
            ymaximum 750
            vbox:
                text ("{size=+10}Compatible subroutes{/size}") xalign 0.35
                frame:
                    ymaximum 150
                    background "#5bf400aa"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "Confirmed subroutes" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/jenna_%s.png" action [Hide("crabs_compatible_routes_ash"), ShowMenu("crabs_compatible_routes_jenna")]
                            imagebutton auto "gui/sarah_%s.png" action [Hide("crabs_compatible_routes_ash"), ShowMenu("crabs_compatible_routes_sarah")]

                frame:
                    ymaximum 250
                    background "#cab50099"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "No route conflicts in current version" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/zoey_%s.png" action [Hide("crabs_compatible_routes_ash"), ShowMenu("crabs_compatible_routes_zoey")]
                            imagebutton auto "gui/katy_%s.png" action [Hide("crabs_compatible_routes_ash"), ShowMenu("crabs_compatible_routes_katy")]
                            imagebutton auto "gui/rachel_%s.png" action [Hide("crabs_compatible_routes_ash"), ShowMenu("crabs_compatible_routes_rachel")]
                            imagebutton auto "gui/jules_%s.png" action [Hide("crabs_compatible_routes_ash"), ShowMenu("crabs_compatible_routes_jules")]
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/penny_%s.png" action [Hide("crabs_compatible_routes_ash"), ShowMenu("crabs_compatible_routes_penny")]
                            imagebutton auto "gui/madison_%s.png" action [Hide("crabs_compatible_routes_ash"), ShowMenu("crabs_compatible_routes_madison")]
                            imagebutton auto "gui/trish_%s.png" action [Hide("crabs_compatible_routes_ash"), ShowMenu("crabs_compatible_routes_trish")]
                            imagebutton auto "gui/emma_%s.png" action [Hide("crabs_compatible_routes_ash"), ShowMenu("crabs_compatible_routes_emma")]


                frame:
                    ymaximum 150
                    background "#d47800"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "Routes are mutually exclusive after the second week" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/caroline_%s.png" action [Hide("crabs_compatible_routes_ash"), ShowMenu("crabs_compatible_routes_caroline")]
                            imagebutton auto "gui/malia_%s.png" action [Hide("crabs_compatible_routes_ash"), ShowMenu("crabs_compatible_routes_malia")]
                            imagebutton auto "gui/chloe_%s.png" action [Hide("crabs_compatible_routes_ash"), ShowMenu("crabs_compatible_routes_chloe")]

                frame:
                    ymaximum 150
                    background "#fc4f00"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "Routes are mutually exclusive after the first week" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/holmes_%s.png" action [Hide("crabs_compatible_routes_ash"), ShowMenu("crabs_compatible_routes_holmes")]
                            imagebutton auto "gui/isabella_%s.png" action [Hide("crabs_compatible_routes_ash"), ShowMenu("crabs_compatible_routes_isabella")]

            add "sheet_ash.png" xalign 0 yalign 1 zoom 0.4

        textbutton "Back" action Hide("crabs_compatible_routes_ash")

screen crabs_compatible_routes_caroline():
    predict False
    use game_menu(_("Caroline"), scroll="viewport"):
        frame:
            background "#0099cc"
            ymaximum 750
            vbox:
                text ("{size=+10}Compatible subroutes{/size}") xalign 0.35
                frame:
                    ymaximum 150
                    background "#5bf400aa"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "Confirmed subroutes" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/holmes_%s.png" action [Hide("crabs_compatible_routes_caroline"), ShowMenu("crabs_compatible_routes_holmes")]
                            imagebutton auto "gui/trish_%s.png" action [Hide("crabs_compatible_routes_caroline"), ShowMenu("crabs_compatible_routes_trish")]

                frame:
                    ymaximum 250
                    background "#cab50099"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "No route conflicts in current version" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/katy_%s.png" action [Hide("crabs_compatible_routes_caroline"), ShowMenu("crabs_compatible_routes_katy")]
                            imagebutton auto "gui/rachel_%s.png" action [Hide("crabs_compatible_routes_caroline"), ShowMenu("crabs_compatible_routes_rachel")]
                            imagebutton auto "gui/zoey_%s.png" action [Hide("crabs_compatible_routes_caroline"), ShowMenu("crabs_compatible_routes_zoey")]
                            imagebutton auto "gui/penny_%s.png" action [Hide("crabs_compatible_routes_caroline"), ShowMenu("crabs_compatible_routes_penny")]
                            imagebutton auto "gui/jenna_%s.png" action [Hide("crabs_compatible_routes_caroline"), ShowMenu("crabs_compatible_routes_jenna")]
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/sarah_%s.png" action [Hide("crabs_compatible_routes_caroline"), ShowMenu("crabs_compatible_routes_sarah")]
                            imagebutton auto "gui/isabella_%s.png" action [Hide("crabs_compatible_routes_caroline"), ShowMenu("crabs_compatible_routes_isabella")]
                            imagebutton auto "gui/jules_%s.png" action [Hide("crabs_compatible_routes_caroline"), ShowMenu("crabs_compatible_routes_jules")]
                            imagebutton auto "gui/emma_%s.png" action [Hide("crabs_compatible_routes_caroline"), ShowMenu("crabs_compatible_routes_emma")]
                            imagebutton auto "gui/madison_%s.png" action [Hide("crabs_compatible_routes_caroline"), ShowMenu("crabs_compatible_routes_madison")]

                frame:
                    ymaximum 150
                    background "#fc4f00"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "Routes are mutually exclusive after the second week" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/ash_%s.png" action [Hide("crabs_compatible_routes_caroline"), ShowMenu("crabs_compatible_routes_ash")]
                            imagebutton auto "gui/malia_%s.png" action [Hide("crabs_compatible_routes_caroline"), ShowMenu("crabs_compatible_routes_malia")]
                            imagebutton auto "gui/chloe_%s.png" action [Hide("crabs_compatible_routes_caroline"), ShowMenu("crabs_compatible_routes_chloe")]

            add "sheet_caroline.png" xalign 0 yalign 1 zoom 0.4

        textbutton "Back" action Hide("crabs_compatible_routes_caroline")

screen crabs_compatible_routes_chloe():
    predict False
    use game_menu(_("Chloe"), scroll="viewport"):
        frame:
            background "#0099cc"
            ymaximum 750
            vbox:
                text ("{size=+10}Compatible subroutes{/size}") xalign 0.35
                frame:
                    ymaximum 150
                    background "#5bf400aa"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "Confirmed subroutes" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/madison_%s.png" action [Hide("crabs_compatible_routes_chloe"), ShowMenu("crabs_compatible_routes_madison")]
                            imagebutton auto "gui/jules_%s.png" action [Hide("crabs_compatible_routes_chloe"), ShowMenu("crabs_compatible_routes_jules")]

                frame:
                    ymaximum 250
                    background "#cab50099"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "No route conflicts in current version" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/emma_%s.png" action [Hide("crabs_compatible_routes_chloe"), ShowMenu("crabs_compatible_routes_emma")]
                            imagebutton auto "gui/trish_%s.png" action [Hide("crabs_compatible_routes_chloe"), ShowMenu("crabs_compatible_routes_trish")]
                            imagebutton auto "gui/zoey_%s.png" action [Hide("crabs_compatible_routes_chloe"), ShowMenu("crabs_compatible_routes_zoey")]
                            imagebutton auto "gui/penny_%s.png" action [Hide("crabs_compatible_routes_chloe"), ShowMenu("crabs_compatible_routes_penny")]
                            imagebutton auto "gui/isabella_%s.png" action [Hide("crabs_compatible_routes_chloe"), ShowMenu("crabs_compatible_routes_isabella")]
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/sarah_%s.png" action [Hide("crabs_compatible_routes_chloe"), ShowMenu("crabs_compatible_routes_sarah")]
                            imagebutton auto "gui/katy_%s.png" action [Hide("crabs_compatible_routes_chloe"), ShowMenu("crabs_compatible_routes_katy")]
                            imagebutton auto "gui/rachel_%s.png" action [Hide("crabs_compatible_routes_chloe"), ShowMenu("crabs_compatible_routes_rachel")]
                            imagebutton auto "gui/holmes_%s.png" action [Hide("crabs_compatible_routes_chloe"), ShowMenu("crabs_compatible_routes_holmes")]

                frame:
                    ymaximum 150
                    background "#d47800"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "Routes are mutually exclusive after the second week" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/ash_%s.png" action [Hide("crabs_compatible_routes_chloe"), ShowMenu("crabs_compatible_routes_ash")]
                            imagebutton auto "gui/malia_%s.png" action [Hide("crabs_compatible_routes_chloe"), ShowMenu("crabs_compatible_routes_malia")]
                            imagebutton auto "gui/caroline_%s.png" action [Hide("crabs_compatible_routes_chloe"), ShowMenu("crabs_compatible_routes_caroline")]

                frame:
                    ymaximum 150
                    background "#fc4f00"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "Routes are mutually exclusive after the first week" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/jenna_%s.png" action [Hide("crabs_compatible_routes_chloe"), ShowMenu("crabs_compatible_routes_jenna")]

            add "sheet_chloe.png" xalign 0 yalign 1 zoom 0.4

        textbutton "Back" action Hide("crabs_compatible_routes_chloe")

screen crabs_compatible_routes_emma():
    predict False
    use game_menu(_("Emma"), scroll="viewport"):
        frame:
            background "#0099cc"
            ymaximum 750
            vbox:
                text ("{size=+10}Compatible subroutes{/size}") xalign 0.35
                frame:
                    ymaximum 150
                    background "#5bf400aa"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "Confirmed subroutes" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/madison_%s.png" action [Hide("crabs_compatible_routes_emma"), ShowMenu("crabs_compatible_routes_madison")]
                            imagebutton auto "gui/jules_%s.png" action [Hide("crabs_compatible_routes_emma"), ShowMenu("crabs_compatible_routes_jules")]

                frame:
                    ymaximum 350
                    background "#cab50099"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "No route conflicts in current version" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/penny_%s.png" action [Hide("crabs_compatible_routes_emma"), ShowMenu("crabs_compatible_routes_penny")]
                            imagebutton auto "gui/rachel_%s.png" action [Hide("crabs_compatible_routes_emma"), ShowMenu("crabs_compatible_routes_rachel")]
                            imagebutton auto "gui/caroline_%s.png" action [Hide("crabs_compatible_routes_emma"), ShowMenu("crabs_compatible_routes_caroline")]
                            imagebutton auto "gui/chloe_%s.png" action [Hide("crabs_compatible_routes_emma"), ShowMenu("crabs_compatible_routes_chloe")]
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/zoey_%s.png" action [Hide("crabs_compatible_routes_emma"), ShowMenu("crabs_compatible_routes_zoey")]
                            imagebutton auto "gui/katy_%s.png" action [Hide("crabs_compatible_routes_emma"), ShowMenu("crabs_compatible_routes_katy")]
                            imagebutton auto "gui/sarah_%s.png" action [Hide("crabs_compatible_routes_emma"), ShowMenu("crabs_compatible_routes_sarah")]
                            imagebutton auto "gui/isabella_%s.png" action [Hide("crabs_compatible_routes_emma"), ShowMenu("crabs_compatible_routes_isabella")]
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/ash_%s.png" action [Hide("crabs_compatible_routes_emma"), ShowMenu("crabs_compatible_routes_ash")]
                            imagebutton auto "gui/malia_%s.png" action [Hide("crabs_compatible_routes_emma"), ShowMenu("crabs_compatible_routes_malia")]
                            imagebutton auto "gui/jenna_%s.png" action [Hide("crabs_compatible_routes_emma"), ShowMenu("crabs_compatible_routes_jenna")]

                frame:
                    ymaximum 150
                    background "#fc4f00"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "Routes are mutually exclusive after the second week" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/holmes_%s.png" action [Hide("crabs_compatible_routes_emma"), ShowMenu("crabs_compatible_routes_holmes")]
                            imagebutton auto "gui/trish_%s.png" action [Hide("crabs_compatible_routes_emma"), ShowMenu("crabs_compatible_routes_trish")]

            add "sheet_emma.png" xalign 0 yalign 1 zoom 0.4

        textbutton "Back" action Hide("crabs_compatible_routes_emma")

screen crabs_compatible_routes_holmes():
    predict False
    use game_menu(_("Holmes"), scroll="viewport"):
        frame:
            background "#0099cc"
            ymaximum 750
            vbox:
                text ("{size=+10}Compatible subroutes{/size}") xalign 0.35
                frame:
                    ymaximum 150
                    background "#5bf400aa"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "Confirmed subroutes" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/caroline_%s.png" action [Hide("crabs_compatible_routes_holmes"), ShowMenu("crabs_compatible_routes_caroline")]
                            imagebutton auto "gui/malia_%s.png" action [Hide("crabs_compatible_routes_holmes"), ShowMenu("crabs_compatible_routes_malia")]
                            imagebutton auto "gui/penny_%s.png" action [Hide("crabs_compatible_routes_holmes"), ShowMenu("crabs_compatible_routes_penny")]

                frame:
                    ymaximum 150
                    background "#cab50099"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "No route conflicts in current version" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/jules_%s.png" action [Hide("crabs_compatible_routes_holmes"), ShowMenu("crabs_compatible_routes_jules")]
                            imagebutton auto "gui/chloe_%s.png" action [Hide("crabs_compatible_routes_holmes"), ShowMenu("crabs_compatible_routes_chloe")]
                            imagebutton auto "gui/zoey_%s.png" action [Hide("crabs_compatible_routes_holmes"), ShowMenu("crabs_compatible_routes_zoey")]
                            imagebutton auto "gui/jenna_%s.png" action [Hide("crabs_compatible_routes_holmes"), ShowMenu("crabs_compatible_routes_jenna")]

                frame:
                    ymaximum 150
                    background "#d47800"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "Routes are mutually exclusive after the second week" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/sarah_%s.png" action [Hide("crabs_compatible_routes_holmes"), ShowMenu("crabs_compatible_routes_sarah")]
                            imagebutton auto "gui/madison_%s.png" action [Hide("crabs_compatible_routes_holmes"), ShowMenu("crabs_compatible_routes_madison")]
                            imagebutton auto "gui/emma_%s.png" action [Hide("crabs_compatible_routes_holmes"), ShowMenu("crabs_compatible_routes_holmes")]
                            imagebutton auto "gui/rachel_%s.png" action [Hide("crabs_compatible_routes_holmes"), ShowMenu("crabs_compatible_routes_rachel")]

                frame:
                    ymaximum 150
                    background "#fc4f00"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "Routes are mutually exclusive after the first week" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/ash_%s.png" action [Hide("crabs_compatible_routes_holmes"), ShowMenu("crabs_compatible_routes_ash")]
                            imagebutton auto "gui/katy_%s.png" action [Hide("crabs_compatible_routes_holmes"), ShowMenu("crabs_compatible_routes_katy")]
                            imagebutton auto "gui/isabella_%s.png" action [Hide("crabs_compatible_routes_holmes"), ShowMenu("crabs_compatible_routes_isabella")]
                            imagebutton auto "gui/trish_%s.png" action [Hide("crabs_compatible_routes_holmes"), ShowMenu("crabs_compatible_routes_trish")]

            add "sheet_holmes.png" xalign 0 yalign 1 zoom 0.4

        textbutton "Back" action Hide("crabs_compatible_routes_holmes")

screen crabs_compatible_routes_isabella():
    predict False
    use game_menu(_("Isabella"), scroll="viewport"):
        frame:
            background "#0099cc"
            ymaximum 750
            vbox:
                text ("{size=+10}Compatible subroutes{/size}") xalign 0.35
                frame:
                    ymaximum 150
                    background "#5bf400aa"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "Confirmed subroutes" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/jules_%s.png" action [Hide("crabs_compatible_routes_isabella"), ShowMenu("crabs_compatible_routes_jules")]
                            imagebutton auto "gui/sarah_%s.png" action [Hide("crabs_compatible_routes_isabella"), ShowMenu("crabs_compatible_routes_sarah")]

                frame:
                    ymaximum 250
                    background "#cab50099"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "No route conflicts in current version" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/malia_%s.png" action [Hide("crabs_compatible_routes_isabella"), ShowMenu("crabs_compatible_routes_malia")]
                            imagebutton auto "gui/penny_%s.png" action [Hide("crabs_compatible_routes_isabella"), ShowMenu("crabs_compatible_routes_penny")]
                            imagebutton auto "gui/chloe_%s.png" action [Hide("crabs_compatible_routes_isabella"), ShowMenu("crabs_compatible_routes_chloe")]
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/zoey_%s.png" action [Hide("crabs_compatible_routes_isabella"), ShowMenu("crabs_compatible_routes_zoey")]
                            imagebutton auto "gui/emma_%s.png" action [Hide("crabs_compatible_routes_isabella"), ShowMenu("crabs_compatible_routes_emma")]
                            imagebutton auto "gui/caroline_%s.png" action [Hide("crabs_compatible_routes_isabella"), ShowMenu("crabs_compatible_routes_caroline")]
                frame:
                    ymaximum 150
                    background "#d47800"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "Routes are mutually exclusive after the second week" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/madison_%s.png" action [Hide("crabs_compatible_routes_isabella"), ShowMenu("crabs_compatible_routes_madison")]
                            imagebutton auto "gui/rachel_%s.png" action [Hide("crabs_compatible_routes_isabella"), ShowMenu("crabs_compatible_routes_rachel")]
                frame:
                    ymaximum 150
                    background "#fc4f00"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "Routes are mutually exclusive after the first week" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/ash_%s.png" action [Hide("crabs_compatible_routes_isabella"), ShowMenu("crabs_compatible_routes_ash")]
                            imagebutton auto "gui/jenna_%s.png" action [Hide("crabs_compatible_routes_isabella"), ShowMenu("crabs_compatible_routes_jenna")]
                            imagebutton auto "gui/holmes_%s.png" action [Hide("crabs_compatible_routes_isabella"), ShowMenu("crabs_compatible_routes_holmes")]
                            imagebutton auto "gui/trish_%s.png" action [Hide("crabs_compatible_routes_isabella"), ShowMenu("crabs_compatible_routes_trish")]
                            imagebutton auto "gui/katy_%s.png" action [Hide("crabs_compatible_routes_isabella"), ShowMenu("crabs_compatible_routes_katy")]

            add "sheet_isabella.png" xalign 0 yalign 1 zoom 0.4

        textbutton "Back" action Hide("crabs_compatible_routes_isabella")

screen crabs_compatible_routes_jenna():
    predict False
    use game_menu(_("Jenna"), scroll="viewport"):
        frame:
            background "#0099cc"
            ymaximum 750
            vbox:
                text ("{size=+10}Compatible subroutes{/size}") xalign 0.35
                frame:
                    ymaximum 150
                    background "#5bf400aa"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "Confirmed subroutes" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/ash_%s.png" action [Hide("crabs_compatible_routes_jenna"), ShowMenu("crabs_compatible_routes_ash")]
                            imagebutton auto "gui/zoey_%s.png" action [Hide("crabs_compatible_routes_jenna"), ShowMenu("crabs_compatible_routes_zoey")]

                frame:
                    ymaximum 250
                    background "#cab50099"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "No route conflicts in current version" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/sarah_%s.png" action [Hide("crabs_compatible_routes_jenna"), ShowMenu("crabs_compatible_routes_sarah")]
                            imagebutton auto "gui/jules_%s.png" action [Hide("crabs_compatible_routes_jenna"), ShowMenu("crabs_compatible_routes_jules")]
                            imagebutton auto "gui/katy_%s.png" action [Hide("crabs_compatible_routes_jenna"), ShowMenu("crabs_compatible_routes_katy")]
                            imagebutton auto "gui/rachel_%s.png" action [Hide("crabs_compatible_routes_jenna"), ShowMenu("crabs_compatible_routes_rachel")]
                            imagebutton auto "gui/caroline_%s.png" action [Hide("crabs_compatible_routes_jenna"), ShowMenu("crabs_compatible_routes_caroline")]
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/madison_%s.png" action [Hide("crabs_compatible_routes_jenna"), ShowMenu("crabs_compatible_routes_madison")]
                            imagebutton auto "gui/emma_%s.png" action [Hide("crabs_compatible_routes_jenna"), ShowMenu("crabs_compatible_routes_emma")]
                            imagebutton auto "gui/penny_%s.png" action [Hide("crabs_compatible_routes_jenna"), ShowMenu("crabs_compatible_routes_penny")]
                            imagebutton auto "gui/trish_%s.png" action [Hide("crabs_compatible_routes_jenna"), ShowMenu("crabs_compatible_routes_trish")]

                frame:
                    ymaximum 150
                    background "#fc4f00"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "Routes are mutually exclusive after the first week" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/chloe_%s.png" action [Hide("crabs_compatible_routes_jenna"), ShowMenu("crabs_compatible_routes_chloe")]
                            imagebutton auto "gui/malia_%s.png" action [Hide("crabs_compatible_routes_jenna"), ShowMenu("crabs_compatible_routes_malia")]
                            imagebutton auto "gui/isabella_%s.png" action [Hide("crabs_compatible_routes_jenna"), ShowMenu("crabs_compatible_routes_isabella")]
                            imagebutton auto "gui/holmes_%s.png" action [Hide("crabs_compatible_routes_jenna"), ShowMenu("crabs_compatible_routes_holmes")]

            add "sheet_jenna.png" xalign 0 yalign 1 zoom 0.4

        textbutton "Back" action Hide("crabs_compatible_routes_jenna")

screen crabs_compatible_routes_jules():
    predict False
    use game_menu(_("Jules"), scroll="viewport"):
        frame:
            background "#0099cc"
            ymaximum 750
            vbox:
                text ("{size=+10}Compatible subroutes{/size}") xalign 0.35
                frame:
                    ymaximum 150
                    background "#5bf400aa"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "Confirmed subroutes" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/isabella_%s.png" action [Hide("crabs_compatible_routes_jules"), ShowMenu("crabs_compatible_routes_isabella")]
                            imagebutton auto "gui/chloe_%s.png" action [Hide("crabs_compatible_routes_jules"), ShowMenu("crabs_compatible_routes_chloe")]
                            imagebutton auto "gui/emma_%s.png" action [Hide("crabs_compatible_routes_jules"), ShowMenu("crabs_compatible_routes_emma")]

                frame:
                    ymaximum 250
                    background "#cab50099"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "No route conflicts in current version" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/zoey_%s.png" action [Hide("crabs_compatible_routes_jules"), ShowMenu("crabs_compatible_routes_zoey")]
                            imagebutton auto "gui/penny_%s.png" action [Hide("crabs_compatible_routes_jules"), ShowMenu("crabs_compatible_routes_penny")]
                            imagebutton auto "gui/jenna_%s.png" action [Hide("crabs_compatible_routes_jules"), ShowMenu("crabs_compatible_routes_jenna")]
                            imagebutton auto "gui/ash_%s.png" action [Hide("crabs_compatible_routes_jules"), ShowMenu("crabs_compatible_routes_ash")]
                            imagebutton auto "gui/madison_%s.png" action [Hide("crabs_compatible_routes_jules"), ShowMenu("crabs_compatible_routes_madison")]
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/rachel_%s.png" action [Hide("crabs_compatible_routes_jules"), ShowMenu("crabs_compatible_routes_rachel")]
                            imagebutton auto "gui/trish_%s.png" action [Hide("crabs_compatible_routes_jules"), ShowMenu("crabs_compatible_routes_trish")]
                            imagebutton auto "gui/caroline_%s.png" action [Hide("crabs_compatible_routes_jules"), ShowMenu("crabs_compatible_routes_caroline")]
                            imagebutton auto "gui/malia_%s.png" action [Hide("crabs_compatible_routes_jules"), ShowMenu("crabs_compatible_routes_malia")]
                            imagebutton auto "gui/holmes_%s.png" action [Hide("crabs_compatible_routes_jules"), ShowMenu("crabs_compatible_routes_holmes")]

                frame:
                    ymaximum 150
                    background "#fc4f00"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "Routes are mutually exclusive after the second week" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/katy_%s.png" action [Hide("crabs_compatible_routes_jules"), ShowMenu("crabs_compatible_routes_katy")]
                            imagebutton auto "gui/sarah_%s.png" action [Hide("crabs_compatible_routes_jules"), ShowMenu("crabs_compatible_routes_sarah")]

            add "sheet_jules.png" xalign 0 yalign 1 zoom 0.4

        textbutton "Back" action Hide("crabs_compatible_routes_jules")

screen crabs_compatible_routes_katy():
    predict False
    use game_menu(_("Katy"), scroll="viewport"):
        frame:
            background "#0099cc"
            ymaximum 750
            vbox:
                text ("{size=+10}Compatible subroutes{/size}") xalign 0.35
                frame:
                    ymaximum 150
                    background "#5bf400aa"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "Confirmed subroutes" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/rachel_%s.png" action [Hide("crabs_compatible_routes_katy"), ShowMenu("crabs_compatible_routes_rachel")]
                            imagebutton auto "gui/zoey_%s.png" action [Hide("crabs_compatible_routes_katy"), ShowMenu("crabs_compatible_routes_zoey")]


                frame:
                    ymaximum 250
                    background "#cab50099"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "No route conflicts in current version" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/ash_%s.png" action [Hide("crabs_compatible_routes_katy"), ShowMenu("crabs_compatible_routes_ash")]
                            imagebutton auto "gui/jenna_%s.png" action [Hide("crabs_compatible_routes_katy"), ShowMenu("crabs_compatible_routes_jenna")]
                            imagebutton auto "gui/penny_%s.png" action [Hide("crabs_compatible_routes_katy"), ShowMenu("crabs_compatible_routes_penny")]
                            imagebutton auto "gui/madison_%s.png" action [Hide("crabs_compatible_routes_katy"), ShowMenu("crabs_compatible_routes_madison")]
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/chloe_%s.png" action [Hide("crabs_compatible_routes_katy"), ShowMenu("crabs_compatible_routes_chloe")]
                            imagebutton auto "gui/emma_%s.png" action [Hide("crabs_compatible_routes_katy"), ShowMenu("crabs_compatible_routes_emma")]
                            imagebutton auto "gui/caroline_%s.png" action [Hide("crabs_compatible_routes_katy"), ShowMenu("crabs_compatible_routes_caroline")]
                            imagebutton auto "gui/malia_%s.png" action [Hide("crabs_compatible_routes_katy"), ShowMenu("crabs_compatible_routes_malia")]
                frame:
                    ymaximum 150
                    background "#d47800"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "Routes are mutually exclusive after the second week" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/jules_%s.png" action [Hide("crabs_compatible_routes_katy"), ShowMenu("crabs_compatible_routes_jules")]
                            imagebutton auto "gui/sarah_%s.png" action [Hide("crabs_compatible_routes_katy"), ShowMenu("crabs_compatible_routes_sarah")]
                frame:
                    ymaximum 150
                    background "#fc4f00"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "Routes are mutually exclusive after the first week" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/holmes_%s.png" action [Hide("crabs_compatible_routes_katy"), ShowMenu("crabs_compatible_routes_holmes")]
                            imagebutton auto "gui/isabella_%s.png" action [Hide("crabs_compatible_routes_katy"), ShowMenu("crabs_compatible_routes_isabella")]
                            imagebutton auto "gui/trish_%s.png" action [Hide("crabs_compatible_routes_katy"), ShowMenu("crabs_compatible_routes_trish")]

            add "sheet_katy.png" xalign 0 yalign 1 zoom 0.4

        textbutton "Back" action Hide("crabs_compatible_routes_katy")

screen crabs_compatible_routes_madison():
    predict False
    use game_menu(_("Madison"), scroll="viewport"):
        frame:
            background "#0099cc"
            ymaximum 750
            vbox:
                text ("{size=+10}Compatible subroutes{/size}") xalign 0.35
                frame:
                    ymaximum 150
                    background "#5bf400aa"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "Confirmed subroutes" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/chloe_%s.png" action [Hide("crabs_compatible_routes_madison"), ShowMenu("crabs_compatible_routes_chloe")]
                            imagebutton auto "gui/emma_%s.png" action [Hide("crabs_compatible_routes_madison"), ShowMenu("crabs_compatible_routes_emma")]
                            imagebutton auto "gui/trish_%s.png" action [Hide("crabs_compatible_routes_madison"), ShowMenu("crabs_compatible_routes_trish")]

                frame:
                    ymaximum 350
                    background "#cab50099"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "No route conflicts in current version" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/jules_%s.png" action [Hide("crabs_compatible_routes_madison"), ShowMenu("crabs_compatible_routes_jules")]
                            imagebutton auto "gui/caroline_%s.png" action [Hide("crabs_compatible_routes_madison"), ShowMenu("crabs_compatible_routes_caroline")]
                            imagebutton auto "gui/zoey_%s.png" action [Hide("crabs_compatible_routes_madison"), ShowMenu("crabs_compatible_routes_zoey")]
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/malia_%s.png" action [Hide("crabs_compatible_routes_madison"), ShowMenu("crabs_compatible_routes_malia")]
                            imagebutton auto "gui/penny_%s.png" action [Hide("crabs_compatible_routes_madison"), ShowMenu("crabs_compatible_routes_penny")]
                            imagebutton auto "gui/katy_%s.png" action [Hide("crabs_compatible_routes_madison"), ShowMenu("crabs_compatible_routes_katy")]
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/ash_%s.png" action [Hide("crabs_compatible_routes_madison"), ShowMenu("crabs_compatible_routes_ash")]
                            imagebutton auto "gui/jenna_%s.png" action [Hide("crabs_compatible_routes_madison"), ShowMenu("crabs_compatible_routes_jenna")]
                            imagebutton auto "gui/sarah_%s.png" action [Hide("crabs_compatible_routes_madison"), ShowMenu("crabs_compatible_routes_sarah")]

                frame:
                    ymaximum 150
                    background "#fc4f00"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "Routes are mutually exclusive after the second week" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/holmes_%s.png" action [Hide("crabs_compatible_routes_madison"), ShowMenu("crabs_compatible_routes_holmes")]
                            imagebutton auto "gui/isabella_%s.png" action [Hide("crabs_compatible_routes_madison"), ShowMenu("crabs_compatible_routes_isabella")]
                            imagebutton auto "gui/rachel_%s.png" action [Hide("crabs_compatible_routes_madison"), ShowMenu("crabs_compatible_routes_rachel")]

            add "sheet_madison.png" xalign 0 yalign 1 zoom 0.4

        textbutton "Back" action Hide("crabs_compatible_routes_madison")

screen crabs_compatible_routes_malia():
    predict False
    use game_menu(_("Malia"), scroll="viewport"):
        frame:
            background "#0099cc"
            ymaximum 750
            vbox:
                text ("{size=+10}Compatible subroutes{/size}") xalign 0.35
                frame:
                    ymaximum 150
                    background "#5bf400aa"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "Confirmed subroutes" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/trish_%s.png" action [Hide("crabs_compatible_routes_malia"), ShowMenu("crabs_compatible_routes_trish")]
                            imagebutton auto "gui/holmes_%s.png" action [Hide("crabs_compatible_routes_malia"), ShowMenu("crabs_compatible_routes_holmes")]

                frame:
                    ymaximum 250
                    background "#cab50099"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "No route conflicts in current version" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/jules_%s.png" action [Hide("crabs_compatible_routes_malia"), ShowMenu("crabs_compatible_routes_jules")]
                            imagebutton auto "gui/emma_%s.png" action [Hide("crabs_compatible_routes_malia"), ShowMenu("crabs_compatible_routes_emma")]
                            imagebutton auto "gui/isabella_%s.png" action [Hide("crabs_compatible_routes_malia"), ShowMenu("crabs_compatible_routes_isabella")]
                            imagebutton auto "gui/madison_%s.png" action [Hide("crabs_compatible_routes_malia"), ShowMenu("crabs_compatible_routes_madison")]
                            imagebutton auto "gui/sarah_%s.png" action [Hide("crabs_compatible_routes_malia"), ShowMenu("crabs_compatible_routes_sarah")]
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/penny_%s.png" action [Hide("crabs_compatible_routes_malia"), ShowMenu("crabs_compatible_routes_penny")]
                            imagebutton auto "gui/zoey_%s.png" action [Hide("crabs_compatible_routes_malia"), ShowMenu("crabs_compatible_routes_zoey")]
                            imagebutton auto "gui/rachel_%s.png" action [Hide("crabs_compatible_routes_malia"), ShowMenu("crabs_compatible_routes_rachel")]
                            imagebutton auto "gui/katy_%s.png" action [Hide("crabs_compatible_routes_malia"), ShowMenu("crabs_compatible_routes_katy")]
                frame:
                    ymaximum 150
                    background "#d47800"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "Routes are mutually exclusive after the second week" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/ash_%s.png" action [Hide("crabs_compatible_routes_malia"), ShowMenu("crabs_compatible_routes_ash")]
                            imagebutton auto "gui/chloe_%s.png" action [Hide("crabs_compatible_routes_malia"), ShowMenu("crabs_compatible_routes_chloe")]
                            imagebutton auto "gui/caroline_%s.png" action [Hide("crabs_compatible_routes_malia"), ShowMenu("crabs_compatible_routes_caroline")]
                frame:
                    ymaximum 150
                    background "#fc4f00"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "Routes are mutually exclusive after the first week" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/jenna_%s.png" action [Hide("crabs_compatible_routes_malia"), ShowMenu("crabs_compatible_routes_jenna")]

            add "sheet_malia.png" xalign 0 yalign 1 zoom 0.4

        textbutton "Back" action Hide("crabs_compatible_routes_malia")

screen crabs_compatible_routes_rachel():
    predict False
    use game_menu(_("Rachel"), scroll="viewport"):
        frame:
            background "#0099cc"
            ymaximum 750
            vbox:
                text ("{size=+10}Compatible subroutes{/size}") xalign 0.35
                frame:
                    ymaximum 150
                    background "#5bf400aa"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "Confirmed subroutes" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/katy_%s.png" action [Hide("crabs_compatible_routes_rachel"), ShowMenu("crabs_compatible_routes_katy")]
                            imagebutton auto "gui/zoey_%s.png" action [Hide("crabs_compatible_routes_rachel"), ShowMenu("crabs_compatible_routes_zoey")]

                frame:
                    ymaximum 250
                    background "#cab50099"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "No route conflicts in current version" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/jules_%s.png" action [Hide("crabs_compatible_routes_rachel"), ShowMenu("crabs_compatible_routes_jules")]
                            imagebutton auto "gui/emma_%s.png" action [Hide("crabs_compatible_routes_rachel"), ShowMenu("crabs_compatible_routes_emma")]
                            imagebutton auto "gui/penny_%s.png" action [Hide("crabs_compatible_routes_rachel"), ShowMenu("crabs_compatible_routes_penny")]
                            imagebutton auto "gui/malia_%s.png" action [Hide("crabs_compatible_routes_rachel"), ShowMenu("crabs_compatible_routes_malia")]
                            imagebutton auto "gui/trish_%s.png" action [Hide("crabs_compatible_routes_rachel"), ShowMenu("crabs_compatible_routes_trish")]
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/sarah_%s.png" action [Hide("crabs_compatible_routes_rachel"), ShowMenu("crabs_compatible_routes_sarah")]
                            imagebutton auto "gui/ash_%s.png" action [Hide("crabs_compatible_routes_rachel"), ShowMenu("crabs_compatible_routes_ash")]
                            imagebutton auto "gui/jenna_%s.png" action [Hide("crabs_compatible_routes_rachel"), ShowMenu("crabs_compatible_routes_jenna")]
                            imagebutton auto "gui/chloe_%s.png" action [Hide("crabs_compatible_routes_rachel"), ShowMenu("crabs_compatible_routes_chloe")]
                            imagebutton auto "gui/caroline_%s.png" action [Hide("crabs_compatible_routes_rachel"), ShowMenu("crabs_compatible_routes_caroline")]

                frame:
                    ymaximum 150
                    background "#fc4f00"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "Routes are mutually exclusive after the second week" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/holmes_%s.png" action [Hide("crabs_compatible_routes_rachel"), ShowMenu("crabs_compatible_routes_holmes")]
                            imagebutton auto "gui/isabella_%s.png" action [Hide("crabs_compatible_routes_rachel"), ShowMenu("crabs_compatible_routes_isabella")]
                            imagebutton auto "gui/madison_%s.png" action [Hide("crabs_compatible_routes_rachel"), ShowMenu("crabs_compatible_routes_madison")]

            add "sheet_rachel.png" xalign 0 yalign 1 zoom 0.4

        textbutton "Back" action Hide("crabs_compatible_routes_rachel")

screen crabs_compatible_routes_sarah():
    predict False
    use game_menu(_("Sarah"), scroll="viewport"):
        frame:
            background "#0099cc"
            ymaximum 750
            vbox:
                text ("{size=+10}Compatible subroutes{/size}") xalign 0.35
                frame:
                    ymaximum 150
                    background "#5bf400aa"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "Confirmed subroutes" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/ash_%s.png" action [Hide("crabs_compatible_routes_sarah"), ShowMenu("crabs_compatible_routes_ash")]
                            imagebutton auto "gui/isabella_%s.png" action [Hide("crabs_compatible_routes_sarah"), ShowMenu("crabs_compatible_routes_isabella")]

                frame:
                    ymaximum 250
                    background "#cab50099"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "No route conflicts in current version" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/emma_%s.png" action [Hide("crabs_compatible_routes_sarah"), ShowMenu("crabs_compatible_routes_emma")]
                            imagebutton auto "gui/penny_%s.png" action [Hide("crabs_compatible_routes_sarah"), ShowMenu("crabs_compatible_routes_penny")]
                            imagebutton auto "gui/malia_%s.png" action [Hide("crabs_compatible_routes_sarah"), ShowMenu("crabs_compatible_routes_malia")]
                            imagebutton auto "gui/trish_%s.png" action [Hide("crabs_compatible_routes_sarah"), ShowMenu("crabs_compatible_routes_trish")]
                            imagebutton auto "gui/zoey_%s.png" action [Hide("crabs_compatible_routes_sarah"), ShowMenu("crabs_compatible_routes_zoey")]
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/rachel_%s.png" action [Hide("crabs_compatible_routes_sarah"), ShowMenu("crabs_compatible_routes_rachel")]
                            imagebutton auto "gui/jenna_%s.png" action [Hide("crabs_compatible_routes_sarah"), ShowMenu("crabs_compatible_routes_jenna")]
                            imagebutton auto "gui/chloe_%s.png" action [Hide("crabs_compatible_routes_sarah"), ShowMenu("crabs_compatible_routes_chloe")]
                            imagebutton auto "gui/caroline_%s.png" action [Hide("crabs_compatible_routes_sarah"), ShowMenu("crabs_compatible_routes_caroline")]
                            imagebutton auto "gui/madison_%s.png" action [Hide("crabs_compatible_routes_sarah"), ShowMenu("crabs_compatible_routes_madison")]


                frame:
                    ymaximum 150
                    background "#fc4f00"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "Routes are mutually exclusive after the second week" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/holmes_%s.png" action [Hide("crabs_compatible_routes_sarah"), ShowMenu("crabs_compatible_routes_holmes")]
                            imagebutton auto "gui/jules_%s.png" action [Hide("crabs_compatible_routes_sarah"), ShowMenu("crabs_compatible_routes_jules")]
                            imagebutton auto "gui/katy_%s.png" action [Hide("crabs_compatible_routes_sarah"), ShowMenu("crabs_compatible_routes_katy")]

            add "sheet_sarah.png" xalign 0 yalign 1 zoom 0.4

        textbutton "Back" action Hide("crabs_compatible_routes_sarah")

screen crabs_compatible_routes_trish():
    predict False
    use game_menu(_("Trish"), scroll="viewport"):
        frame:
            background "#0099cc"
            ymaximum 750
            vbox:
                text ("{size=+10}Compatible subroutes{/size}") xalign 0.35
                frame:
                    ymaximum 150
                    background "#5bf400aa"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "Confirmed subroutes" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/caroline_%s.png" action [Hide("crabs_compatible_routes_trish"), ShowMenu("crabs_compatible_routes_caroline")]
                            imagebutton auto "gui/madison_%s.png" action [Hide("crabs_compatible_routes_trish"), ShowMenu("crabs_compatible_routes_madison")]
                            imagebutton auto "gui/malia_%s.png" action [Hide("crabs_compatible_routes_trish"), ShowMenu("crabs_compatible_routes_malia")]

                frame:
                    ymaximum 250
                    background "#cab50099"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "No route conflicts in current version" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/penny_%s.png" action [Hide("crabs_compatible_routes_trish"), ShowMenu("crabs_compatible_routes_penny")]
                            imagebutton auto "gui/sarah_%s.png" action [Hide("crabs_compatible_routes_trish"), ShowMenu("crabs_compatible_routes_sarah")]
                            imagebutton auto "gui/zoey_%s.png" action [Hide("crabs_compatible_routes_trish"), ShowMenu("crabs_compatible_routes_zoey")]
                            imagebutton auto "gui/ash_%s.png" action [Hide("crabs_compatible_routes_trish"), ShowMenu("crabs_compatible_routes_ash")]
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/rachel_%s.png" action [Hide("crabs_compatible_routes_trish"), ShowMenu("crabs_compatible_routes_rachel")]
                            imagebutton auto "gui/jenna_%s.png" action [Hide("crabs_compatible_routes_trish"), ShowMenu("crabs_compatible_routes_jenna")]
                            imagebutton auto "gui/chloe_%s.png" action [Hide("crabs_compatible_routes_trish"), ShowMenu("crabs_compatible_routes_chloe")]
                            imagebutton auto "gui/jules_%s.png" action [Hide("crabs_compatible_routes_trish"), ShowMenu("crabs_compatible_routes_jules")]
                frame:
                    ymaximum 150
                    background "#d47800"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "Routes are mutually exclusive after the second week" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/emma_%s.png" action [Hide("crabs_compatible_routes_trish"), ShowMenu("crabs_compatible_routes_emma")]
                frame:
                    ymaximum 100
                    background "#fc4f00"
                    vbox:
                        xfill True
                        xcenter 0.5
                        text "Routes are mutually exclusive after the second week" xcenter 0.5
                        hbox:
                            xcenter 0.5
                            imagebutton auto "gui/holmes_%s.png" action [Hide("crabs_compatible_routes_trish"), ShowMenu("crabs_compatible_routes_holmes")]
                            imagebutton auto "gui/katy_%s.png" action [Hide("crabs_compatible_routes_trish"), ShowMenu("crabs_compatible_routes_katy")]
                            imagebutton auto "gui/isabella_%s.png" action [Hide("crabs_compatible_routes_trish"), ShowMenu("crabs_compatible_routes_isabella")]


            add "sheet_trish.png" xalign 0 yalign 1 zoom 0.25

        textbutton "Back" action Hide("crabs_compatible_routes_trish")
