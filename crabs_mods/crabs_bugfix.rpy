init offset = 3

default crabs_patch_temp_variables = set()
init python:

    #to track the changes and be able to revert them if needed
    def crabs_temp_variable(name):
        crabs_patch_temp_variables.add(name)

    def crabs_patch_function(labelName, newContext):
        #in case there are callbacks defined by the base game, other mods or something
        if old_label_callback is not None and not old_label_callback.__name__ == "crabs_patch_function":
            old_label_callback(labelName, newContext)

    old_label_callback = config.label_callback
    config.label_callback = crabs_patch_function
    